# -*- encoding: utf-8 -*-
# stub: hoe-gemspec 1.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "hoe-gemspec"
  s.version = "1.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Mike Dalessio"]
  s.date = "2010-09-03"
  s.description = "Generate a prerelease gemspec based on a Hoe spec."
  s.email = ["mike.dalessio@gmail.com"]
  s.extra_rdoc_files = ["Manifest.txt", "CHANGELOG.rdoc", "README.rdoc"]
  s.files = ["CHANGELOG.rdoc", "Manifest.txt", "README.rdoc"]
  s.homepage = "http://github.com/flavorjones/hoe-gemspec"
  s.rdoc_options = ["--main", "README.rdoc"]
  s.rubyforge_project = "hoe-gemspec"
  s.rubygems_version = "2.2.5"
  s.summary = "Generate a prerelease gemspec based on a Hoe spec."

  s.installed_by_version = "2.2.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<hoe>, [">= 2.2.0"])
      s.add_development_dependency(%q<rubyforge>, [">= 2.0.4"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<hoe>, [">= 2.6.1"])
    else
      s.add_dependency(%q<hoe>, [">= 2.2.0"])
      s.add_dependency(%q<rubyforge>, [">= 2.0.4"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<hoe>, [">= 2.6.1"])
    end
  else
    s.add_dependency(%q<hoe>, [">= 2.2.0"])
    s.add_dependency(%q<rubyforge>, [">= 2.0.4"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<hoe>, [">= 2.6.1"])
  end
end
