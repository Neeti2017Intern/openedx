# -*- encoding: utf-8 -*-
# stub: iconv 1.0.3 ruby lib
# stub: ext/iconv/extconf.rb

Gem::Specification.new do |s|
  s.name = "iconv"
  s.version = "1.0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["NARUSE, Yui"]
  s.date = "2013-04-10"
  s.description = "iconv wrapper library"
  s.email = ["naruse@airemix.jp"]
  s.extensions = ["ext/iconv/extconf.rb"]
  s.files = ["ext/iconv/extconf.rb"]
  s.homepage = "https://github.com/nurse/iconv"
  s.rubygems_version = "2.2.5"
  s.summary = "iconv wrapper library"

  s.installed_by_version = "2.2.5" if s.respond_to? :installed_by_version
end
