# -*- encoding: utf-8 -*-
# stub: github-markdown 0.5.3 ruby lib
# stub: ext/markdown/extconf.rb

Gem::Specification.new do |s|
  s.name = "github-markdown"
  s.version = "0.5.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["GitHub, Inc"]
  s.date = "2012-07-07"
  s.description = "Self-contained Markdown parser for GitHub, with all our custom extensions"
  s.email = "vicent@github.com"
  s.extensions = ["ext/markdown/extconf.rb"]
  s.files = ["ext/markdown/extconf.rb"]
  s.homepage = "http://github.github.com/github-flavored-markdown/"
  s.rubygems_version = "2.2.5"
  s.summary = "The Markdown parser for GitHub.com"

  s.installed_by_version = "2.2.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake-compiler>, [">= 0"])
    else
      s.add_dependency(%q<rake-compiler>, [">= 0"])
    end
  else
    s.add_dependency(%q<rake-compiler>, [">= 0"])
  end
end
