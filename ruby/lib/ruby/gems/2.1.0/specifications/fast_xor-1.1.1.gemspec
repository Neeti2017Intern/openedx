# -*- encoding: utf-8 -*-
# stub: fast_xor 1.1.1 ruby lib/fast_xor
# stub: ext/xor/extconf.rb

Gem::Specification.new do |s|
  s.name = "fast_xor"
  s.version = "1.1.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib/fast_xor"]
  s.authors = ["Steve Sloan"]
  s.date = "2012-07-22"
  s.description = "Provides a C-optimized method for in-place XORing of two (or three) strings"
  s.email = "steve@finagle.org"
  s.extensions = ["ext/xor/extconf.rb"]
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc", "ext/xor/extconf.rb"]
  s.homepage = "http://github.com/CodeMonkeySteve/fast_xor"
  s.rdoc_options = ["--charset=UTF-8"]
  s.rubygems_version = "2.2.5"
  s.summary = "Fast String XOR operator"

  s.installed_by_version = "2.2.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rake>, [">= 0"])
      s.add_runtime_dependency(%q<rake-compiler>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
    else
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rake-compiler>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
    end
  else
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rake-compiler>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
  end
end
