# -*- encoding: utf-8 -*-
# stub: rexical 1.0.5 ruby lib

Gem::Specification.new do |s|
  s.name = "rexical"
  s.version = "1.0.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Aaron Patterson"]
  s.date = "2010-12-08"
  s.description = "Rexical is a lexical scanner generator.\nIt is written in Ruby itself, and generates Ruby program.\nIt is designed for use with Racc."
  s.email = ["aaronp@rubyforge.org"]
  s.executables = ["rex"]
  s.extra_rdoc_files = ["Manifest.txt", "CHANGELOG.rdoc", "DOCUMENTATION.en.rdoc", "DOCUMENTATION.ja.rdoc", "README.rdoc"]
  s.files = ["CHANGELOG.rdoc", "DOCUMENTATION.en.rdoc", "DOCUMENTATION.ja.rdoc", "Manifest.txt", "README.rdoc", "bin/rex"]
  s.homepage = "http://github.com/tenderlove/rexical/tree/master"
  s.rdoc_options = ["--main", "README.rdoc"]
  s.rubyforge_project = "ruby-rex"
  s.rubygems_version = "2.2.5"
  s.summary = "Rexical is a lexical scanner generator"

  s.installed_by_version = "2.2.5" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<hoe>, [">= 2.6.2"])
    else
      s.add_dependency(%q<hoe>, [">= 2.6.2"])
    end
  else
    s.add_dependency(%q<hoe>, [">= 2.6.2"])
  end
end
