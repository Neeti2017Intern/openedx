# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497939835.016969
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/static_templates/server-error.html'
_template_uri = 'static_templates/server-error.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from openedx.core.djangolib.markup import HTML, Text


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'../main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n<main id="main" aria-label="Content" tabindex="-1">\n    <section class="outside-app">\n      <h1>\n        ')
        __M_writer(filters.html_escape(filters.decode.utf8(Text(_(u"There has been a 500 error on the {platform_name} servers")).format(
            platform_name=HTML("<em>{platform_name}</em>").format(platform_name=Text(static.get_platform_name()))
        ))))
        __M_writer(u'\n      </h1>\n      <p>\n        ')
        __M_writer(filters.html_escape(filters.decode.utf8(Text(_('Please wait a few seconds and then reload the page. If the problem persists, please email us at {email}.')).format(
            email=HTML('<a href="mailto:{email}">{email}</a>').format(
                email=Text(static.get_tech_support_email_address())
            )
        ))))
        __M_writer(u'\n      </p>\n    </section>\n</main>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"48": 17, "34": 1, "40": 1, "41": 2, "42": 6, "43": 7, "44": 12, "47": 14, "16": 3, "53": 21, "59": 53, "28": 2}, "uri": "static_templates/server-error.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/static_templates/server-error.html"}
__M_END_METADATA
"""
