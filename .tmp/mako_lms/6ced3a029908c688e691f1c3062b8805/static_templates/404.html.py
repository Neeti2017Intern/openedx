# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1498036960.144196
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/static_templates/404.html'
_template_uri = 'static_templates/404.html'
_source_encoding = 'utf-8'
_exports = [u'pagetitle']



from django.utils.translation import ugettext as _
from openedx.core.djangolib.markup import HTML, Text


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'../main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n\n<main id="main" aria-label="Content" tabindex="-1">\n    <section class="outside-app">\n    <h1>')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Page not found"))))
        __M_writer(u'</h1>\n    <p>')
        __M_writer(filters.html_escape(filters.decode.utf8(Text(_('The page that you were looking for was not found. Go back to the {link_start}homepage{link_end} or let us know about any pages that may have been moved at {email}.')).format(
        link_start=HTML('<a href="/">'),
        link_end=HTML('</a>'),
        email=HTML('<a href="mailto:{email}">{email}</a>').format(email=Text(static.get_tech_support_email_address()))
      ))))
        __M_writer(u'</p>\n    </section>\n</main>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def pagetitle():
            return render_pagetitle(context)
        __M_writer = context.writer()
        __M_writer(filters.html_escape(filters.decode.utf8(_("Page Not Found"))))
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"64": 9, "34": 1, "70": 9, "76": 70, "42": 1, "43": 2, "44": 6, "45": 7, "16": 3, "50": 9, "51": 13, "52": 13, "53": 14, "58": 18, "28": 2}, "uri": "static_templates/404.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/static_templates/404.html"}
__M_END_METADATA
"""
