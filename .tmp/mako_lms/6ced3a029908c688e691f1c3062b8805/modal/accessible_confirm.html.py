# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946367.535165
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/modal/accessible_confirm.html'
_template_uri = u'modal/accessible_confirm.html'
_source_encoding = 'utf-8'
_exports = []


from django.utils.translation import ugettext as _ 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        __M_writer = context.writer()
        __M_writer(u'\n<div id="accessibile-confirm-modal" class="modal" aria-hidden="true">\n  <div class="inner-wrapper" role="dialog" aria-labelledby="accessibile-confirm-title">\n    <button class="close-modal">\n      <span class="icon fa fa-remove" aria-hidden="true"></span>\n      <span class="sr">\n')
        __M_writer(u'        ')
        __M_writer(filters.decode.utf8(_('Close')))
        __M_writer(u'\n      </span>\n    </button>\n\n    <header>\n      <h2 id="accessibile-confirm-title">\n          ')
        __M_writer(filters.decode.utf8(_('Confirm')))
        __M_writer(u'\n        <span class="sr">,\n')
        __M_writer(u'          ')
        __M_writer(filters.decode.utf8(_("modal open")))
        __M_writer(u'\n        </span>\n      </h2>\n    </header>\n    <div role="alertdialog" class="status message" tabindex="-1">\n        <p class="message-title"></p>\n    </div>\n    <hr aria-hidden="true" />\n    <div class="actions">\n        <button class="dismiss ok-button">')
        __M_writer(filters.decode.utf8(_('OK')))
        __M_writer(u'</button>\n        <button class="dismiss cancel-button" data-dismiss="leanModal">')
        __M_writer(filters.decode.utf8(_('Cancel')))
        __M_writer(u'</button>\n    </div>\n  </div>\n  <a href="#accessibile-confirm-modal" rel="leanModal" id="confirm_open_button" style="display:none">')
        __M_writer(filters.decode.utf8(_("open")))
        __M_writer(u'</a>\n</div>\n\n<script type="text/javascript">\nvar accessible_confirm = function(message, callback) {\n    $("#accessibile-confirm-modal .cancel-button").click(function(){\n        $("#accessibile-confirm-modal .close-modal").click();\n    });\n    $("#accessibile-confirm-modal .ok-button").click(function(){\n        $("#accessibile-confirm-modal .close-modal").click();\n        callback();\n    });\n\n    accessible_modal("#accessibile-confirm-modal #confirm_open_button", "#accessibile-confirm-modal .close-modal", "#accessibile-confirm-modal", ".content-wrapper");\n    $("#accessibile-confirm-modal #confirm_open_button").click();\n    $("#accessibile-confirm-modal .message-title").html(message);\n};\n</script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"32": 26, "33": 26, "34": 27, "35": 27, "36": 30, "37": 30, "43": 37, "16": 1, "18": 0, "23": 1, "24": 8, "25": 8, "26": 8, "27": 14, "28": 14, "29": 17, "30": 17, "31": 17}, "uri": "modal/accessible_confirm.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/modal/accessible_confirm.html"}
__M_END_METADATA
"""
