# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849599.574323
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/register-sidebar.html'
_template_uri = 'register-sidebar.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace('__anon_0x7fa1b84005d0', context._clean_inheritance_tokens(), templateuri=u'main.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, '__anon_0x7fa1b84005d0')] = ns

    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b84005d0')._populate(_import_ns, [u'login_query'])
        platform_name = _import_ns.get('platform_name', context.get('platform_name', UNDEFINED))
        has_extauth_info = _import_ns.get('has_extauth_info', context.get('has_extauth_info', UNDEFINED))
        marketing_link = _import_ns.get('marketing_link', context.get('marketing_link', UNDEFINED))
        login_query = _import_ns.get('login_query', context.get('login_query', UNDEFINED))
        settings = _import_ns.get('settings', context.get('settings', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n\n<header>\n  <h3 class="sr">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Registration Help"))))
        __M_writer(u'</h3>\n</header>\n\n')
        if has_extauth_info is UNDEFINED:
            __M_writer(u'\n<div class="btn-brand btn-login">\n  <h3 class="title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Already registered?"))))
            __M_writer(u'</h3>\n  <p class="instructions">\n    <a class="btn-login-action" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('signin_user'))))
            __M_writer(filters.html_escape(filters.decode.utf8(login_query())))
            __M_writer(u'">\n      ')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Log in"))))
            __M_writer(u'\n    </a>\n  </p>\n</div>\n\n')
        __M_writer(u'\n')
        __M_writer(u'<div class="cta cta-welcome">\n  <h3>')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Welcome to {platform_name}").format(platform_name=platform_name))))
        __M_writer(u'</h3>\n  <p>')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Registering with {platform_name} gives you access to all of our current and future free courses. Not ready to take a course just yet? Registering puts you on our mailing list - we will update you as courses are added.").format(platform_name=platform_name))))
        __M_writer(u'</p>\n</div>\n\n<div class="cta cta-nextsteps">\n  <h3>')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Next Steps"))))
        __M_writer(u'</h3>\n    <p>')
        __M_writer(filters.html_escape(filters.decode.utf8(_("As part of joining {platform_name}, you will receive an email message with instructions for activating your account. Don't see the email? Check your spam folder and mark {platform_name} emails as 'not spam'. At {platform_name}, we communicate mostly through email.").format(platform_name=platform_name))))
        __M_writer(u'</p>\n</div>\n\n')
        if settings.MKTG_URL_LINK_MAP.get('FAQ'):
            __M_writer(u'  <div class="cta cta-help">\n    <h3>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Need Help?"))))
            __M_writer(u'</h3>\n    <p>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Need help registering with {platform_name}?").format(platform_name=platform_name))))
            __M_writer(u'\n      <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('FAQ'))))
            __M_writer(u'">\n          ')
            __M_writer(filters.html_escape(filters.decode.utf8(_("View our FAQs for answers to commonly asked questions."))))
            __M_writer(u'\n      </a>\n      ')
            __M_writer(filters.html_escape(filters.decode.utf8(_("You can find the answers to most of your questions in our list of FAQs. After you enroll in a course, you can also find answers in the course discussions."))))
            __M_writer(u'</p>\n  </div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "28": 6, "31": 7, "34": 1, "46": 1, "47": 5, "48": 6, "49": 7, "50": 11, "51": 11, "52": 14, "53": 15, "54": 17, "55": 17, "56": 19, "57": 19, "58": 19, "59": 20, "60": 20, "61": 26, "62": 29, "63": 30, "64": 30, "65": 31, "66": 31, "67": 35, "68": 35, "69": 36, "70": 36, "71": 39, "72": 40, "73": 41, "74": 41, "75": 42, "76": 42, "77": 43, "78": 43, "79": 44, "80": 44, "81": 46, "82": 46, "88": 82}, "uri": "register-sidebar.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/register-sidebar.html"}
__M_END_METADATA
"""
