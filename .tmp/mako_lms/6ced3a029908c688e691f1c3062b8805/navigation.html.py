# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497848942.225568
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/navigation.html'
_template_uri = u'navigation.html'
_source_encoding = 'utf-8'
_exports = [u'navigation_sign_in', u'navigation_global_links', u'navigation_logo', u'navigation_top', u'navigation_other_global_links', u'js_extra', u'navigation_global_links_authenticated']



from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from context_processors import doc_url
from lms.djangoapps.ccx.overrides import get_current_ccx
from openedx.core.djangolib.markup import HTML, Text

# App that handles subdomain specific branding
from branding import api as branding_api
# app that handles site status messages
from status.status import get_site_status_msg
from util.enterprise_helpers import get_enterprise_customer_logo_url


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

    ns = runtime.TemplateNamespace('__anon_0x7fa1b85a1210', context._clean_inheritance_tokens(), templateuri=u'main.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, '__anon_0x7fa1b85a1210')] = ns

def render_body(context,online_help_token,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs,online_help_token=online_help_token)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        csrf_token = _import_ns.get('csrf_token', context.get('csrf_token', UNDEFINED))
        show_program_listing = _import_ns.get('show_program_listing', context.get('show_program_listing', UNDEFINED))
        def navigation_top():
            return render_navigation_top(context._locals(__M_locals))
        course = _import_ns.get('course', context.get('course', UNDEFINED))
        def navigation_other_global_links():
            return render_navigation_other_global_links(context._locals(__M_locals))
        get_online_help_info = _import_ns.get('get_online_help_info', context.get('get_online_help_info', UNDEFINED))
        static = _mako_get_namespace(context, 'static')
        LANGUAGE_CODE = _import_ns.get('LANGUAGE_CODE', context.get('LANGUAGE_CODE', UNDEFINED))
        def __M_anon_32():
            __M_caller = context.caller_stack._push_frame()
            try:
                __M_writer = context.writer()
                __M_writer(u'\n')

                try:
                    course_id = course.id
                except:
                    # can't figure out a better way to get at a possibly-defined course var
                    course_id = None
                site_status_msg = get_site_status_msg(course_id)
                
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_id','site_status_msg'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n')
                if site_status_msg:
                    __M_writer(u'<div class="site-status">\n  <div class="inner-wrapper">\n    <span class="white-error-icon"></span>\n    <p>')
                    __M_writer(filters.html_escape(filters.decode.utf8(site_status_msg)))
                    __M_writer(u'</p>\n  </div>\n</div>\n')
                return ''
            finally:
                context.caller_stack._pop_frame()
        def navigation_sign_in():
            return render_navigation_sign_in(context._locals(__M_locals))
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        def navigation_global_links_authenticated():
            return render_navigation_global_links_authenticated(context._locals(__M_locals))
        def navigation_global_links():
            return render_navigation_global_links(context._locals(__M_locals))
        len = _import_ns.get('len', context.get('len', UNDEFINED))
        user = _import_ns.get('user', context.get('user', UNDEFINED))
        is_secure = _import_ns.get('is_secure', context.get('is_secure', UNDEFINED))
        uses_pattern_library = _import_ns.get('uses_pattern_library', context.get('uses_pattern_library', UNDEFINED))
        marketing_link = _import_ns.get('marketing_link', context.get('marketing_link', UNDEFINED))
        settings = _import_ns.get('settings', context.get('settings', UNDEFINED))
        request = _import_ns.get('request', context.get('request', UNDEFINED))
        should_display_shopping_cart_func = _import_ns.get('should_display_shopping_cart_func', context.get('should_display_shopping_cart_func', UNDEFINED))
        login_query = _import_ns.get('login_query', context.get('login_query', UNDEFINED))
        def navigation_logo():
            return render_navigation_logo(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_top'):
            context['self'].navigation_top(**pageargs)
        

        __M_writer(u'\n\n')
        if uses_pattern_library:
            __M_writer(u'    ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
                context['self'].js_extra(**pageargs)
            

            __M_writer(u'\n')
        __M_writer(u'\n')
        __M_anon_32()
        __M_writer(u'\n  <header id="global-navigation" class="header-global ')
        __M_writer(filters.html_escape(filters.decode.utf8("slim" if course else "")))
        __M_writer(u'" >\n    <nav class="wrapper-header" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Global'))))
        __M_writer(u'">\n    <h1 class="logo">\n      <a href="')
        __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('ROOT'))))
        __M_writer(u'">\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_logo'):
            context['self'].navigation_logo(**pageargs)
        

        __M_writer(u'\n      </a>\n    </h1>\n\n')
        if course:
            __M_writer(u'      <h2 class="course-header"><span class="provider">')
            __M_writer(filters.html_escape(filters.decode.utf8(course.display_org_with_default)))
            __M_writer(u':</span>\n        <span class="course-number">')
            __M_writer(filters.html_escape(filters.decode.utf8(course.display_number_with_default)))
            __M_writer(u'</span>\n        ')

            display_name = course.display_name_with_default
            if settings.FEATURES.get('CUSTOM_COURSES_EDX', False):
              ccx = get_current_ccx(course.id)
              if ccx:
                display_name = ccx.display_name
            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['display_name','ccx'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n        <span class="course-name">')
            __M_writer(filters.html_escape(filters.decode.utf8(display_name)))
            __M_writer(u'</span></h2>\n')
        __M_writer(u'\n')
        if user.is_authenticated():
            __M_writer(u'        <ol class="left nav-global list-inline authenticated">\n        ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_global_links_authenticated'):
                context['self'].navigation_global_links_authenticated(**pageargs)
            

            __M_writer(u'\n      </ol>\n\n      ')
            runtime._include_file(context, u'user_dropdown.html', _template_uri)
            __M_writer(u'\n\n      <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(get_online_help_info(online_help_token)['doc_url'])))
            __M_writer(u'" \n         target="_blank"\n         class="doc-link">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Help"))))
            __M_writer(u'</a>\n\n')
            if should_display_shopping_cart_func() and not (course and static.is_request_in_themed_site()): # see shoppingcart.context_processor.user_has_cart_context_processor
                __M_writer(u'        <ol class="user">\n          <li class="primary">\n            <a class="shopping-cart" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(reverse('shoppingcart.views.show_cart'))))
                __M_writer(u'">\n              <span class="icon fa fa-shopping-cart" aria-hidden="true"></span> ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Shopping Cart"))))
                __M_writer(u'\n            </a>\n          </li>\n        </ol>\n')
        else:
            __M_writer(u'      <ol class="left list-inline nav-global">\n        ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_global_links'):
                context['self'].navigation_global_links(**pageargs)
            

            __M_writer(u'\n\n        ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_other_global_links'):
                context['self'].navigation_other_global_links(**pageargs)
            

            __M_writer(u'\n      </ol>\n\n      <ol class="right nav-courseware list-inline">\n        ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_sign_in'):
                context['self'].navigation_sign_in(**pageargs)
            

            __M_writer(u'\n      </ol>\n')
        if static.show_language_selector():
            __M_writer(u'     ')
            languages = static.get_released_languages() 
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['languages'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n')
            if len(languages) > 1:
                __M_writer(u'      <ol class="user">\n        <li class="primary">\n          <form action="/i18n/setlang/" method="post" class="settings-language-form" id="language-settings-form">\n            <input type="hidden" id="csrf_token" name="csrfmiddlewaretoken" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(csrf_token)))
                __M_writer(u'">\n')
                if user.is_authenticated():
                    __M_writer(u'            <input title="preference api" type="hidden" class="url-endpoint" value="')
                    __M_writer(filters.html_escape(filters.decode.utf8(reverse('preferences_api', kwargs={'username': user.username}))))
                    __M_writer(u'" data-user-is-authenticated="true">\n')
                else:
                    __M_writer(u'            <input title="session update url" type="hidden" class="url-endpoint" value="')
                    __M_writer(filters.html_escape(filters.decode.utf8(reverse('session_language'))))
                    __M_writer(u'" data-user-is-authenticated="false">\n')
                __M_writer(u'            <label><span class="sr">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Choose Language"))))
                __M_writer(u'</span>\n                <select class="input select language-selector" id="settings-language-value" name="language">\n')
                for language in languages:
                    if language[0] == LANGUAGE_CODE:
                        __M_writer(u'                      <option value="')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[0])))
                        __M_writer(u'" selected="selected">')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[1])))
                        __M_writer(u'</option>\n')
                    else:
                        __M_writer(u'                      <option value="')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[0])))
                        __M_writer(u'" >')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[1])))
                        __M_writer(u'</option>\n')
                __M_writer(u'                </select>\n            </label>\n          </form>\n        </li>\n      </ol>\n')
        __M_writer(u'    </nav>\n</header>\n')
        if course:
            __M_writer(u'<!--[if lte IE 9]>\n<div class="ie-banner" aria-hidden="true">')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_('{begin_strong}Warning:{end_strong} Your browser is not fully supported. We strongly recommend using {chrome_link} or {ff_link}.')).format(
    begin_strong=HTML('<strong>'),
    end_strong=HTML('</strong>'),
    chrome_link=HTML('<a href="https://www.google.com/chrome" target="_blank">Chrome</a>'),
    ff_link=HTML('<a href="http://www.mozilla.org/firefox" target="_blank">Firefox</a>'),
))))
            __M_writer(u'</div>\n<![endif]-->\n')
        __M_writer(u'\n')
        runtime._include_file(context, u'help_modal.html', _template_uri)
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_sign_in(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        course = _import_ns.get('course', context.get('course', UNDEFINED))
        login_query = _import_ns.get('login_query', context.get('login_query', UNDEFINED))
        def navigation_sign_in():
            return render_navigation_sign_in(context)
        settings = _import_ns.get('settings', context.get('settings', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(u'\n          <li class="item nav-courseware-01">\n')
        if not settings.FEATURES['DISABLE_LOGIN_BUTTON']:
            if course and settings.FEATURES.get('RESTRICT_ENROLL_BY_REG_METHOD') and course.enrollment_domain:
                __M_writer(u'                <a class="btn btn-login" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(reverse('course-specific-login', args=[course.id.to_deprecated_string()]))))
                __M_writer(filters.html_escape(filters.decode.utf8(login_query())))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Sign in"))))
                __M_writer(u'</a>\n')
            else:
                __M_writer(u'                <a class="btn btn-login" href="/login')
                __M_writer(filters.html_escape(filters.decode.utf8(login_query())))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Sign in"))))
                __M_writer(u'</a>\n')
        __M_writer(u'          </li>\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_global_links(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        def navigation_global_links():
            return render_navigation_global_links(context)
        static = _mako_get_namespace(context, 'static')
        marketing_link = _import_ns.get('marketing_link', context.get('marketing_link', UNDEFINED))
        settings = _import_ns.get('settings', context.get('settings', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(u'\n')
        if static.get_value('ENABLE_MKTG_SITE', settings.FEATURES.get('ENABLE_MKTG_SITE', False)):
            __M_writer(u'            <li class="item nav-global-01">\n              <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('HOW_IT_WORKS'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("How it Works"))))
            __M_writer(u'</a>\n            </li>\n')
            if settings.FEATURES.get('COURSES_ARE_BROWSABLE'):
                __M_writer(u'              <li class="item nav-global-02">\n                <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('COURSES'))))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Courses"))))
                __M_writer(u'</a>\n              </li>\n')
            __M_writer(u'            <li class="item nav-global-03">\n              <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('SCHOOLS'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Schools"))))
            __M_writer(u'</a>\n            </li>\n')
        __M_writer(u'        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_logo(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        is_secure = _import_ns.get('is_secure', context.get('is_secure', UNDEFINED))
        static = _mako_get_namespace(context, 'static')
        request = _import_ns.get('request', context.get('request', UNDEFINED))
        def navigation_logo():
            return render_navigation_logo(context)
        __M_writer = context.writer()
        __M_writer(u'\n        ')

        logo_url = get_enterprise_customer_logo_url(request)
        logo_size = 'ec-logo-size'
        if logo_url is None:
            logo_url = branding_api.get_logo_url(is_secure)
            logo_size = ''
        
        
        __M_writer(u'\n            <img class="')
        __M_writer(filters.html_escape(filters.decode.utf8(logo_size)))
        __M_writer(u'" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(logo_url)))
        __M_writer(u'" alt="')
        __M_writer(filters.html_escape(filters.decode.utf8(_("{platform_name} Home Page").format(platform_name=static.get_platform_name()))))
        __M_writer(u'"/>\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_top(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        def navigation_top():
            return render_navigation_top(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_other_global_links(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        course = _import_ns.get('course', context.get('course', UNDEFINED))
        def navigation_other_global_links():
            return render_navigation_other_global_links(context)
        login_query = _import_ns.get('login_query', context.get('login_query', UNDEFINED))
        settings = _import_ns.get('settings', context.get('settings', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(u'\n')
        if not settings.FEATURES['DISABLE_LOGIN_BUTTON']:
            if settings.FEATURES.get('ENABLE_COURSE_DISCOVERY'):
                __M_writer(u'              <li class="item nav-global-05">\n                <a class="btn" href="/courses">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Explore Courses"))))
                __M_writer(u'</a>\n              </li>\n')
            if course and settings.FEATURES.get('RESTRICT_ENROLL_BY_REG_METHOD') and course.enrollment_domain:
                __M_writer(u'              <li class="item nav-global-04">\n                <a class="btn-neutral" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(reverse('course-specific-register', args=[course.id.to_deprecated_string()]))))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Register"))))
                __M_writer(u'</a>\n              </li>\n')
            else:
                __M_writer(u'              <li class="item nav-global-04">\n                <a class="btn-neutral" href="/register')
                __M_writer(filters.html_escape(filters.decode.utf8(login_query())))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Register"))))
                __M_writer(u'</a>\n              </li>\n')
        __M_writer(u'        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        def js_extra():
            return render_js_extra(context)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n    ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                __M_writer(u'\n    HeaderFactory();\n    ')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module(class_name=u'HeaderFactory',module_name=u'js/header_factory'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n    ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_global_links_authenticated(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b85a1210')._populate(_import_ns, [u'login_query'])
        marketing_link = _import_ns.get('marketing_link', context.get('marketing_link', UNDEFINED))
        settings = _import_ns.get('settings', context.get('settings', UNDEFINED))
        show_program_listing = _import_ns.get('show_program_listing', context.get('show_program_listing', UNDEFINED))
        request = _import_ns.get('request', context.get('request', UNDEFINED))
        user = _import_ns.get('user', context.get('user', UNDEFINED))
        def navigation_global_links_authenticated():
            return render_navigation_global_links_authenticated(context)
        __M_writer = context.writer()
        __M_writer(u'\n')
        if settings.FEATURES.get('COURSES_ARE_BROWSABLE') and not show_program_listing:
            __M_writer(u'            <li class="item nav-global-01">\n              <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('COURSES'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Explore courses'))))
            __M_writer(u'</a>\n            </li>\n')
        if show_program_listing:
            __M_writer(u'            <li class="tab-nav-item">\n              <a class="')
            __M_writer(filters.html_escape(filters.decode.utf8('active ' if reverse('dashboard') == request.path else '')))
            __M_writer(u'tab-nav-link" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('dashboard'))))
            __M_writer(u'">\n                ')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Courses"))))
            __M_writer(u'\n              </a>\n            </li>\n            <li class="tab-nav-item">\n              <a class="')
            __M_writer(filters.html_escape(filters.decode.utf8('active ' if reverse('program_listing_view') in request.path else '')))
            __M_writer(u'tab-nav-link" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('program_listing_view'))))
            __M_writer(u'">\n                ')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Programs"))))
            __M_writer(u'\n              </a>\n            </li>\n')
        if settings.FEATURES.get('ENABLE_SYSADMIN_DASHBOARD','') and user.is_staff:
            __M_writer(u'            <li class="item">\n')
            __M_writer(u'              <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('sysadmin'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Sysadmin"))))
            __M_writer(u'</a>\n            </li>\n')
        __M_writer(u'        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 5, "38": 3, "41": 4, "44": 2, "64": 32, "65": 33, "76": 40, "77": 41, "78": 42, "79": 45, "80": 45, "104": 2, "105": 3, "106": 4, "107": 18, "112": 21, "113": 24, "114": 25, "119": 29, "120": 31, "122": 49, "123": 50, "124": 50, "125": 51, "126": 51, "127": 53, "128": 53, "133": 63, "134": 67, "135": 68, "136": 68, "137": 68, "138": 69, "139": 69, "140": 70, "150": 76, "151": 77, "152": 77, "153": 79, "154": 80, "155": 81, "160": 106, "161": 109, "162": 109, "163": 111, "164": 111, "165": 113, "166": 113, "167": 115, "168": 116, "169": 118, "170": 118, "171": 119, "172": 119, "173": 124, "174": 125, "179": 140, "184": 159, "189": 173, "190": 176, "191": 177, "192": 177, "196": 177, "197": 178, "198": 179, "199": 182, "200": 182, "201": 183, "202": 184, "203": 184, "204": 184, "205": 185, "206": 186, "207": 186, "208": 186, "209": 188, "210": 188, "211": 188, "212": 190, "213": 191, "214": 192, "215": 192, "216": 192, "217": 192, "218": 192, "219": 193, "220": 194, "221": 194, "222": 194, "223": 194, "224": 194, "225": 197, "226": 204, "227": 206, "228": 207, "229": 208, "235": 213, "236": 216, "237": 217, "238": 217, "244": 163, "255": 163, "256": 165, "257": 166, "258": 167, "259": 167, "260": 167, "261": 167, "262": 167, "263": 167, "264": 168, "265": 169, "266": 169, "267": 169, "268": 169, "269": 169, "270": 172, "276": 126, "287": 126, "288": 127, "289": 128, "290": 129, "291": 129, "292": 129, "293": 129, "294": 131, "295": 132, "296": 133, "297": 133, "298": 133, "299": 133, "300": 136, "301": 137, "302": 137, "303": 137, "304": 137, "305": 140, "311": 54, "322": 54, "323": 55, "331": 61, "332": 62, "333": 62, "334": 62, "335": 62, "336": 62, "337": 62, "343": 21, "356": 142, "367": 142, "368": 143, "369": 144, "370": 145, "371": 146, "372": 146, "373": 149, "374": 150, "375": 151, "376": 151, "377": 151, "378": 151, "379": 153, "380": 154, "381": 155, "382": 155, "383": 155, "384": 155, "385": 159, "391": 25, "400": 25, "404": 26, "409": 26, "412": 28, "418": 82, "431": 82, "432": 83, "433": 84, "434": 85, "435": 85, "436": 85, "437": 85, "438": 88, "439": 89, "440": 90, "441": 90, "442": 90, "443": 90, "444": 91, "445": 91, "446": 95, "447": 95, "448": 95, "449": 95, "450": 96, "451": 96, "452": 100, "453": 101, "454": 103, "455": 103, "456": 103, "457": 103, "458": 103, "459": 106, "465": 459}, "uri": "navigation.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/navigation.html"}
__M_END_METADATA
"""
