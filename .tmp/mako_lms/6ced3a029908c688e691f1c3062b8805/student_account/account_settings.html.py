# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497941559.331467
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/student_account/account_settings.html'
_template_uri = 'student_account/account_settings.html'
_source_encoding = 'utf-8'
_exports = [u'pagetitle', u'headextra', u'js_extra', 'online_help_token']



import json

from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.translation import ugettext as _

from openedx.core.djangolib.js_utils import dump_js_escaped_json, js_escaped_string


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'/static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        def headextra():
            return render_headextra(context._locals(__M_locals))
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        duplicate_provider = context.get('duplicate_provider', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\n')
        __M_writer(u'\n\n<!--')
        __M_writer(u'-->\n\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n\n')
        if duplicate_provider:
            __M_writer(u'    <section>\n        ')
            runtime._include_file(context, u'/dashboard/_dashboard_third_party_error.html', _template_uri)
            __M_writer(u'\n    </section>\n')
        __M_writer(u'\n<div class="wrapper-account-settings"></div>\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'headextra'):
            context['self'].headextra(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def pagetitle():
            return render_pagetitle(context)
        __M_writer = context.writer()
        __M_writer(filters.html_escape(filters.decode.utf8(_("Account Settings"))))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_headextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def headextra():
            return render_headextra(context)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n    ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-course'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def js_extra():
            return render_js_extra(context)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        def ccall(caller):
            def body():
                user_preferences_api_url = context.get('user_preferences_api_url', UNDEFINED)
                fields = context.get('fields', UNDEFINED)
                auth = context.get('auth', UNDEFINED)
                user = context.get('user', UNDEFINED)
                user_accounts_api_url = context.get('user_accounts_api_url', UNDEFINED)
                order_history = context.get('order_history', UNDEFINED)
                __M_writer = context.writer()
                __M_writer(u'\n    var fieldsData = ')
                __M_writer(dump_js_escaped_json( fields ))
                __M_writer(u',\n    ordersHistoryData = ')
                __M_writer(dump_js_escaped_json( order_history ))
                __M_writer(u',\n    authData = ')
                __M_writer(dump_js_escaped_json( auth ))
                __M_writer(u",\n    platformName = '")
                __M_writer(js_escaped_string( static.get_platform_name() ))
                __M_writer(u"';\n\n    AccountSettingsFactory(\n        fieldsData,\n        ordersHistoryData,\n        authData,\n        '")
                __M_writer(js_escaped_string( user_accounts_api_url ))
                __M_writer(u"',\n        '")
                __M_writer(js_escaped_string( user_preferences_api_url ))
                __M_writer(u"',\n        ")
                __M_writer(dump_js_escaped_json( user.id ))
                __M_writer(u',\n        platformName\n    );\n')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module(class_name=u'AccountSettingsFactory',module_name=u'js/student_account/views/account_settings_factory'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_online_help_token(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        return "learneraccountsettings" 
        
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"136": 34, "137": 35, "138": 35, "139": 36, "140": 36, "141": 37, "142": 37, "143": 38, "16": 3, "145": 44, "146": 44, "147": 45, "148": 45, "149": 46, "150": 46, "155": 34, "158": 49, "33": 17, "164": 16, "39": 1, "168": 16, "175": 168, "52": 1, "53": 11, "54": 13, "55": 15, "56": 16, "57": 17, "62": 19, "63": 21, "64": 22, "65": 23, "66": 23, "67": 26, "72": 31, "77": 50, "83": 19, "89": 19, "95": 29, "144": 38, "102": 29, "110": 30, "113": 30, "119": 33, "126": 33}, "uri": "student_account/account_settings.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/student_account/account_settings.html"}
__M_END_METADATA
"""
