# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946365.805151
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/seq_module.html'
_template_uri = 'seq_module.html'
_source_encoding = 'utf-8'
_exports = []


from django.utils.translation import ugettext as _ 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        prev_url = context.get('prev_url', UNDEFINED)
        banner_text = context.get('banner_text', UNDEFINED)
        items = context.get('items', UNDEFINED)
        next_url = context.get('next_url', UNDEFINED)
        ajax_url = context.get('ajax_url', UNDEFINED)
        enumerate = context.get('enumerate', UNDEFINED)
        item_id = context.get('item_id', UNDEFINED)
        position = context.get('position', UNDEFINED)
        element_id = context.get('element_id', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n<div id="sequence_')
        __M_writer(filters.html_escape(filters.decode.utf8(element_id)))
        __M_writer(u'" class="sequence" data-id="')
        __M_writer(filters.html_escape(filters.decode.utf8(item_id)))
        __M_writer(u'" data-position="')
        __M_writer(filters.html_escape(filters.decode.utf8(position)))
        __M_writer(u'" data-ajax-url="')
        __M_writer(filters.html_escape(filters.decode.utf8(ajax_url)))
        __M_writer(u'" data-next-url="')
        __M_writer(filters.html_escape(filters.decode.utf8(next_url)))
        __M_writer(u'" data-prev-url="')
        __M_writer(filters.html_escape(filters.decode.utf8(prev_url)))
        __M_writer(u'">\n')
        if banner_text:
            __M_writer(u'    <div class="pattern-library-shim alert alert-information subsection-header" tabindex="-1">\n      <span class="pattern-library-shim icon alert-icon fa fa-bullhorn" aria-hidden="true"></span>\n      <div class="pattern-library-shim alert-message">\n        <p class="pattern-library-shim alert-copy">\n          ')
            __M_writer(filters.html_escape(filters.decode.utf8(banner_text)))
            __M_writer(u'\n        </p>\n      </div>\n    </div>\n')
        __M_writer(u'  <div class="sequence-nav">\n    <button class="sequence-nav-button button-previous">\n      <span class="icon fa fa-chevron-prev" aria-hidden="true"></span>\n      <span>')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Previous'))))
        __M_writer(u'</span>\n    </button>\n    <nav class="sequence-list-wrapper" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Unit'))))
        __M_writer(u'">\n      <ol id="sequence-list">\n')
        for idx, item in enumerate(items):
            __M_writer(u'        <li>\n        <button class="seq_')
            __M_writer(filters.html_escape(filters.decode.utf8(item['type'])))
            __M_writer(u' inactive nav-item"\n           data-id="')
            __M_writer(filters.html_escape(filters.decode.utf8(item['id'])))
            __M_writer(u'"\n           data-element="')
            __M_writer(filters.html_escape(filters.decode.utf8(idx+1)))
            __M_writer(u'"\n           data-page-title="')
            __M_writer(filters.html_escape(filters.decode.utf8(item['page_title'])))
            __M_writer(u'"\n           data-path="')
            __M_writer(filters.html_escape(filters.decode.utf8(item['path'])))
            __M_writer(u'"\n           id="tab_')
            __M_writer(filters.html_escape(filters.decode.utf8(idx)))
            __M_writer(u'">\n            <span class="icon fa seq_')
            __M_writer(filters.html_escape(filters.decode.utf8(item['type'])))
            __M_writer(u'" aria-hidden="true"></span>\n            <span class="fa fa-fw fa-bookmark bookmark-icon ')
            __M_writer(filters.html_escape(filters.decode.utf8("is-hidden" if not item['bookmarked'] else "bookmarked")))
            __M_writer(u'" aria-hidden="true"></span>\n            <div class="sequence-tooltip sr"><span class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(item['type'])))
            __M_writer(u'&nbsp;</span>')
            __M_writer(filters.html_escape(filters.decode.utf8(item['page_title'])))
            __M_writer(u'<span class="sr bookmark-icon-sr">&nbsp;')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Bookmarked") if item['bookmarked'] else "")))
            __M_writer(u'</span></div>\n          </button>\n        </li>\n')
        __M_writer(u'      </ol>\n    </nav>\n    <button class="sequence-nav-button button-next">\n      <span>')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Next'))))
        __M_writer(u'</span>\n      <span class="icon fa fa-chevron-next" aria-hidden="true"></span>\n    </button>\n  </div>\n\n  <div class="sr-is-focusable" tabindex="-1"></div>\n\n')
        for idx, item in enumerate(items):
            __M_writer(u'  <div id="seq_contents_')
            __M_writer(filters.html_escape(filters.decode.utf8(idx)))
            __M_writer(u'"\n    aria-labelledby="tab_')
            __M_writer(filters.html_escape(filters.decode.utf8(idx)))
            __M_writer(u'"\n    aria-hidden="true"\n    class="seq_contents tex2jax_ignore asciimath2jax_ignore">\n    ')
            __M_writer(filters.html_escape(filters.decode.utf8(item['content'])))
            __M_writer(u'\n  </div>\n')
        __M_writer(u'  <div id="seq_content"></div>\n\n  <nav class="sequence-bottom" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Section'))))
        __M_writer(u'">\n    <button class="sequence-nav-button button-previous">\n      <span class="icon fa fa-chevron-prev" aria-hidden="true"></span>\n      <span>')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Previous'))))
        __M_writer(u'</span>\n    </button>\n    <button class="sequence-nav-button button-next">\n      <span>')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Next'))))
        __M_writer(u'</span>\n      <span class="icon fa fa-chevron-next" aria-hidden="true"></span>\n    </button>\n  </nav>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "18": 1, "32": 1, "33": 2, "34": 4, "35": 4, "36": 4, "37": 4, "38": 4, "39": 4, "40": 4, "41": 4, "42": 4, "43": 4, "44": 4, "45": 4, "46": 5, "47": 6, "48": 10, "49": 10, "50": 15, "51": 18, "52": 18, "53": 20, "54": 20, "55": 22, "56": 23, "57": 24, "58": 24, "59": 25, "60": 25, "61": 26, "62": 26, "63": 27, "64": 27, "65": 28, "66": 28, "67": 29, "68": 29, "69": 30, "70": 30, "71": 31, "72": 31, "73": 32, "74": 32, "75": 32, "76": 32, "77": 32, "78": 32, "79": 36, "80": 39, "81": 39, "82": 46, "83": 47, "84": 47, "85": 47, "86": 48, "87": 48, "88": 51, "89": 51, "90": 54, "91": 56, "92": 56, "93": 59, "94": 59, "95": 62, "96": 62, "102": 96}, "uri": "seq_module.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/seq_module.html"}
__M_END_METADATA
"""
