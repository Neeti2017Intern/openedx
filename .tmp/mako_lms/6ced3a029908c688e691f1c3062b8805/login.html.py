# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497848936.531624
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/login.html'
_template_uri = 'login.html'
_source_encoding = 'utf-8'
_exports = [u'pagetitle', u'js_extra', u'bodyclass']



from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
import third_party_auth
from third_party_auth import provider, pipeline


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        platform_name = context.get('platform_name', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        third_party_auth_error = context.get('third_party_auth_error', UNDEFINED)
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        pipeline_url = context.get('pipeline_url', UNDEFINED)
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        pipeline_running = context.get('pipeline_running', UNDEFINED)
        login_redirect_url = context.get('login_redirect_url', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n')
        runtime._include_file(context, u'forgot_password_modal.html', _template_uri)
        __M_writer(u'\n\n<section class="introduction">\n  <header>\n    <h1 class="title">\n      <span class="title-super">')
        __M_writer(filters.decode.utf8(_("Please log in")))
        __M_writer(u'</span>\n      <span class="title-sub">')
        __M_writer(filters.decode.utf8(_("to access your account and courses")))
        __M_writer(u'</span>\n    </h1>\n  </header>\n</section>\n\n<section class="login container">\n  <section role="main" class="content">\n    <form role="form" id="login-form" method="post" data-remote="true" action="/login_ajax" novalidate>\n\n      <!-- status messages -->\n      <div role="alert" class="status message">\n        <h3 class="message-title">')
        __M_writer(filters.decode.utf8(_("We're Sorry, {platform_name} accounts are unavailable currently").format(platform_name=platform_name)))
        __M_writer(u'</h3>\n      </div>\n\n      <div role="alert" class="status message submission-error" tabindex="-1">\n        <h3 class="message-title">')
        __M_writer(filters.decode.utf8(_("We couldn't log you in.")))
        __M_writer(u' </h3>\n        <ul class="message-copy">\n          <li>')
        __M_writer(filters.decode.utf8(_("Your email or password is incorrect")))
        __M_writer(u'</li>\n        </ul>\n      </div>\n\n      <div role="alert" class="third-party-signin message" tabindex="-1">\n        <p class="instructions"> </p>\n      </div>\n\n')
        if third_party_auth_error:
            __M_writer(u'        <div role="alert" class="status message third-party-auth-error is-shown" tabindex="-1">\n          <h3 class="message-title">')
            __M_writer(filters.decode.utf8(_("An error occurred when signing you in to {platform_name}.").format(platform_name=platform_name)))
            __M_writer(u' </h3>\n          <ul class="message-copy">\n            <li>')
            __M_writer(filters.decode.utf8(third_party_auth_error))
            __M_writer(u'</li>\n          </ul>\n        </div>\n')
        __M_writer(u'\n      <p class="instructions sr">\n        ')
        __M_writer(filters.decode.utf8(_('Please provide the following information to log into your {platform_name} account. Required fields are noted by <strong class="indicator">bold text and an asterisk (*)</strong>.').format(platform_name=platform_name)))
        __M_writer(u'\n      </p>\n\n      <div class="group group-form group-form-requiredinformation">\n        <h2 class="sr">')
        __M_writer(filters.decode.utf8(_('Required Information')))
        __M_writer(u'</h2>\n\n        <ol class="list-input">\n          <li class="field required text" id="field-email">\n            <label for="email">')
        __M_writer(filters.decode.utf8(_('E-mail')))
        __M_writer(u'</label>\n            <input class="" id="email" type="email" name="email" value="" placeholder="')
        __M_writer(filters.decode.utf8(_('example: username@domain.com')))
        __M_writer(u'" required aria-required="true" aria-described-by="email-tip" />\n            <span class="tip tip-input" id="email-tip">')
        __M_writer(filters.decode.utf8(_("This is the e-mail address you used to register with {platform}").format(platform=platform_name)))
        __M_writer(u'</span>\n          </li>\n          <li class="field required password" id="field-password">\n            <label for="password">')
        __M_writer(filters.decode.utf8(_('Password')))
        __M_writer(u'</label>\n            <input id="password" type="password" name="password" value="" required aria-required="true" />\n            <span class="tip tip-input">\n              <a href="#forgot-password-modal" rel="leanModal" class="pwd-reset action action-forgotpw" id="forgot-password-link" role="button" aria-haspopup="true">')
        __M_writer(filters.decode.utf8(_('Forgot password?')))
        __M_writer(u'</a>\n            </span>\n          </li>\n        </ol>\n      </div>\n\n      <div class="group group-form group-form-secondary group-form-accountpreferences">\n        <h2 class="sr">')
        __M_writer(filters.decode.utf8(_('Account Preferences')))
        __M_writer(u'</h2>\n\n        <ol class="list-input">\n          <li class="field required checkbox" id="field-remember">\n            <input id="remember-yes" type="checkbox" name="remember" value="true" />\n            <label for="remember-yes">')
        __M_writer(filters.decode.utf8(_('Remember me')))
        __M_writer(u'</label>\n          </li>\n        </ol>\n      </div>\n\n      <div class="form-actions">\n        <button name="submit" type="submit" id="submit" class="action action-primary action-update login-button"></button>\n      </div>\n    </form>\n\n')
        if third_party_auth.is_enabled():
            __M_writer(u'\n      <span class="deco-divider">\n')
            __M_writer(u'        <span class="copy">')
            __M_writer(filters.decode.utf8(_('or')))
            __M_writer(u'</span>\n      </span>\n\n    <div class="form-actions form-third-party-auth">\n\n')
            for enabled in provider.Registry.displayed_for_login():
                __M_writer(u'      <button type="submit" class="button button-primary button-')
                __M_writer(filters.decode.utf8(enabled.provider_id))
                __M_writer(u' login-')
                __M_writer(filters.decode.utf8(enabled.provider_id))
                __M_writer(u'" onclick="thirdPartySignin(event, \'')
                __M_writer(filters.decode.utf8(pipeline_url[enabled.provider_id]))
                __M_writer(u'\');">\n')
                if enabled.icon_class:
                    __M_writer(u'        <span class="icon fa ')
                    __M_writer(filters.decode.utf8(enabled.icon_class))
                    __M_writer(u'" aria-hidden="true"></span>\n')
                else:
                    __M_writer(u'        <span class="icon" aria-hidden="true"><img class="icon-image" src="')
                    __M_writer(filters.decode.utf8(enabled.icon_image.url))
                    __M_writer(u'" alt="')
                    __M_writer(filters.decode.utf8(enabled.name))
                    __M_writer(u' icon" /></span>\n')
                __M_writer(u'        ')
                __M_writer(filters.decode.utf8(_('Sign in with {provider_name}').format(provider_name=enabled.name)))
                __M_writer(u'\n      </button>\n')
            __M_writer(u'\n    </div>\n\n')
        __M_writer(u'\n  </section>\n\n  <aside role="complementary">\n\n')

  # allow for theming overrides on the registration sidebars, otherwise default to pre-existing ones
        sidebar_file = static.get_template_path('login-sidebar.html')
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['sidebar_file'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n    ')
        runtime._include_file(context, (sidebar_file), _template_uri)
        __M_writer(u'\n\n  </aside>\n  </section>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def pagetitle():
            return render_pagetitle(context)
        platform_name = context.get('platform_name', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(filters.decode.utf8(_("Log into your {platform_name} Account").format(platform_name=platform_name)))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        platform_name = context.get('platform_name', UNDEFINED)
        def js_extra():
            return render_js_extra(context)
        login_redirect_url = context.get('login_redirect_url', UNDEFINED)
        pipeline_running = context.get('pipeline_running', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n  <script type="text/javascript">\n    $(function() {\n\n      // adding js class for styling with accessibility in mind\n      $(\'body\').addClass(\'js\');\n\n      // show forgot password modal UI if URL contains matching DOM ID\n      if ($.deparam.fragment()[\'forgot-password-modal\'] !== undefined) {\n        $(\'.pwd-reset\').click();\n      }\n\n      // new window/tab opening\n      $(\'a[rel="external"], a[class="new-vp"]\')\n      .click( function() {\n        window.open( $(this).attr(\'href\') );\n        return false;\n      });\n\n      // form field label styling on focus\n      $("form :input").focus(function() {\n        $("label[for=\'" + this.id + "\']").parent().addClass("is-focused");\n      }).blur(function() {\n        $("label").parent().removeClass("is-focused");\n      });\n\n      $("#email").focus();\n    });\n\n    (function() {\n      toggleSubmitButton(true);\n\n      $(\'#login-form\').on(\'submit\', function() {\n        toggleSubmitButton(false);\n      });\n\n      $(\'#login-form\').on(\'ajax:error\', function(event, request, status_string) {\n        toggleSubmitButton(true);\n\n        if (request.status === 403) {\n          $(\'.message.submission-error\').removeClass(\'is-shown\');\n          $(\'.third-party-signin.message\').addClass(\'is-shown\').focus();\n          $(\'.third-party-signin.message .instructions\').html(request.responseText);\n        } else {\n          $(\'.third-party-signin.message\').removeClass(\'is-shown\');\n          $(\'.message.submission-error\').addClass(\'is-shown\').focus();\n          $(\'.message.submission-error\').html(gettext("Your request could not be completed.  Please try again."));\n        }\n      });\n\n      $(\'#login-form\').on(\'ajax:success\', function(event, json, xhr) {\n        if(json.success) {\n          var nextUrl = "')
        __M_writer(filters.decode.utf8(login_redirect_url))
        __M_writer(u'";\n          if (json.redirect_url) {\n            nextUrl = json.redirect_url; // Most likely third party auth completion. This trumps \'nextUrl\' above.\n          }\n          if (!isExternal(nextUrl)) {\n            location.href=nextUrl;\n          } else {\n            location.href="')
        __M_writer(filters.decode.utf8(reverse('dashboard')))
        __M_writer(u'";\n          }\n        } else if(json.hasOwnProperty(\'redirect\')) {\n          // Shibboleth authentication redirect requested by the server:\n          var u=decodeURI(window.location.search);\n          if (!isExternal(json.redirect)) { // a paranoid check.  Our server is the one providing json.redirect\n              location.href=json.redirect+u;\n          } // else we just remain on this page, which is fine since this particular path implies a login failure\n            // that has been generated via packet tampering (json.redirect has been messed with).\n        } else {\n          toggleSubmitButton(true);\n          $(\'.message.submission-error\').addClass(\'is-shown\').focus();\n          $(\'.message.submission-error .message-copy\').html(json.value);\n        }\n      });\n      $("#forgot-password-link").click(function() {\n        $("#forgot-password-modal").show();\n        $("#forgot-password-modal .close-modal").focus();\n      });\n\n    })(this);\n\n    function toggleSubmitButton(enable) {\n      var $submitButton = $(\'form .form-actions #submit\');\n\n      if(enable) {\n        $submitButton.\n          removeClass(\'is-disabled\').\n          attr(\'aria-disabled\', false).\n          removeProp(\'disabled\').\n          html("')
        __M_writer(filters.decode.utf8(_('Log into My {platform_name} Account').format(platform_name=platform_name)))
        __M_writer(u" <span class='orn-plus'>+</span> ")
        __M_writer(filters.decode.utf8(_('Access My Courses')))
        __M_writer(u'");\n      }\n      else {\n        $submitButton.\n          addClass(\'is-disabled\').\n          prop(\'disabled\', true).\n          text("')
        __M_writer(filters.decode.utf8(_(u'Processing your account information')))
        __M_writer(u'");\n      }\n    }\n\n    function thirdPartySignin(event, url) {\n      event.preventDefault();\n      window.location.href = url;\n    }\n\n    (function post_form_if_pipeline_running(pipeline_running) {\n       // If the pipeline is running, the user has already authenticated via a\n       // third-party provider. We want to invoke /login_ajax to loop in the\n       // code that does logging and sets cookies on the request. It is most\n       // consistent to do that by using the same mechanism that is used when\n       // the use does first-party sign-in: POSTing the sign-in form.\n       if (pipeline_running) {\n         $(\'#login-form\').submit();\n       }\n    })(')
        __M_writer(filters.decode.utf8(pipeline_running))
        __M_writer(u')\n  </script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        __M_writer(u'view-login')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 4, "30": 2, "36": 0, "53": 1, "54": 2, "55": 9, "60": 11, "65": 13, "70": 130, "71": 132, "72": 132, "73": 137, "74": 137, "75": 138, "76": 138, "77": 149, "78": 149, "79": 153, "80": 153, "81": 155, "82": 155, "83": 163, "84": 164, "85": 165, "86": 165, "87": 167, "88": 167, "89": 171, "90": 173, "91": 173, "92": 177, "93": 177, "94": 181, "95": 181, "96": 182, "97": 182, "98": 183, "99": 183, "100": 186, "101": 186, "102": 189, "103": 189, "104": 196, "105": 196, "106": 201, "107": 201, "108": 211, "109": 212, "110": 216, "111": 216, "112": 216, "113": 221, "114": 223, "115": 223, "116": 223, "117": 223, "118": 223, "119": 223, "120": 223, "121": 224, "122": 225, "123": 225, "124": 225, "125": 226, "126": 227, "127": 227, "128": 227, "129": 227, "130": 227, "131": 229, "132": 229, "133": 229, "134": 232, "135": 236, "136": 241, "143": 244, "144": 246, "145": 246, "151": 11, "158": 11, "164": 15, "173": 15, "174": 67, "175": 67, "176": 74, "177": 74, "178": 104, "179": 104, "180": 104, "181": 104, "182": 110, "183": 110, "184": 128, "185": 128, "191": 13, "197": 13, "203": 197}, "uri": "login.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/login.html"}
__M_END_METADATA
"""
