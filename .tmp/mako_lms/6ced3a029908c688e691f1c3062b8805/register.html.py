# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849599.355324
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/register.html'
_template_uri = 'register.html'
_source_encoding = 'utf-8'
_exports = [u'pagetitle', u'js_extra', u'bodyclass']



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.utils import html
from django_countries import countries
from student.models import UserProfile
from datetime import date
import third_party_auth
from third_party_auth import pipeline, provider
import calendar


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

    ns = runtime.TemplateNamespace('__anon_0x7fa1b84002d0', context._clean_inheritance_tokens(), templateuri=u'main.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, '__anon_0x7fa1b84002d0')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b84002d0')._populate(_import_ns, [u'login_query'])
        platform_name = _import_ns.get('platform_name', context.get('platform_name', UNDEFINED))
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        login_redirect_url = _import_ns.get('login_redirect_url', context.get('login_redirect_url', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n<section class="introduction">\n  <header>\n    <h1 class="title">\n      <span class="title-super">')
        __M_writer(filters.decode.utf8(_("Welcome!")))
        __M_writer(u'</span>\n      <span class="title-sub">')
        __M_writer(filters.decode.utf8(_("Register below to create your {platform_name} account").format(platform_name=platform_name)))
        __M_writer(u'</span>\n    </h1>\n  </header>\n</section>\n\n<section class="register container">\n  <section role="main" class="content">\n    <form role="form" id="register-form" method="post" data-remote="true" action="/create_account" novalidate>\n')

  # allow for theming override on the registration form
        registration_form = static.get_template_path('register-form.html')
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['registration_form'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n      ')
        runtime._include_file(context, (registration_form), _template_uri)
        __M_writer(u'\n    </form>\n  </section>\n\n  <aside role="complementary">\n\n')

  # allow for theming overrides on the registration sidebars, otherwise default to pre-existing ones
        sidebar_file = static.get_template_path('register-sidebar.html')
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['sidebar_file'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n    ')
        runtime._include_file(context, (sidebar_file), _template_uri)
        __M_writer(u'\n\n  </aside>\n</section>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b84002d0')._populate(_import_ns, [u'login_query'])
        def pagetitle():
            return render_pagetitle(context)
        platform_name = _import_ns.get('platform_name', context.get('platform_name', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(filters.decode.utf8(_("Register for {platform_name}").format(platform_name=platform_name)))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b84002d0')._populate(_import_ns, [u'login_query'])
        platform_name = _import_ns.get('platform_name', context.get('platform_name', UNDEFINED))
        def js_extra():
            return render_js_extra(context)
        login_redirect_url = _import_ns.get('login_redirect_url', context.get('login_redirect_url', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(u'\n  <script type="text/javascript">\n    $(function() {\n\n      // adding js class for styling with accessibility in mind\n      $(\'body\').addClass(\'js\');\n\n      // new window/tab opening\n      $(\'a[rel="external"], a[class="new-vp"]\')\n      .click( function() {\n      window.open( $(this).attr(\'href\') );\n      return false;\n      });\n\n      // form field label styling on focus\n      $("form :input").focus(function() {\n        $("label[for=\'" + this.id + "\']").parent().addClass("is-focused");\n      }).blur(function() {\n        $("label").parent().removeClass("is-focused");\n      });\n\n    });\n\n    (function() {\n      toggleSubmitButton(true);\n\n      $(\'#register-form\').on(\'submit\', function() {\n        toggleSubmitButton(false);\n      });\n\n      $(\'#register-form\').on(\'ajax:error\', function() {\n        toggleSubmitButton(true);\n      });\n\n      $(\'#register-form\').on(\'ajax:success\', function(event, json, xhr) {\n        var nextUrl = "')
        __M_writer(filters.decode.utf8(login_redirect_url))
        __M_writer(u'";\n        if (json.redirect_url) {\n          nextUrl = json.redirect_url; // Most likely third party auth completion. This trumps \'nextUrl\' above.\n        }\n        if (!isExternal(nextUrl)) {\n          location.href=nextUrl;\n        } else {\n          location.href="')
        __M_writer(filters.decode.utf8(reverse('dashboard')))
        __M_writer(u'";\n        }\n      });\n\n      $(\'#register-form\').on(\'ajax:error\', function(event, jqXHR, textStatus) {\n        toggleSubmitButton(true);\n        json = $.parseJSON(jqXHR.responseText);\n        $(\'.status.message.submission-error\').addClass(\'is-shown\').focus();\n        $(\'.status.message.submission-error .message-copy\').html(json.value).stop().css("display", "block");\n        $(".field-error").removeClass(\'field-error\');\n        $("[data-field=\'"+json.field+"\']").addClass(\'field-error\')\n      });\n    })(this);\n\n    function thirdPartySignin(event, url) {\n      event.preventDefault();\n      window.location.href = url;\n    }\n\n    function toggleSubmitButton(enable) {\n      var $submitButton = $(\'form .form-actions #submit\');\n\n      if(enable) {\n        $submitButton.\n          removeClass(\'is-disabled\').\n          attr(\'aria-disabled\', false).\n          removeProp(\'disabled\').\n          html("')
        __M_writer(filters.decode.utf8(_('Create My {platform_name} Account').format(platform_name=platform_name)))
        __M_writer(u'");\n      }\n      else {\n        $submitButton.\n          addClass(\'is-disabled\').\n          prop(\'disabled\', true).\n          text("')
        __M_writer(filters.decode.utf8(_('Processing your account information')))
        __M_writer(u'");\n      }\n    }\n  </script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7fa1b84002d0')._populate(_import_ns, [u'login_query'])
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        __M_writer(u'view-register')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"133": 20, "134": 55, "135": 55, "136": 62, "137": 62, "138": 89, "139": 89, "140": 95, "141": 95, "16": 4, "147": 18, "155": 18, "161": 155, "35": 2, "38": 3, "44": 0, "60": 1, "61": 2, "62": 3, "63": 14, "68": 16, "73": 18, "78": 99, "79": 104, "80": 104, "81": 105, "82": 105, "83": 113, "90": 116, "91": 117, "92": 117, "93": 123, "100": 126, "101": 128, "102": 128, "108": 16, "117": 16, "123": 20}, "uri": "register.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/register.html"}
__M_END_METADATA
"""
