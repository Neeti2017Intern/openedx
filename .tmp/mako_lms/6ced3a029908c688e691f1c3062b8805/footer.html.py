# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497848942.654817
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/footer.html'
_template_uri = u'footer.html'
_source_encoding = 'utf-8'
_exports = []



from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from branding.api import get_footer


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        include_dependencies = context.get('include_dependencies', UNDEFINED)
        footer_css_urls = context.get('footer_css_urls', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        bidi = context.get('bidi', UNDEFINED)
        enumerate = context.get('enumerate', UNDEFINED)
        is_secure = context.get('is_secure', UNDEFINED)
        hide_openedx_link = context.get('hide_openedx_link', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        footer = get_footer(is_secure=is_secure) 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['footer'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n')
        __M_writer(u'\n\n<div class="wrapper wrapper-footer">\n  <footer id="footer-openedx" class="grid-container"\n')
        if bidi:
            __M_writer(u'      dir=')
            __M_writer(filters.html_escape(filters.decode.utf8(bidi)))
            __M_writer(u'\n')
        __M_writer(u'  >\n    <div class="colophon">\n      <nav class="nav-colophon" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('About'))))
        __M_writer(u'">\n        <ol>\n')
        for item_num, link in enumerate(footer['navigation_links'], start=1):
            __M_writer(u'            <li class="nav-colophon-0')
            __M_writer(filters.html_escape(filters.decode.utf8(item_num)))
            __M_writer(u'">\n              <a id="')
            __M_writer(filters.html_escape(filters.decode.utf8(link['name'])))
            __M_writer(u'" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(link['url'])))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(link['title'])))
            __M_writer(u'</a>\n            </li>\n')
        __M_writer(u'        </ol>\n      </nav>\n\n      <div class="wrapper-logo">\n        <p>\n          <a href="/">\n')
        __M_writer(u'            <img alt="organization logo" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(footer['logo_image'])))
        __M_writer(u'">\n          </a>\n        </p>\n      </div>\n\n')
        __M_writer(u'      <p class="copyright">')
        __M_writer(filters.html_escape(filters.decode.utf8(footer['copyright'])))
        __M_writer(u'</p>\n\n      <nav class="nav-legal" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Legal'))))
        __M_writer(u'">\n        <ul>\n')
        for item_num, link in enumerate(footer['legal_links'], start=1):
            __M_writer(u'            <li class="nav-legal-0')
            __M_writer(filters.html_escape(filters.decode.utf8(item_num)))
            __M_writer(u'">\n              <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(link['url'])))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(link['title'])))
            __M_writer(u'</a>\n            </li>\n')
        __M_writer(u'        </ul>\n      </nav>\n    </div>\n\n')
        if not hide_openedx_link:
            __M_writer(u'    <div class="footer-about-openedx">\n      <p>\n        <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(footer['openedx_link']['url'])))
            __M_writer(u'">\n          <img src="')
            __M_writer(filters.html_escape(filters.decode.utf8(footer['openedx_link']['image'])))
            __M_writer(u'" alt="')
            __M_writer(filters.html_escape(filters.decode.utf8(footer['openedx_link']['title'])))
            __M_writer(u'" width="140" />\n        </a>\n      </p>\n    </div>\n')
        __M_writer(u'  </footer>\n</div>\n')
        if include_dependencies:
            __M_writer(u'  ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.js(group=u'base_vendor'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n  ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-vendor'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n  ')
            runtime._include_file(context, u'widgets/segment-io.html', _template_uri)
            __M_writer(u'\n  ')
            runtime._include_file(context, u'widgets/segment-io-footer.html', _template_uri)
            __M_writer(u'\n')
        if footer_css_urls:
            for url in footer_css_urls:
                __M_writer(u'    <link rel="stylesheet" type="text/css" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(url)))
                __M_writer(u'"></link>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"128": 83, "134": 128, "16": 3, "29": 9, "32": 2, "44": 2, "45": 7, "46": 8, "50": 8, "51": 9, "52": 16, "53": 17, "54": 17, "55": 17, "56": 19, "57": 21, "58": 21, "59": 23, "60": 24, "61": 24, "62": 24, "63": 25, "64": 25, "65": 25, "66": 25, "67": 25, "68": 25, "69": 28, "70": 41, "71": 41, "72": 41, "73": 47, "74": 47, "75": 47, "76": 49, "77": 49, "78": 51, "79": 52, "80": 52, "81": 52, "82": 53, "83": 53, "84": 53, "85": 53, "86": 56, "87": 64, "88": 65, "89": 67, "90": 67, "91": 68, "92": 68, "93": 68, "94": 68, "95": 73, "96": 75, "97": 76, "105": 76, "108": 76, "116": 77, "119": 77, "120": 78, "121": 78, "122": 79, "123": 79, "124": 81, "125": 82, "126": 83, "127": 83}, "uri": "footer.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/footer.html"}
__M_END_METADATA
"""
