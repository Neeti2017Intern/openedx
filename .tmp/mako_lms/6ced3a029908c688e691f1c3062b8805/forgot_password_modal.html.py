# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497848942.586378
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/forgot_password_modal.html'
_template_uri = u'forgot_password_modal.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n\n')
        __M_writer(u'\n\n<section id="forgot-password-modal" class="modal" role="dialog" tabindex="-1" aria-label="')
        __M_writer(filters.decode.utf8(_('Password Reset')))
        __M_writer(u'">\n  <div class="inner-wrapper">\n    <button class="close-modal">\n      <span class="icon fa fa-remove" aria-hidden="true"></span>\n      <span class="sr">\n')
        __M_writer(u'        ')
        __M_writer(filters.decode.utf8(_('Close')))
        __M_writer(u'\n      </span>\n    </button>\n\n    <div id="password-reset">\n      <header>\n        <h2>')
        __M_writer(filters.decode.utf8(_("Password Reset")))
        __M_writer(u'</h2>\n      </header>\n\n      <div class="instructions">\n        <p>')
        __M_writer(filters.decode.utf8(_("Please enter your e-mail address below, and we will e-mail instructions for setting a new password.")))
        __M_writer(u'</p>\n      </div>\n\n      <form id="pwd_reset_form" action="')
        __M_writer(filters.decode.utf8(reverse('password_reset')))
        __M_writer(u'" method="post" data-remote="true">\n        <fieldset class="group group-form group-form-requiredinformation">\n          <legend class="is-hidden">')
        __M_writer(filters.decode.utf8(_("Required Information")))
        __M_writer(u'</legend>\n\n          <ol class="list-input">\n            <li class="field required text" id="forgot-password-modal-field-email">\n              <label for="pwd_reset_email">')
        __M_writer(filters.decode.utf8(_("Your E-mail Address")))
        __M_writer(u'</label>\n              <input class="" id="pwd_reset_email" type="email" name="email" value="" placeholder="example: username@domain.com" aria-describedby="pwd_reset_email-tip" aria-required="true" />\n              <span class="tip tip-input" id="pwd_reset_email-tip">')
        __M_writer(filters.decode.utf8(_("This is the e-mail address you used to register with {platform}").format(platform=static.get_platform_name())))
        __M_writer(u'</span>\n            </li>\n          </ol>\n        </fieldset>\n\n        <div class="form-actions">\n          <button name="submit" type="submit" id="pwd_reset_button" class="action action-primary action-update">')
        __M_writer(filters.decode.utf8(_("Reset My Password")))
        __M_writer(u'</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</section>\n\n<script type="text/javascript">\n  (function() {\n    $(document).delegate(\'#pwd_reset_form\', \'ajax:success\', function(data, json, xhr) {\n      if(json.success) {\n        $("#password-reset").html(json.value);\n      } else {\n        if($(\'#pwd_error\').length == 0) {\n          $(\'#pwd_reset_form\').prepend(\'<div id="pwd_error" class="modal-form-error">')
        __M_writer(filters.decode.utf8(_("Email is incorrect.")))
        __M_writer(u'</div>\');\n        }\n        $(\'#pwd_error\').stop().css("display", "block");\n      }\n    });\n\n    // removing close link\'s default behavior\n    $(\'#login-modal .close-modal\').click(function(e) {\n     e.preventDefault();\n    });\n\n    var onModalClose = function() {\n      $("#forgot-password-modal").attr("aria-hidden", "true");\n      $("#forgot-password-link").focus();\n    };\n\n    var cycle_modal_tab = function(from_element_name, to_element_name) {\n      $(from_element_name).on(\'keydown\', function(e) {\n          var keyCode = e.keyCode || e.which;\n          var TAB_KEY = 9;  // 9 corresponds to the tab key\n          if (keyCode === TAB_KEY) {\n              e.preventDefault();\n              $(to_element_name).focus();\n          }\n      });\n    };\n    $("#forgot-password-modal .close-modal").click(onModalClose);\n    cycle_modal_tab("#forgot-password-modal .close-modal", "#pwd_reset_email");\n    cycle_modal_tab("#pwd_reset_email", "#pwd_reset_button");\n    cycle_modal_tab("#pwd_reset_button", "#forgot-password-modal .close-modal");\n\n    // Hitting the ESC key will exit the modal\n    $("#forgot-password-modal").on("keydown", function(e) {\n        var keyCode = e.keyCode || e.which;\n        // 27 is the ESC key\n        if (keyCode === 27) {\n            e.preventDefault();\n            $("#forgot-password-modal .close-modal").click();\n        }\n    });\n\n  })(this)\n</script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 3, "28": 1, "31": 0, "37": 1, "38": 6, "39": 8, "40": 8, "41": 14, "42": 14, "43": 14, "44": 20, "45": 20, "46": 24, "47": 24, "48": 27, "49": 27, "50": 29, "51": 29, "52": 33, "53": 33, "54": 35, "55": 35, "56": 41, "57": 41, "58": 55, "59": 55, "65": 59}, "uri": "forgot_password_modal.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/forgot_password_modal.html"}
__M_END_METADATA
"""
