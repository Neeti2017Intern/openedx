# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497848942.605051
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/login-sidebar.html'
_template_uri = 'login-sidebar.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        platform_name = context.get('platform_name', UNDEFINED)
        marketing_link = context.get('marketing_link', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\n<header>\n  <h2 class="sr">')
        __M_writer(filters.decode.utf8(_("Helpful Information")))
        __M_writer(u'</h2>\n</header>\n\n')
        if settings.FEATURES.get('AUTH_USE_OPENID'):
            __M_writer(u'<!-- <div class="cta cta-login-options-openid">\n  <h3>')
            __M_writer(filters.decode.utf8(_("Login via OpenID")))
            __M_writer(u'</h3>\n  <p>')
            __M_writer(filters.decode.utf8(_('You can now start learning with {platform_name} by logging in with your <a rel="external" href="http://openid.net/">OpenID account</a>.').format(platform_name=platform_name)))
            __M_writer(u'</p>\n  <a class="action action-login-openid" href="#">')
            __M_writer(filters.decode.utf8(_("Login via OpenID")))
            __M_writer(u'</a>\n</div> -->\n')
        __M_writer(u'\n<div class="cta cta-help">\n  <h3>')
        __M_writer(filters.decode.utf8(_("Not Enrolled?")))
        __M_writer(u'</h3>\n  <p><a href="')
        __M_writer(filters.decode.utf8(reverse('register_user')))
        __M_writer(u'">')
        __M_writer(filters.decode.utf8(_("Sign up for {platform_name} today!").format(platform_name=platform_name)))
        __M_writer(u'</a></p>\n\n')
        if settings.MKTG_URL_LINK_MAP.get('FAQ'):
            __M_writer(u'    <h3>')
            __M_writer(filters.decode.utf8(_("Need Help?")))
            __M_writer(u'</h3>\n    <p>')
            __M_writer(filters.decode.utf8(_("Looking for help signing in or with your {platform_name} account?").format(platform_name=platform_name)))
            __M_writer(u'\n    <a href="')
            __M_writer(filters.decode.utf8(marketing_link('FAQ')))
            __M_writer(u'">\n        ')
            __M_writer(filters.decode.utf8(_("View our help section for answers to commonly asked questions.")))
            __M_writer(u'\n    </a></p>\n')
        __M_writer(u'</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 1, "21": 0, "29": 4, "30": 7, "31": 7, "32": 10, "33": 11, "34": 12, "35": 12, "36": 13, "37": 13, "38": 14, "39": 14, "40": 17, "41": 19, "42": 19, "43": 20, "44": 20, "45": 20, "46": 20, "47": 23, "48": 24, "49": 24, "50": 24, "51": 25, "52": 25, "53": 26, "54": 26, "55": 27, "56": 27, "57": 30, "63": 57}, "uri": "login-sidebar.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/login-sidebar.html"}
__M_END_METADATA
"""
