# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849019.232361
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/user_dropdown.html'
_template_uri = u'user_dropdown.html'
_source_encoding = 'utf-8'
_exports = [u'navigation_dropdown_menu_links']



from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from openedx.core.djangoapps.user_api.accounts.image_helpers import get_profile_image_urls_for_user


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def navigation_dropdown_menu_links():
            return render_navigation_dropdown_menu_links(context._locals(__M_locals))
        self = context.get('self', UNDEFINED)
        uses_pattern_library = context.get('uses_pattern_library', UNDEFINED)
        real_user = context.get('real_user', UNDEFINED)
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')

        self.real_user = real_user if real_user != UNDEFINED else user
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in [] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        if uses_pattern_library:
            __M_writer(u'    <div class="wrapper-user-menu dropdown-menu-container logged-in js-header-user-menu">\n        <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('dashboard'))))
            __M_writer(u'" class="menu-title">\n            <span class="sr-only">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Dashboard for:"))))
            __M_writer(u'</span>\n            ')

            username = self.real_user.username
            profile_image_url = get_profile_image_urls_for_user(self.real_user)['medium']
            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['username','profile_image_url'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n            <img class="menu-image" src="')
            __M_writer(filters.html_escape(filters.decode.utf8(profile_image_url)))
            __M_writer(u'" alt="">\n            ')
            __M_writer(filters.html_escape(filters.decode.utf8(username)))
            __M_writer(u'\n        </a>\n        <div role="group" aria-label="User menu" class="user-menu">\n            <button type="button" class="menu-button button-more has-dropdown js-dropdown-button" aria-expanded="false" aria-controls="')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Usermenu"))))
            __M_writer(u'">\n                  <span class="icon fa fa-caret-down" aria-hidden="true"></span>\n                  <span class="sr-only">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Usermenu dropdown"))))
            __M_writer(u'</span>\n            </button>\n            <ul class="dropdown-menu list-divided is-hidden" id="')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Usermenu"))))
            __M_writer(u'" tabindex="-1">\n                ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_dropdown_menu_links'):
                context['self'].navigation_dropdown_menu_links(**pageargs)
            

            __M_writer(u'\n                <li class="dropdown-item item has-block-link"><a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('logout'))))
            __M_writer(u'" role="menuitem" class="action dropdown-menuitem">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Sign Out"))))
            __M_writer(u'</a></li>\n            </ul>\n        </div>\n    </div>\n')
        else:
            __M_writer(u'    <ol class="user">\n        <li class="primary">\n            <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('dashboard'))))
            __M_writer(u'" class="user-link">\n                <span class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Dashboard for:"))))
            __M_writer(u'</span>\n                ')

            username = self.real_user.username
            profile_image_url = get_profile_image_urls_for_user(self.real_user)['medium']
            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['username','profile_image_url'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n                <img class="user-image-frame" src="')
            __M_writer(filters.html_escape(filters.decode.utf8(profile_image_url)))
            __M_writer(u'" alt="">\n                <div class="label-username">')
            __M_writer(filters.html_escape(filters.decode.utf8(username)))
            __M_writer(u'</div>\n            </a>\n        </li>\n        <li class="primary">\n            <div role="group" aria-label="User menu" class="user-menu">\n                <button class="dropdown" aria-expanded="false"><span class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("More options dropdown"))))
            __M_writer(u'</span><span class="fa fa-sort-desc" aria-hidden="true"></span></button>\n                <ul class="dropdown-menu" aria-label="More Options" role="menu">\n                    ')
            __M_writer(filters.html_escape(filters.decode.utf8(navigation_dropdown_menu_links())))
            __M_writer(u'\n                    <li class="item"><a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('logout'))))
            __M_writer(u'" role="menuitem" class="dropdown-menuitem">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Sign Out"))))
            __M_writer(u'</a></li>\n                </ul>\n            </div>\n        </li>\n    </ol>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_dropdown_menu_links(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def navigation_dropdown_menu_links():
            return render_navigation_dropdown_menu_links(context)
        self = context.get('self', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n                    <li class="dropdown-item item has-block-link"><a href="')
        __M_writer(filters.html_escape(filters.decode.utf8(reverse('dashboard'))))
        __M_writer(u'" role="menuitem" class="action dropdown-menuitem">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Dashboard"))))
        __M_writer(u'</a></li>\n                    <li class="dropdown-item item has-block-link"><a href="')
        __M_writer(filters.html_escape(filters.decode.utf8(reverse('learner_profile', kwargs={'username': self.real_user.username}))))
        __M_writer(u'" role="menuitem" class="action dropdown-menuitem">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Profile"))))
        __M_writer(u'</a></li>\n                    <li class="dropdown-item item has-block-link"><a href="')
        __M_writer(filters.html_escape(filters.decode.utf8(reverse('account_settings'))))
        __M_writer(u'" role="menuitem" class="action dropdown-menuitem">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Account"))))
        __M_writer(u'</a></li>\n                ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"128": 35, "129": 35, "130": 36, "131": 36, "132": 36, "133": 36, "134": 37, "135": 37, "136": 37, "137": 37, "143": 137, "16": 10, "30": 3, "33": 2, "44": 2, "45": 3, "46": 6, "52": 8, "53": 15, "54": 17, "55": 18, "56": 19, "57": 19, "58": 20, "59": 20, "60": 21, "67": 24, "68": 25, "69": 25, "70": 26, "71": 26, "72": 29, "73": 29, "74": 31, "75": 31, "76": 33, "77": 33, "82": 38, "83": 39, "84": 39, "85": 39, "86": 39, "87": 43, "88": 44, "89": 46, "90": 46, "91": 47, "92": 47, "93": 48, "100": 51, "101": 52, "102": 52, "103": 53, "104": 53, "105": 58, "106": 58, "107": 60, "108": 60, "109": 61, "110": 61, "111": 61, "112": 61, "118": 34, "125": 34, "126": 35, "127": 35}, "uri": "user_dropdown.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/user_dropdown.html"}
__M_END_METADATA
"""
