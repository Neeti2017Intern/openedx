# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946423.876011
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/discussion/_js_body_dependencies.html'
_template_uri = u'/discussion/_js_body_dependencies.html'
_source_encoding = 'utf-8'
_exports = []



from openedx.core.djangolib.js_utils import js_escaped_string


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'/static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,disable_fast_preview=True,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs,disable_fast_preview=disable_fast_preview)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        runtime._include_file(context, u'/mathjax_include.html', _template_uri, disable_fast_preview=disable_fast_preview)
        __M_writer(u'\n\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.js(group=u'discussion'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n')

        discussion_classes = [
            ['Discussion', 'common/js/discussion/discussion'],
            ['Content', 'common/js/discussion/content'],
            ['DiscussionInlineView', 'common/js/discussion/views/discussion_inline_view'],
            ['DiscussionThreadView', 'common/js/discussion/views/discussion_thread_view'],
            ['DiscussionThreadListView', 'common/js/discussion/views/discussion_thread_list_view'],
            ['DiscussionThreadProfileView', 'common/js/discussion/views/discussion_thread_profile_view'],
            ['DiscussionUtil', 'common/js/discussion/utils'],
            ['DiscussionCourseSettings', 'common/js/discussion/models/discussion_course_settings'],
            ['DiscussionUser', 'common/js/discussion/models/discussion_user'],
            ['NewPostView', 'common/js/discussion/views/new_post_view'],
        ]
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['discussion_classes'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n<script type="text/javascript">\n')
        for discussion_class in discussion_classes:
            __M_writer(u"        RequireJS.define(\n            '")
            __M_writer(js_escaped_string(discussion_class[1] ))
            __M_writer(u"',\n            [],\n            function() {\n                return window['")
            __M_writer(js_escaped_string(discussion_class[0] ))
            __M_writer(u"'];\n            }\n        );\n")
        __M_writer(u'</script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"75": 36, "36": 2, "37": 3, "38": 5, "39": 9, "40": 11, "41": 11, "74": 33, "71": 31, "76": 36, "77": 40, "16": 7, "49": 13, "83": 77, "52": 13, "53": 16, "73": 33, "72": 32, "27": 3, "70": 29, "30": 5}, "uri": "/discussion/_js_body_dependencies.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/discussion/_js_body_dependencies.html"}
__M_END_METADATA
"""
