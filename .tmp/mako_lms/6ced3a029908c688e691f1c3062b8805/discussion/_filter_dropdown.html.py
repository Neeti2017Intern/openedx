# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946424.18727
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/discussion/_filter_dropdown.html'
_template_uri = u'discussion/_filter_dropdown.html'
_source_encoding = 'utf-8'
_exports = ['render_category', 'render_entry', 'render_dropdown']



from django.utils.translation import ugettext as _
from lms.djangoapps.django_comment_client.constants import TYPE_ENTRY
from openedx.core.djangolib.markup import HTML


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def render_dropdown(map):
            return render_render_dropdown(context._locals(__M_locals),map)
        category_map = context.get('category_map', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n<div class="forum-nav-browse-menu-wrapper" style="display: none" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Discussion topics list"))))
        __M_writer(u'">\n    <form class="forum-nav-browse-filter" autocomplete="off">\n        <label>\n            <span class="sr">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Filter Topics"))))
        __M_writer(u'</span>\n            <input type="text" id="forum-nav-browse-filter-input" role="combobox" aria-expanded="true" aria-owns="discussion_topics_listbox" aria-autocomplete="list" class="forum-nav-browse-filter-input" placeholder="')
        __M_writer(filters.html_escape(filters.decode.utf8(_("filter topics"))))
        __M_writer(u'">\n            <span class="icon fa fa-filter" aria-hidden="true"></span>\n        </label>\n    </form>\n    <ul class="forum-nav-browse-menu" role="listbox" id="discussion_topics_listbox">\n        <li class="forum-nav-browse-menu-item forum-nav-browse-menu-all" role="option" id="all_discussions">\n            <span class="forum-nav-browse-title">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("All Discussions"))))
        __M_writer(u'</span>\n        </li>\n        <li class="forum-nav-browse-menu-item forum-nav-browse-menu-following" role="option" id="posts_following">\n            <span class="icon fa fa-star" aria-hidden="true"></span>\n            <span class="forum-nav-browse-title">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Posts I'm Following"))))
        __M_writer(u'</span>\n        </li>\n        ')
        __M_writer(filters.html_escape(filters.decode.utf8(HTML(render_dropdown(category_map)))))
        __M_writer(u'\n    </ul>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_render_category(context,categories,category):
    __M_caller = context.caller_stack._push_frame()
    try:
        def render_dropdown(map):
            return render_render_dropdown(context,map)
        __M_writer = context.writer()
        __M_writer(u'\n    <li class="forum-nav-browse-menu-item"\n        id=\'')
        __M_writer(filters.url_escape(filters.html_escape(filters.decode.utf8(category ))))
        __M_writer(u'\'\n    >\n        <span class="forum-nav-browse-title">')
        __M_writer(filters.html_escape(filters.decode.utf8(category)))
        __M_writer(u'</span>\n        <ul class="forum-nav-browse-submenu">\n            ')
        __M_writer(filters.html_escape(filters.decode.utf8(HTML(render_dropdown(categories[category])))))
        __M_writer(u'\n        </ul>\n    </li>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_render_entry(context,entries,entry):
    __M_caller = context.caller_stack._push_frame()
    try:
        str = context.get('str', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n    <li\n        class="forum-nav-browse-menu-item"\n        data-discussion-id=\'')
        __M_writer(filters.html_escape(filters.decode.utf8(entries[entry]["id"])))
        __M_writer(u"'\n        id='")
        __M_writer(filters.html_escape(filters.decode.utf8(entries[entry]["id"])))
        __M_writer(u'\'\n        data-cohorted="')
        __M_writer(filters.html_escape(filters.decode.utf8(str(entries[entry]['is_cohorted']).lower())))
        __M_writer(u'"\n        role="option"\n    >\n        <span class="forum-nav-browse-title">')
        __M_writer(filters.html_escape(filters.decode.utf8(entry)))
        __M_writer(u'</span>\n    </li>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_render_dropdown(context,map):
    __M_caller = context.caller_stack._push_frame()
    try:
        def render_category(categories,category):
            return render_render_category(context,categories,category)
        def render_entry(entries,entry):
            return render_render_entry(context,entries,entry)
        __M_writer = context.writer()
        __M_writer(u'\n')
        for child, c_type in map["children"]:
            if child in map["entries"] and c_type == TYPE_ENTRY:
                __M_writer(u'            ')
                __M_writer(filters.html_escape(filters.decode.utf8(HTML(render_entry(map["entries"], child)))))
                __M_writer(u'\n')
            else:
                __M_writer(u'            ')
                __M_writer(filters.html_escape(filters.decode.utf8(HTML(render_category(map["subcategories"], child)))))
                __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "22": 1, "30": 1, "31": 6, "32": 16, "33": 28, "34": 39, "35": 41, "36": 41, "37": 44, "38": 44, "39": 45, "40": 45, "41": 51, "42": 51, "43": 55, "44": 55, "45": 57, "46": 57, "52": 30, "58": 30, "59": 32, "60": 32, "61": 34, "62": 34, "63": 36, "64": 36, "70": 18, "75": 18, "76": 21, "77": 21, "78": 22, "79": 22, "80": 23, "81": 23, "82": 26, "83": 26, "89": 8, "97": 8, "98": 9, "99": 10, "100": 11, "101": 11, "102": 11, "103": 12, "104": 13, "105": 13, "106": 13, "112": 106}, "uri": "discussion/_filter_dropdown.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/discussion/_filter_dropdown.html"}
__M_END_METADATA
"""
