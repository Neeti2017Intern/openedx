# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946423.518502
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/discussion/_js_head_dependencies.html'
_template_uri = u'discussion/_js_head_dependencies.html'
_source_encoding = 'utf-8'
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n\n<script type="text/javascript" src="')
        __M_writer(filters.decode.utf8(static.url('js/jquery.autocomplete.js')))
        __M_writer(u'"></script>\n<script type="text/javascript" src="')
        __M_writer(filters.decode.utf8(static.url('js/src/tooltip_manager.js')))
        __M_writer(u'"></script>\n\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.decode.utf8(static.js(group=u'discussion_vendor')))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n<link href="')
        __M_writer(filters.decode.utf8(static.url('css/vendor/jquery.autocomplete.css')))
        __M_writer(u'" rel="stylesheet" type="text/css">\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"32": 1, "33": 3, "34": 3, "35": 4, "36": 4, "55": 49, "44": 6, "47": 6, "48": 8, "49": 8, "23": 1, "26": 0}, "uri": "discussion/_js_head_dependencies.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/discussion/_js_head_dependencies.html"}
__M_END_METADATA
"""
