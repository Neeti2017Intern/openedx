# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849593.027026
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courses_list.html'
_template_uri = 'courses_list.html'
_source_encoding = 'utf-8'
_exports = []


from django.utils.translation import ugettext as _ 

def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        courses = context.get('courses', UNDEFINED)
        marketing_link = context.get('marketing_link', UNDEFINED)
        len = context.get('len', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n<section class="courses-container">\n  <section class="highlighted-courses">\n\n')
        if settings.FEATURES.get('COURSES_ARE_BROWSABLE'):
            __M_writer(u'      <section class="courses">\n        <ul class="courses-listing">\n')
            for course in courses[:settings.HOMEPAGE_COURSE_MAX]:
                __M_writer(u'          <li class="courses-listing-item">\n              ')
                runtime._include_file(context, u'course.html', _template_uri, course=course)
                __M_writer(u'\n          </li>\n')
            __M_writer(u'        </ul>\n      </section>\n')
            if settings.HOMEPAGE_COURSE_MAX and len(courses) > settings.HOMEPAGE_COURSE_MAX:
                __M_writer(u'      <div class="courses-more">\n        <a class="courses-more-cta" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('COURSES'))))
                __M_writer(u'"> ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("View all Courses"))))
                __M_writer(u' </a>\n      </div>\n')
        __M_writer(u'\n  </section>\n</section>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"48": 21, "37": 1, "38": 2, "39": 3, "40": 8, "41": 9, "42": 12, "43": 13, "44": 14, "45": 14, "46": 17, "47": 20, "16": 3, "49": 22, "50": 22, "51": 22, "52": 22, "53": 26, "25": 2, "59": 53, "28": 1}, "uri": "courses_list.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courses_list.html"}
__M_END_METADATA
"""
