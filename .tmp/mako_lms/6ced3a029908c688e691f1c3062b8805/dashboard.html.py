# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849018.910444
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/dashboard.html'
_template_uri = 'dashboard.html'
_source_encoding = 'utf-8'
_exports = [u'pagetitle', u'header_extras', u'bodyclass', 'online_help_token', u'js_extra']



from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.template import RequestContext
import third_party_auth
from third_party_auth import pipeline
from openedx.core.djangolib.js_utils import dump_js_escaped_json, js_escaped_string
from openedx.core.djangolib.markup import HTML, Text


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        enrollment_message = context.get('enrollment_message', UNDEFINED)
        block_courses = context.get('block_courses', UNDEFINED)
        course_enrollments = context.get('course_enrollments', UNDEFINED)
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        unicode = context.get('unicode', UNDEFINED)
        enumerate = context.get('enumerate', UNDEFINED)
        message = context.get('message', UNDEFINED)
        redirect_message = context.get('redirect_message', UNDEFINED)
        def header_extras():
            return render_header_extras(context._locals(__M_locals))
        show_email_settings_for = context.get('show_email_settings_for', UNDEFINED)
        getattr = context.get('getattr', UNDEFINED)
        errored_courses = context.get('errored_courses', UNDEFINED)
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        credit_statuses = context.get('credit_statuses', UNDEFINED)
        display_course_modes_on_dashboard = context.get('display_course_modes_on_dashboard', UNDEFINED)
        staff_access = context.get('staff_access', UNDEFINED)
        len = context.get('len', UNDEFINED)
        all_course_modes = context.get('all_course_modes', UNDEFINED)
        enrolled_courses_either_paid = context.get('enrolled_courses_either_paid', UNDEFINED)
        courses_requirements_not_met = context.get('courses_requirements_not_met', UNDEFINED)
        order_history_list = context.get('order_history_list', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        cert_statuses = context.get('cert_statuses', UNDEFINED)
        marketing_link = context.get('marketing_link', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        show_courseware_links_for = context.get('show_courseware_links_for', UNDEFINED)
        verification_status_by_course = context.get('verification_status_by_course', UNDEFINED)
        programs_by_run = context.get('programs_by_run', UNDEFINED)
        show_refund_option_for = context.get('show_refund_option_for', UNDEFINED)
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')

        cert_name_short = settings.CERT_NAME_SHORT
        cert_name_long = settings.CERT_NAME_LONG
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['cert_name_long','cert_name_short'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header_extras'):
            context['self'].header_extras(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n<div class="dashboard-notifications" tabindex="-1">\n')
        if message:
            __M_writer(u'        <div class="dashboard-banner">\n            ')
            __M_writer(filters.decode.utf8(message ))
            __M_writer(u'\n        </div>\n')
        __M_writer(u'\n')
        if enrollment_message:
            __M_writer(u'        <div class="dashboard-banner">\n            ')
            __M_writer(filters.decode.utf8(enrollment_message ))
            __M_writer(u'\n        </div>\n')
        __M_writer(u'</div>\n\n<main id="main" aria-label="Content" tabindex="-1">\n    <div class="container dashboard" id="dashboard-main">\n      <div class="my-courses" id="my-courses">\n        <header class="wrapper-header-courses">\n          <h2 class="header-courses">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("My Courses"))))
        __M_writer(u'</h2>\n        </header>\n\n\n')
        if len(course_enrollments) > 0:
            __M_writer(u'          <ul class="listing-courses">\n          ')
            share_settings = getattr(settings, 'SOCIAL_SHARING_SETTINGS', {}) 
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['share_settings'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n')
            for dashboard_index, enrollment in enumerate(course_enrollments):
                __M_writer(u'            ')
                show_courseware_link = (enrollment.course_id in show_courseware_links_for) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['show_courseware_link'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                cert_status = cert_statuses.get(enrollment.course_id) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['cert_status'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                can_unenroll = (not cert_status) or cert_status.get('can_unenroll') 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['can_unenroll'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                credit_status = credit_statuses.get(enrollment.course_id) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['credit_status'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                show_email_settings = (enrollment.course_id in show_email_settings_for) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['show_email_settings'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                course_mode_info = all_course_modes.get(enrollment.course_id) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_mode_info'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                show_refund_option = (enrollment.course_id in show_refund_option_for) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['show_refund_option'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                is_paid_course = (enrollment.course_id in enrolled_courses_either_paid) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['is_paid_course'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                is_course_blocked = (enrollment.course_id in block_courses) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['is_course_blocked'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                course_verification_status = verification_status_by_course.get(enrollment.course_id, {}) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_verification_status'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                course_requirements = courses_requirements_not_met.get(enrollment.course_id) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_requirements'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                related_programs = programs_by_run.get(unicode(enrollment.course_id)) 
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['related_programs'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n            ')
                runtime._include_file(context, u'dashboard/_dashboard_course_listing.html', _template_uri, course_overview=enrollment.course_overview, enrollment=enrollment, show_courseware_link=show_courseware_link, cert_status=cert_status, can_unenroll=can_unenroll, credit_status=credit_status, show_email_settings=show_email_settings, course_mode_info=course_mode_info, show_refund_option=show_refund_option, is_paid_course=is_paid_course, is_course_blocked=is_course_blocked, verification_status=course_verification_status, course_requirements=course_requirements, dashboard_index=dashboard_index, share_settings=share_settings, user=user, related_programs=related_programs, display_course_modes_on_dashboard=display_course_modes_on_dashboard)
                __M_writer(u'\n')
            __M_writer(u'\n          </ul>\n')
        else:
            __M_writer(u'          <div class="empty-dashboard-message">\n            <p>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("You are not enrolled in any courses yet."))))
            __M_writer(u'</p>\n\n')
            if settings.FEATURES.get('COURSES_ARE_BROWSABLE'):
                __M_writer(u'              <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(marketing_link('COURSES'))))
                __M_writer(u'">\n                ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Explore courses"))))
                __M_writer(u'\n              </a>\n\n')
            __M_writer(u'        </div>\n')
        __M_writer(u'\n')
        if staff_access and len(errored_courses) > 0:
            __M_writer(u'          <div id="course-errors">\n            <h2>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course-loading errors"))))
            __M_writer(u'</h2>\n\n')
            for course_dir, errors in errored_courses.items():
                __M_writer(u'             <h3>')
                __M_writer(filters.html_escape(filters.decode.utf8(course_dir)))
                __M_writer(u'</h3>\n                 <ul>\n')
                for (msg, err) in errors:
                    __M_writer(u'                   <li>')
                    __M_writer(filters.html_escape(filters.decode.utf8(msg)))
                    __M_writer(u'\n                     <ul><li><pre>')
                    __M_writer(filters.html_escape(filters.decode.utf8(err)))
                    __M_writer(u'</pre></li></ul>\n                   </li>\n')
                __M_writer(u'                 </ul>\n')
            __M_writer(u'          </div>\n')
        __M_writer(u'    </div>\n\n')
        if settings.FEATURES.get('ENABLE_DASHBOARD_SEARCH'):
            __M_writer(u'        <div id="dashboard-search-bar" class="search-bar dashboard-search-bar" role="search" aria-label="Dashboard">\n          <form>\n            <label for="dashboard-search-input">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Search Your Courses'))))
            __M_writer(u'</label>\n            <div class="search-field-wrapper">\n              <input id="dashboard-search-input" type="text" class="search-field"/>\n              <button type="submit" class="search-button" title="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Search'))))
            __M_writer(u'">\n                <span class="icon fa fa-search" aria-hidden="true"></span>\n              </button>\n              <button type="button" class="cancel-button" title="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Clear search'))))
            __M_writer(u'">\n                <span class="icon fa fa-remove" aria-hidden="true"></span>\n              </button>\n            </div>\n          </form>\n        </div>\n')
        __M_writer(u'\n')
        if settings.FEATURES.get('ENABLE_DASHBOARD_SEARCH'):
            __M_writer(u'        <div id="dashboard-search-results" class="search-results dashboard-search-results"></div>\n')
        __M_writer(u'\n      <div class="profile-sidebar" id="profile-sidebar" role="region" aria-label="Account Status Info">\n        <header class="profile">\n          <h2 class="account-status-title sr">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Account Status Info"))))
        __M_writer(u': </h2>\n        </header>\n        <div class="user-info">\n          <ul>\n\n')
        if len(order_history_list):
            __M_writer(u'            <li class="order-history">\n              <span class="title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Order History"))))
            __M_writer(u'</span>\n')
            for order_history_item in order_history_list:
                __M_writer(u'                <span><a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(order_history_item['receipt_url'])))
                __M_writer(u'" target="_blank" class="edit-name">')
                __M_writer(filters.html_escape(filters.decode.utf8(order_history_item['order_date'])))
                __M_writer(u'</a></span>\n')
            __M_writer(u'            </li>\n')
        __M_writer(u'\n            ')
        runtime._include_file(context, (static.get_template_path('dashboard/_dashboard_status_verification.html')), _template_uri)
        __M_writer(u'\n\n          </ul>\n            </div>\n        </div>\n    </div>\n</main>\n\n<div id="email-settings-modal" class="modal" aria-hidden="true">\n  <div class="inner-wrapper" role="dialog" aria-labelledby="email-settings-title">\n    <button class="close-modal">\n      <span class="icon fa fa-remove" aria-hidden="true"></span>\n      <span class="sr">\n')
        __M_writer(u'        ')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Close"))))
        __M_writer(u'\n      </span>\n    </button>\n\n    <header>\n      <h2 id="email-settings-title">\n        ')
        __M_writer(filters.html_escape(filters.decode.utf8(Text(_("Email Settings for {course_number}")).format(course_number=HTML('<span id="email_settings_course_number"></span>')))))
        __M_writer(u'\n        <span class="sr">,\n')
        __M_writer(u'          ')
        __M_writer(filters.html_escape(filters.decode.utf8(_("window open"))))
        __M_writer(u'\n        </span>\n      </h2>\n      <hr/>\n    </header>\n\n    <form id="email_settings_form" method="post">\n      <input name="course_id" id="email_settings_course_id" type="hidden" />\n      <label>')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Receive course emails"))))
        __M_writer(u' <input type="checkbox" id="receive_emails" name="receive_emails" /></label>\n      <div class="submit">\n        <input type="submit" id="submit" value="')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Save Settings"))))
        __M_writer(u'" />\n      </div>\n    </form>\n  </div>\n</div>\n\n<div id="unenroll-modal" class="modal unenroll-modal" aria-hidden="true">\n  <div class="inner-wrapper" role="dialog" aria-labelledby="unenrollment-modal-title">\n    <button class="close-modal">\n      <span class="icon fa fa-remove" aria-hidden="true"></span>\n      <span class="sr">\n')
        __M_writer(u'        ')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Close"))))
        __M_writer(u'\n      </span>\n    </button>\n\n    <header>\n      <h2 id="unenrollment-modal-title">\n        <span id=\'track-info\'></span>\n        <span id=\'refund-info\'></span>\n        <span class="sr">,\n')
        __M_writer(u'          ')
        __M_writer(filters.html_escape(filters.decode.utf8(_("window open"))))
        __M_writer(u'\n        </span>\n      </h2>\n      <hr/>\n    </header>\n    <div id="unenroll_error" class="modal-form-error"></div>\n    <form id="unenroll_form" method="post" data-remote="true" action="')
        __M_writer(filters.html_escape(filters.decode.utf8(reverse('change_enrollment'))))
        __M_writer(u'">\n      <input name="course_id" id="unenroll_course_id" type="hidden" />\n      <input name="enrollment_action" type="hidden" value="unenroll" />\n      <div class="submit">\n        <input name="submit" type="submit" value="')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Unenroll"))))
        __M_writer(u'" />\n      </div>\n    </form>\n  </div>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def pagetitle():
            return render_pagetitle(context)
        __M_writer = context.writer()
        __M_writer(filters.html_escape(filters.decode.utf8(_("Dashboard"))))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header_extras(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header_extras():
            return render_header_extras(context)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        for template_name in ["donation"]:
            __M_writer(u'<script type="text/template" id="')
            __M_writer(filters.html_escape(filters.decode.utf8(template_name)))
            __M_writer(u'-tpl">\n  ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.include(path=u'dashboard/' + (template_name) + u'.underscore'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n</script>\n')
        __M_writer(u'\n')
        for template_name in ["dashboard_search_item", "dashboard_search_results", "search_loading", "search_error"]:
            __M_writer(u'<script type="text/template" id="')
            __M_writer(filters.html_escape(filters.decode.utf8(template_name)))
            __M_writer(u'-tpl">\n    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.include(path=u'search/' + (template_name) + u'.underscore'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n</script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        __M_writer(u'view-dashboard is-authenticated')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_online_help_token(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        return "learnerdashboard" 
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        redirect_message = context.get('redirect_message', UNDEFINED)
        def js_extra():
            return render_js_extra(context)
        static = _mako_get_namespace(context, 'static')
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n  <script src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('js/commerce/credit.js'))))
        __M_writer(u'"></script>\n  ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.js(group=u'dashboard'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n  <script type="text/javascript">\n    $(document).ready(function() {\n      edx.dashboard.legacy.init({\n        dashboard: "')
        __M_writer(js_escaped_string(reverse('dashboard') ))
        __M_writer(u'",\n        signInUser: "')
        __M_writer(js_escaped_string(reverse('signin_user') ))
        __M_writer(u'",\n        changeEmailSettings: "')
        __M_writer(js_escaped_string(reverse('change_email_settings') ))
        __M_writer(u'"\n      });\n    });\n  </script>\n')
        if settings.FEATURES.get('ENABLE_DASHBOARD_SEARCH'):
            __M_writer(u'    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    __M_writer(u'\n        DashboardSearchFactory();\n    ')
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.require_module(class_name=u'DashboardSearchFactory',module_name=u'js/search/dashboard/dashboard_search_factory'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
        if redirect_message:
            __M_writer(u'    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    __M_writer(u"\n        var banner = new MessageBannerView({urgency: 'low', type: 'warning'});\n        $('#content').prepend(banner.$el);\n        banner.showMessage(")
                    __M_writer(dump_js_escaped_json(redirect_message ))
                    __M_writer(u')\n    ')
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.require_module(class_name=u'MessageBannerView',module_name=u'js/views/message_banner'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 5, "33": 4, "39": 1, "79": 1, "80": 2, "81": 3, "82": 4, "83": 13, "84": 15, "91": 18, "96": 21, "101": 22, "106": 36, "111": 62, "112": 65, "113": 66, "114": 67, "115": 67, "116": 70, "117": 71, "118": 72, "119": 73, "120": 73, "121": 76, "122": 82, "123": 82, "124": 86, "125": 87, "126": 88, "130": 88, "131": 89, "132": 90, "133": 90, "137": 90, "138": 91, "142": 91, "143": 92, "147": 92, "148": 93, "152": 93, "153": 94, "157": 94, "158": 95, "162": 95, "163": 96, "167": 96, "168": 97, "172": 97, "173": 98, "177": 98, "178": 99, "182": 99, "183": 100, "187": 100, "188": 101, "192": 101, "193": 102, "194": 102, "195": 104, "196": 106, "197": 107, "198": 108, "199": 108, "200": 110, "201": 111, "202": 111, "203": 111, "204": 112, "205": 112, "206": 116, "207": 118, "208": 119, "209": 120, "210": 121, "211": 121, "212": 123, "213": 124, "214": 124, "215": 124, "216": 126, "217": 127, "218": 127, "219": 127, "220": 128, "221": 128, "222": 131, "223": 133, "224": 135, "225": 137, "226": 138, "227": 140, "228": 140, "229": 143, "230": 143, "231": 146, "232": 146, "233": 153, "234": 154, "235": 155, "236": 157, "237": 160, "238": 160, "239": 165, "240": 166, "241": 167, "242": 167, "243": 168, "244": 169, "245": 169, "246": 169, "247": 169, "248": 169, "249": 171, "250": 173, "251": 174, "252": 174, "253": 188, "254": 188, "255": 188, "256": 194, "257": 194, "258": 197, "259": 197, "260": 197, "261": 205, "262": 205, "263": 207, "264": 207, "265": 219, "266": 219, "267": 219, "268": 229, "269": 229, "270": 229, "271": 235, "272": 235, "273": 239, "274": 239, "280": 21, "286": 21, "292": 24, "299": 24, "300": 25, "301": 26, "302": 26, "303": 26, "311": 27, "314": 27, "315": 30, "316": 31, "317": 32, "318": 32, "319": 32, "327": 33, "330": 33, "336": 22, "342": 22, "348": 3, "352": 3, "359": 38, "368": 38, "369": 39, "370": 39, "378": 40, "381": 40, "382": 44, "383": 44, "384": 45, "385": 45, "386": 46, "387": 46, "388": 50, "389": 51, "393": 51, "398": 51, "401": 53, "402": 55, "403": 56, "407": 56, "408": 59, "409": 59, "414": 56, "417": 60, "423": 417}, "uri": "dashboard.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/dashboard.html"}
__M_END_METADATA
"""
