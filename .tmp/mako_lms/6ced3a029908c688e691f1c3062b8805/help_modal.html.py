# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497848942.55985
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/help_modal.html'
_template_uri = u'help_modal.html'
_source_encoding = 'utf-8'
_exports = []



from datetime import datetime
import pytz
from django.conf import settings
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from openedx.core.djangolib.js_utils import js_escaped_string
from openedx.core.djangolib.markup import HTML, Text
from xmodule.tabs import CourseTabList


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        course = context.get('course', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        user = context.get('user', UNDEFINED)
        unicode = context.get('unicode', UNDEFINED)
        marketing_link = context.get('marketing_link', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        if settings.FEATURES.get('ENABLE_FEEDBACK_SUBMISSION', False):
            __M_writer(u'\n<div class="help-tab">\n  <a href="#help-modal" rel="leanModal" role="button">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Support"))))
            __M_writer(u'</a>\n</div>\n\n<section id="help-modal" class="modal" aria-hidden="true" role="dialog" tabindex="-1" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_("{platform_name} Support").format(platform_name=static.get_platform_name()))))
            __M_writer(u'">\n  <div class="inner-wrapper">\n')
            __M_writer(u'    <button class="close-modal" tabindex="0">\n      <span class="icon fa fa-remove" aria-hidden="true"></span>\n      <span class="sr">\n')
            __M_writer(u'        ')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Close'))))
            __M_writer(u'\n      </span>\n    </button>\n    <div id="help_wrapper">\n\n        <header>\n          <h2>\n        ')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_('{platform_name} Support')).format(
            platform_name=HTML(u'<span class="edx">{}</span>').format(static.get_platform_name())
        ))))
            __M_writer(u'\n          </h2>\n          <hr>\n        </header>\n\n    ')

            discussion_tab = CourseTabList.get_discussion(course) if course else None
            discussion_link = discussion_tab.link_func(course, reverse) if (discussion_tab and discussion_tab.is_enabled(course, user=user)) else None
                
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['discussion_tab','discussion_link'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n\n')
            if discussion_link:
                __M_writer(u'        <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_('For {strong_start}questions on course lectures, homework, tools, or materials for this course{strong_end}, post in the {link_start}course discussion forum{link_end}.')).format(
          strong_start=HTML('<strong>'),
          strong_end=HTML('</strong>'),
          link_start=HTML('<a href="{url}" target="_blank">').format(
            url=discussion_link
          ),
          link_end=HTML('</a>'),
          ))))
                __M_writer(u'\n        </p>\n')
            __M_writer(u'\n        <p>')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_('Have {strong_start}general questions about {platform_name}{strong_end}? You can find lots of helpful information in the {platform_name} {link_start}FAQ{link_end}.')).format(
            strong_start=HTML('<strong>'),
            strong_end=HTML('</strong>'),
            link_start=HTML('<a href="{url}" id="feedback-faq-link" target="_blank">').format(
              url=marketing_link('FAQ')
            ),
            link_end=HTML('</a>'),
            platform_name=static.get_platform_name()))))
            __M_writer(u'\n          </p>\n\n        <p>')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_('Have a {strong_start}question about something specific{strong_end}? You can contact the {platform_name} general support team directly:')).format(
            strong_start=HTML('<strong>'),
            strong_end=HTML('</strong>'),
            platform_name=static.get_platform_name()
          ))))
            __M_writer(u'</p>\n        <hr>\n\n        <div class="help-buttons">\n            <button type="button" class="" id="feedback_link_problem">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Report a problem'))))
            __M_writer(u'</button><br />\n            <button type="button" class="" id="feedback_link_suggestion">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Make a suggestion'))))
            __M_writer(u'</button><br />\n            <button type="button" class="" id="feedback_link_question">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Ask a question'))))
            __M_writer(u'</button>\n        </div>\n\n        <p class="note">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Please note: The {platform_name} support team is English speaking. While we will do our best to address your inquiry in any language, our responses will be in English.').format(
            platform_name=static.get_platform_name()
        ))))
            __M_writer(u'</p>\n\n    </div>\n\n    <div id="feedback_form_wrapper">\n\n        <header></header>\n\n        <form id="feedback_form" class="feedback_form" method="post" data-remote="true" action="/submit_feedback">\n          <div class="feedback-form-error" aria-live="polite">\n              <div id="feedback_error" class="modal-form-error" tabindex="-1"></div>\n          </div>\n')
            if not user.is_authenticated():
                __M_writer(u'          <label data-field="name" for="feedback_form_name">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Name'))))
                __M_writer(u'*</label>\n          <input name="name" type="text" id="feedback_form_name" required>\n          <label data-field="email" for="feedback_form_email">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('E-mail'))))
                __M_writer(u'*</label>\n          <input name="email" type="text" id="feedback_form_email" required>\n')
            __M_writer(u'          <label data-field="subject" for="feedback_form_subject">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Briefly describe your issue'))))
            __M_writer(u'*</label>\n          <input name="subject" type="text" id="feedback_form_subject" required>\n          <label data-field="details" for="feedback_form_details">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Tell us the details'))))
            __M_writer(u'*</label>\n          <span class="tip" id="feedback_form_details_tip">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Describe what you were doing when you encountered the issue. Include any details that will help us to troubleshoot, including error messages that you saw.'))))
            __M_writer(u'</span>\n          <textarea name="details" id="feedback_form_details" required aria-describedby="feedback_form_details_tip"></textarea>\n          <input name="issue_type" type="hidden">\n')
            if course:
                __M_writer(u'          <input name="course_id" type="hidden" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(unicode(course.id))))
                __M_writer(u'">\n')
            __M_writer(u'          <div class="submit">\n            <input name="submit" type="submit" value="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Submit'))))
            __M_writer(u'" id="feedback_submit">\n          </div>\n        </form>\n    </div>\n\n    <div id="feedback_success_wrapper">\n\n        <header>\n          <h2>')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Thank You!'))))
            __M_writer(u'</h2>\n          <hr>\n        </header>\n\n        <p>\n        ')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_(
            'Thank you for your inquiry or feedback.  We typically respond to a request '
            'within one business day, Monday to Friday.  In the meantime, please '
            'review our {link_start}detailed FAQs{link_end} where most questions have '
            'already been answered.'
        )).format(
            link_start=HTML('<a href="{}" target="_blank" id="success-feedback-faq-link">').format(marketing_link('FAQ')),
            link_end=HTML('</a>')
        ))))
            __M_writer(u'\n        </p>\n    </div>\n  </div>\n</section>\n\n<script type="text/javascript">\n$(document).ready(function() {\n    var $helpModal = $("#help-modal"),\n        $closeButton = $("#help-modal .close-modal"),\n        $leanOverlay = $("#lean_overlay"),\n        $feedbackForm = $("#feedback_form"),\n        onModalClose = function() {\n            $closeButton.off("click");\n            $leanOverlay.off("click");\n            $helpModal.attr("aria-hidden", "true");\n            $(\'area,input,select,textarea,button\').removeAttr(\'tabindex\');\n            $(".help-tab a").focus();\n            $leanOverlay.removeAttr(\'tabindex\');\n        };\n\n    DialogTabControls.setKeydownListener($helpModal, $closeButton);\n\n    $(".help-tab").click(function() {\n        $helpModal.css("position", "absolute");\n        DialogTabControls.initializeTabKeyValues("#help_wrapper", $closeButton);\n        $(".field-error").removeClass("field-error");\n        $feedbackForm[0].reset();\n        $("#feedback_form input[type=\'submit\']").removeAttr("disabled");\n        $("#feedback_form_wrapper").css("display", "none");\n        $("#feedback_error").css("display", "none");\n        $("#feedback_form_details_tip").css("display", "none");\n        $("#feedback_success_wrapper").css("display", "none");\n        $("#help_wrapper").css("display", "block");\n        $helpModal.attr("aria-hidden", "false");\n        $closeButton.click(onModalClose);\n        $leanOverlay.click(onModalClose);\n        $("button.close-modal").attr(\'tabindex\', 0);\n        $closeButton.focus();\n    });\n\n    showFeedback = function(event, issue_type, title, subject_label, details_label) {\n        $("#help_wrapper").css("display", "none");\n        DialogTabControls.initializeTabKeyValues("#feedback_form_wrapper", $closeButton);\n        $("#feedback_form input[name=\'issue_type\']").val(issue_type);\n        $("#feedback_form_wrapper").css("display", "block");\n        $("#feedback_form_wrapper header").html("<h2>" + title + "</h2><hr>");\n        $("#feedback_form_wrapper label[data-field=\'subject\']").html(subject_label);\n        $("#feedback_form_wrapper label[data-field=\'details\']").html(details_label);\n        $closeButton.focus();\n        event.preventDefault();\n    };\n\n    $("#feedback_link_problem").click(function(event) {\n        $("#feedback_form_details_tip").css({"display": "block", "padding-bottom": "5px"});\n        showFeedback(\n            event,\n            "')
            __M_writer(js_escaped_string(_('problem') ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Report a Problem') ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Brief description of the problem') + '*' ))
            __M_writer(u'" ,\n            "')
            __M_writer(js_escaped_string(Text(_('Details of the problem you are encountering{asterisk}')).format(
                asterisk='*',
            ) ))
            __M_writer(u'"\n        );\n    });\n    $("#feedback_link_suggestion").click(function(event) {\n        showFeedback(\n            event,\n            "')
            __M_writer(js_escaped_string(_('suggestion') ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Make a Suggestion') ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Brief description of your suggestion') + '*' ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Details') + '*' ))
            __M_writer(u'"\n        );\n    });\n    $("#feedback_link_question").click(function(event) {\n        showFeedback(\n            event,\n            "')
            __M_writer(js_escaped_string(_('question') ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Ask a Question') ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Brief summary of your question') + '*' ))
            __M_writer(u'",\n            "')
            __M_writer(js_escaped_string(_('Details') + '*' ))
            __M_writer(u'"\n        );\n    });\n    $feedbackForm.submit(function() {\n        $("input[type=\'submit\']", this).attr("disabled", "disabled");\n        $closeButton.focus();\n    });\n    $feedbackForm.on("ajax:complete", function() {\n        $("input[type=\'submit\']", this).removeAttr("disabled");\n    });\n    $feedbackForm.on("ajax:success", function(event, data, status, xhr) {\n        $("#feedback_form_wrapper").css("display", "none");\n        $("#feedback_success_wrapper").css("display", "block");\n        DialogTabControls.initializeTabKeyValues("#feedback_success_wrapper", $closeButton);\n        $closeButton.focus();\n    });\n    $feedbackForm.on("ajax:error", function(event, xhr, status, error) {\n        $(".field-error").removeClass("field-error").removeAttr("aria-invalid");\n        var responseData;\n        try {\n            responseData = jQuery.parseJSON(xhr.responseText);\n        } catch(err) {\n        }\n        if (responseData) {\n            $("[data-field=\'"+responseData.field+"\']").addClass("field-error").attr("aria-invalid", "true");\n            $("#feedback_error").html(responseData.error).stop().css("display", "block");\n        } else {\n            // If no data (or malformed data) is returned, a server error occurred\n            htmlStr = "')
            __M_writer(js_escaped_string(_('An error has occurred.') ))
            __M_writer(u'";\n')
            if settings.FEEDBACK_SUBMISSION_EMAIL:
                __M_writer(u'            htmlStr += " " + "')
                __M_writer(js_escaped_string(Text(_('Please {link_start}send us e-mail{link_end}.')).format(
              link_start=HTML('<button type="button" id="feedback_email">'),
              link_end=HTML('</button>'),
            ) ))
                __M_writer(u'";\n')
            else:
                __M_writer(u'            // If no email is configured, we can\'t do much other than\n            // ask the user to try again later\n            htmlStr += " " + "')
                __M_writer(js_escaped_string(_('Please try again later.') ))
                __M_writer(u'";\n')
            __M_writer(u'            $("#feedback_error").html(htmlStr).stop().css("display", "block");\n')
            if settings.FEEDBACK_SUBMISSION_EMAIL:
                __M_writer(u'            $("#feedback_email").click(function(e) {\n                mailto = "mailto:" + "')
                __M_writer(js_escaped_string(settings.FEEDBACK_SUBMISSION_EMAIL ))
                __M_writer(u'" +\n                    "?subject=" + $("#feedback_form input[name=\'subject\']").val() +\n                    "&body=" + $("#feedback_form textarea[name=\'details\']").val();\n                window.open(mailto);\n                e.preventDefault();\n            });\n')
            __M_writer(u'        }\n        // Make change explicit to assistive technology\n        $("#feedback_error").focus();\n    });\n});\n</script>\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 4, "34": 2, "37": 1, "47": 1, "48": 2, "49": 13, "50": 15, "51": 16, "52": 18, "53": 18, "54": 21, "55": 21, "56": 24, "57": 28, "58": 28, "59": 28, "60": 35, "63": 37, "64": 42, "71": 45, "72": 47, "73": 48, "74": 48, "82": 55, "83": 58, "84": 59, "92": 66, "93": 69, "98": 73, "99": 77, "100": 77, "101": 78, "102": 78, "103": 79, "104": 79, "105": 82, "108": 84, "109": 96, "110": 97, "111": 97, "112": 97, "113": 99, "114": 99, "115": 102, "116": 102, "117": 102, "118": 104, "119": 104, "120": 105, "121": 105, "122": 108, "123": 109, "124": 109, "125": 109, "126": 111, "127": 112, "128": 112, "129": 120, "130": 120, "131": 125, "140": 133, "141": 190, "142": 190, "143": 191, "144": 191, "145": 192, "146": 192, "147": 193, "150": 195, "151": 201, "152": 201, "153": 202, "154": 202, "155": 203, "156": 203, "157": 204, "158": 204, "159": 210, "160": 210, "161": 211, "162": 211, "163": 212, "164": 212, "165": 213, "166": 213, "167": 241, "168": 241, "169": 242, "170": 243, "171": 243, "175": 246, "176": 247, "177": 248, "178": 250, "179": 250, "180": 252, "181": 253, "182": 254, "183": 255, "184": 255, "185": 262, "191": 185}, "uri": "help_modal.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/help_modal.html"}
__M_END_METADATA
"""
