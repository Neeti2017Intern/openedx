# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849077.512851
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/staff_problem_info.html'
_template_uri = 'staff_problem_info.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from django.template.defaultfilters import escapejs


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'/static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        category = context.get('category', UNDEFINED)
        xml_attributes = context.get('xml_attributes', UNDEFINED)
        disable_staff_debug_info = context.get('disable_staff_debug_info', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        xqa_key = context.get('xqa_key', UNDEFINED)
        has_instructor_access = context.get('has_instructor_access', UNDEFINED)
        tags = context.get('tags', UNDEFINED)
        block_content = context.get('block_content', UNDEFINED)
        histogram = context.get('histogram', UNDEFINED)
        is_released = context.get('is_released', UNDEFINED)
        render_histogram = context.get('render_histogram', UNDEFINED)
        user = context.get('user', UNDEFINED)
        fields = context.get('fields', UNDEFINED)
        can_reset_attempts = context.get('can_reset_attempts', UNDEFINED)
        element_id = context.get('element_id', UNDEFINED)
        can_rescore_problem = context.get('can_rescore_problem', UNDEFINED)
        edit_link = context.get('edit_link', UNDEFINED)
        location = context.get('location', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(filters.decode.utf8(block_content))
        __M_writer(u'\n')
        if 'detached' not in tags:
            if edit_link:
                __M_writer(u'  <div>\n      <a href="')
                __M_writer(filters.decode.utf8(edit_link))
                __M_writer(u'">Edit</a>\n')
                if xqa_key:
                    __M_writer(u'          / <a href="#')
                    __M_writer(filters.decode.utf8(element_id))
                    __M_writer(u'_xqa-modal" onclick="javascript:getlog(\'')
                    __M_writer(filters.decode.utf8(element_id))
                    __M_writer(u"', {\n          'location': '")
                    __M_writer(filters.html_escape(filters.decode.utf8(location )))
                    __M_writer(u"',\n          'xqa_key': '")
                    __M_writer(filters.html_escape(filters.decode.utf8(xqa_key )))
                    __M_writer(u"',\n          'category': '")
                    __M_writer(filters.html_escape(filters.decode.utf8(category )))
                    __M_writer(u"',\n          'user': '")
                    __M_writer(filters.html_escape(filters.decode.utf8(user )))
                    __M_writer(u'\'\n       })" id="')
                    __M_writer(filters.decode.utf8(element_id))
                    __M_writer(u'_xqa_log">QA</a>\n')
                __M_writer(u'  </div>\n')
            if not disable_staff_debug_info:
                __M_writer(u'<div class="wrap-instructor-info">\n  <a class="instructor-info-action" href="#')
                __M_writer(filters.decode.utf8(element_id))
                __M_writer(u'_debug" id="')
                __M_writer(filters.decode.utf8(element_id))
                __M_writer(u'_trig">')
                __M_writer(filters.decode.utf8(_("Staff Debug Info")))
                __M_writer(u'</a>\n\n')
                if settings.FEATURES.get('ENABLE_STUDENT_HISTORY_VIEW') and \
    location.category == 'problem':
                    __M_writer(u'    <a class="instructor-info-action" href="#')
                    __M_writer(filters.decode.utf8(element_id))
                    __M_writer(u'_history" id="')
                    __M_writer(filters.decode.utf8(element_id))
                    __M_writer(u'_history_trig">')
                    __M_writer(filters.decode.utf8(_("Submission history")))
                    __M_writer(u'</a>\n')
                __M_writer(u'</div>\n')
            __M_writer(u'\n<div aria-hidden="true" role="dialog" tabindex="-1" id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_xqa-modal" class="modal xqa-modal">\n  <div class="inner-wrapper">\n    <header>\n      <h2>')
            __M_writer(filters.decode.utf8(_("{platform_name} Content Quality Assessment").format(platform_name=settings.PLATFORM_NAME)))
            __M_writer(u'</h2>\n    </header>\n\n    <form id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_xqa_form" class="xqa_form">\n      <label for="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_xqa_entry">')
            __M_writer(filters.decode.utf8(_("Comment")))
            __M_writer(u'</label>\n      <input tabindex="0" id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_xqa_entry" type="text" placeholder="')
            __M_writer(filters.decode.utf8(_('comment')))
            __M_writer(u'">\n      <label for="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_xqa_tag">')
            __M_writer(filters.decode.utf8(_("Tag")))
            __M_writer(u'</label>\n      <span style="color:black;vertical-align: -10pt">')
            __M_writer(filters.decode.utf8(_('Optional tag (eg "done" or "broken"):') + '&nbsp; '))
            __M_writer(u'      </span>\n      <input id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_xqa_tag" type="text" placeholder="')
            __M_writer(filters.decode.utf8(_('tag')))
            __M_writer(u'" style="width:80px;display:inline">\n      <div class="submit">\n        <button name="submit" type="submit">')
            __M_writer(filters.decode.utf8(_('Add comment')))
            __M_writer(u'</button>\n      </div>\n      <hr>\n      <div id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_xqa_log_data"></div>\n    </form>\n\n  </div>\n</div>\n\n<div aria-hidden="true" role="dialog" tabindex="-1" class="modal staff-modal" id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_debug" >\n  <div class="inner-wrapper">\n    <header>\n      <h2>')
            __M_writer(filters.decode.utf8(_('Staff Debug')))
            __M_writer(u'</h2>\n    </header>\n\n    <hr />\n    <div class="staff_actions">\n      <h3>')
            __M_writer(filters.decode.utf8(_('Actions')))
            __M_writer(u'</h3>\n      <div>\n        <label for="sd_fu_')
            __M_writer(filters.html_escape(filters.decode.utf8(location.name )))
            __M_writer(u'">')
            __M_writer(filters.decode.utf8(_('Username')))
            __M_writer(u':</label>\n        <input type="text" tabindex="0" id="sd_fu_')
            __M_writer(filters.html_escape(filters.decode.utf8(location.name )))
            __M_writer(u'" placeholder="')
            __M_writer(filters.decode.utf8(user.username))
            __M_writer(u'"/>\n      </div>\n      <div data-location="')
            __M_writer(filters.html_escape(filters.decode.utf8(location )))
            __M_writer(u'" data-location-name="')
            __M_writer(filters.html_escape(filters.decode.utf8(location.name )))
            __M_writer(u'">\n        [\n')
            if can_reset_attempts:
                __M_writer(u'        <button type="button" class="btn-link staff-debug-reset">')
                __M_writer(filters.decode.utf8(_('Reset Learner\'s Attempts to Zero')))
                __M_writer(u'</button>\n        |\n')
            if has_instructor_access:
                __M_writer(u'        <button type="button" class="btn-link staff-debug-sdelete">')
                __M_writer(filters.decode.utf8(_('Delete Learner\'s State')))
                __M_writer(u'</button>\n')
                if can_rescore_problem:
                    __M_writer(u'        |\n        <button type="button" class="btn-link staff-debug-rescore">')
                    __M_writer(filters.decode.utf8(_('Rescore Learner\'s Submission')))
                    __M_writer(u'</button>\n        |\n        <button type="button" class="btn-link staff-debug-rescore-if-higher">')
                    __M_writer(filters.decode.utf8(_('Rescore Only If Score Improves')))
                    __M_writer(u'</button>\n')
            __M_writer(u'        ]\n      </div>\n      <div id="result_')
            __M_writer(filters.html_escape(filters.decode.utf8(location.name )))
            __M_writer(u'"></div>\n    </div>\n\n    <div class="staff_info" style="display:block">\n      is_released = ')
            __M_writer(filters.decode.utf8(is_released))
            __M_writer(u'\n      location = ')
            __M_writer(filters.html_escape(filters.decode.utf8(location.to_deprecated_string() )))
            __M_writer(u'\n\n      <table summary="')
            __M_writer(filters.decode.utf8(_('Module Fields')))
            __M_writer(u'">\n        <tr><th>')
            __M_writer(filters.decode.utf8(_('Module Fields')))
            __M_writer(u'</th></tr>\n')
            for name, field in fields:
                __M_writer(u'        <tr><td style="width:25%">')
                __M_writer(filters.decode.utf8(name))
                __M_writer(u'</td><td><pre style="display:inline-block; margin: 0;">')
                __M_writer(filters.html_escape(filters.decode.utf8(field )))
                __M_writer(u'</pre></td></tr>\n')
            __M_writer(u'      </table>\n      <table>\n        <tr><th>')
            __M_writer(filters.decode.utf8(_('XML attributes')))
            __M_writer(u'</th></tr>\n')
            for name, field in xml_attributes.items():
                __M_writer(u'        <tr><td style="width:25%">')
                __M_writer(filters.decode.utf8(name))
                __M_writer(u'</td><td><pre style="display:inline-block; margin: 0;">')
                __M_writer(filters.html_escape(filters.decode.utf8(field )))
                __M_writer(u'</pre></td></tr>\n')
            __M_writer(u'      </table>\n      category = ')
            __M_writer(filters.html_escape(filters.decode.utf8(category )))
            __M_writer(u'\n    </div>\n')
            if render_histogram:
                __M_writer(u'    <div id="histogram_')
                __M_writer(filters.decode.utf8(element_id))
                __M_writer(u'" class="histogram" data-histogram="')
                __M_writer(filters.decode.utf8(histogram))
                __M_writer(u'"></div>\n')
            __M_writer(u'  </div>\n</div>\n\n<div aria-hidden="true" role="dialog" tabindex="-1" class="modal history-modal" id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_history">\n  <div class="inner-wrapper">\n    <header>\n      <h2>')
            __M_writer(filters.decode.utf8(_("Submission History Viewer")))
            __M_writer(u'</h2>\n    </header>\n    <form id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_history_form">\n      <label for="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_history_student_username">')
            __M_writer(filters.decode.utf8(_("User:")))
            __M_writer(u'</label>\n      <input tabindex="0" id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_history_student_username" type="text" placeholder=""/>\n      <input type="hidden" id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_history_location" value="')
            __M_writer(filters.html_escape(filters.decode.utf8(location )))
            __M_writer(u'"/>\n      <div class="submit">\n        <button name="submit" type="submit">')
            __M_writer(filters.decode.utf8(_("View History")))
            __M_writer(u'</button>\n      </div>\n    </form>\n\n    <div id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_history_text" class="staff_info" style="display:block">\n    </div>\n  </div>\n</div>\n\n<div id="')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u'_setup"></div>\n\n<script type="text/javascript">\n// assumes courseware.html\'s loaded this method.\n$(function () {\n    setup_debug(\'')
            __M_writer(filters.decode.utf8(element_id))
            __M_writer(u"',\n")
            if edit_link:
                __M_writer(u"        '")
                __M_writer(filters.decode.utf8(edit_link))
                __M_writer(u"',\n")
            else:
                __M_writer(u'        null,\n')
            __M_writer(u"        {\n            'location': '")
            __M_writer(escapejs(filters.decode.utf8(location )))
            __M_writer(u"',\n            'xqa_key': '")
            __M_writer(filters.decode.utf8(xqa_key))
            __M_writer(u"',\n            'category': '")
            __M_writer(filters.decode.utf8(category))
            __M_writer(u"',\n            'user': '")
            __M_writer(filters.decode.utf8(user))
            __M_writer(u"'\n        }\n    );\n});\n</script>\n")
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "28": 1, "31": 0, "54": 1, "55": 5, "56": 8, "57": 8, "58": 9, "59": 10, "60": 11, "61": 12, "62": 12, "63": 13, "64": 14, "65": 14, "66": 14, "67": 14, "68": 14, "69": 15, "70": 15, "71": 16, "72": 16, "73": 17, "74": 17, "75": 18, "76": 18, "77": 19, "78": 19, "79": 21, "80": 23, "81": 24, "82": 25, "83": 25, "84": 25, "85": 25, "86": 25, "87": 25, "88": 27, "90": 29, "91": 29, "92": 29, "93": 29, "94": 29, "95": 29, "96": 29, "97": 31, "98": 33, "99": 34, "100": 34, "101": 37, "102": 37, "103": 40, "104": 40, "105": 41, "106": 41, "107": 41, "108": 41, "109": 42, "110": 42, "111": 42, "112": 42, "113": 43, "114": 43, "115": 43, "116": 43, "117": 44, "118": 44, "119": 45, "120": 45, "121": 45, "122": 45, "123": 47, "124": 47, "125": 50, "126": 50, "127": 56, "128": 56, "129": 59, "130": 59, "131": 64, "132": 64, "133": 66, "134": 66, "135": 66, "136": 66, "137": 67, "138": 67, "139": 67, "140": 67, "141": 69, "142": 69, "143": 69, "144": 69, "145": 71, "146": 72, "147": 72, "148": 72, "149": 75, "150": 76, "151": 76, "152": 76, "153": 77, "154": 78, "155": 79, "156": 79, "157": 81, "158": 81, "159": 84, "160": 86, "161": 86, "162": 90, "163": 90, "164": 91, "165": 91, "166": 93, "167": 93, "168": 94, "169": 94, "170": 95, "171": 96, "172": 96, "173": 96, "174": 96, "175": 96, "176": 98, "177": 100, "178": 100, "179": 101, "180": 102, "181": 102, "182": 102, "183": 102, "184": 102, "185": 104, "186": 105, "187": 105, "188": 107, "189": 108, "190": 108, "191": 108, "192": 108, "193": 108, "194": 110, "195": 113, "196": 113, "197": 116, "198": 116, "199": 118, "200": 118, "201": 119, "202": 119, "203": 119, "204": 119, "205": 120, "206": 120, "207": 121, "208": 121, "209": 121, "210": 121, "211": 123, "212": 123, "213": 127, "214": 127, "215": 132, "216": 132, "217": 137, "218": 137, "219": 138, "220": 139, "221": 139, "222": 139, "223": 140, "224": 141, "225": 143, "226": 144, "227": 144, "228": 145, "229": 145, "230": 146, "231": 146, "232": 147, "233": 147, "239": 233}, "uri": "staff_problem_info.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/staff_problem_info.html"}
__M_END_METADATA
"""
