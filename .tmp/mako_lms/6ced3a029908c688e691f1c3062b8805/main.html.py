# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497848937.906931
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/main.html'
_template_uri = u'main.html'
_source_encoding = 'utf-8'
_exports = [u'bodyclass', u'js_overrides', u'title', u'bodyextra', 'pagetitle', u'js_extra', 'login_query', u'headextra']


main_css = "style-main-v1" 


from django.core.urlresolvers import reverse
from django.utils.http import urlquote_plus
from django.utils.translation import ugettext as _
from django.utils.translation import get_language_bidi
from branding import api as branding_api
from pipeline_mako import render_require_js_path_overrides


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        disable_window_wrap = context.get('disable_window_wrap', UNDEFINED)
        disable_footer = context.get('disable_footer', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        disable_courseware_js = context.get('disable_courseware_js', UNDEFINED)
        def js_overrides():
            return render_js_overrides(context._locals(__M_locals))
        LANGUAGE_CODE = context.get('LANGUAGE_CODE', UNDEFINED)
        self = context.get('self', UNDEFINED)
        def title():
            return render_title(context._locals(__M_locals))
        def headextra():
            return render_headextra(context._locals(__M_locals))
        EDX_ROOT_URL = context.get('EDX_ROOT_URL', UNDEFINED)
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        def bodyextra():
            return render_bodyextra(context._locals(__M_locals))
        allow_iframing = context.get('allow_iframing', UNDEFINED)
        uses_pattern_library = context.get('uses_pattern_library', UNDEFINED)
        disable_header = context.get('disable_header', UNDEFINED)
        hasattr = context.get('hasattr', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n\n')
        __M_writer(u'\n')
        online_help_token = self.online_help_token() if hasattr(self, 'online_help_token') else None 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['online_help_token'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n')
        __M_writer(u'\n<!DOCTYPE html>\n<!--[if lte IE 9]><html class="ie ie9 lte9" lang="')
        __M_writer(filters.decode.utf8(LANGUAGE_CODE))
        __M_writer(u'"><![endif]-->\n<!--[if !IE]><!--><html lang="')
        __M_writer(filters.decode.utf8(LANGUAGE_CODE))
        __M_writer(u'"><!--<![endif]-->\n<head dir="')
        __M_writer(filters.decode.utf8(static.dir_rtl()))
        __M_writer(u'">\n    <meta charset="UTF-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <meta name="viewport" content="width=device-width, initial-scale=1">\n\n')
        __M_writer(u'\n')
        __M_writer(u'\n  ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer(u'\n\n')
        if not allow_iframing:
            __M_writer(u'      <script type="text/javascript">\n        /* immediately break out of an iframe if coming from the marketing website */\n        (function(window) {\n          if (window.location !== window.top.location) {\n            window.top.location = window.location;\n          }\n        })(this);\n      </script>\n')
        __M_writer(u'\n  ')

        jsi18n_path = "js/i18n/{language}/djangojs.js".format(language=LANGUAGE_CODE)
          
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['jsi18n_path'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n  <script type="text/javascript" src="')
        __M_writer(filters.decode.utf8(static.url(jsi18n_path)))
        __M_writer(u'"></script>\n\n  <link rel="icon" type="image/x-icon" href="')
        __M_writer(filters.decode.utf8(static.url(static.get_value('favicon_path', settings.FAVICON_PATH))))
        __M_writer(u'" />\n\n  ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.decode.utf8(static.css(group=u'style-vendor')))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n  ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.decode.utf8(static.css(group=(self.attr.main_css))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n')
        if not uses_pattern_library:
            if disable_courseware_js:
                __M_writer(u'      ')
                def ccall(caller):
                    def body():
                        __M_writer = context.writer()
                        return ''
                    return [body]
                context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
                try:
                    __M_writer(filters.decode.utf8(static.js(group=u'base_vendor')))
                finally:
                    context.caller_stack.nextcaller = None
                __M_writer(u'\n      ')
                def ccall(caller):
                    def body():
                        __M_writer = context.writer()
                        return ''
                    return [body]
                context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
                try:
                    __M_writer(filters.decode.utf8(static.js(group=u'base_application')))
                finally:
                    context.caller_stack.nextcaller = None
                __M_writer(u'\n')
            else:
                __M_writer(u'      ')
                def ccall(caller):
                    def body():
                        __M_writer = context.writer()
                        return ''
                    return [body]
                context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
                try:
                    __M_writer(filters.decode.utf8(static.js(group=u'main_vendor')))
                finally:
                    context.caller_stack.nextcaller = None
                __M_writer(u'\n      ')
                def ccall(caller):
                    def body():
                        __M_writer = context.writer()
                        return ''
                    return [body]
                context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
                try:
                    __M_writer(filters.decode.utf8(static.js(group=u'application')))
                finally:
                    context.caller_stack.nextcaller = None
                __M_writer(u'\n')
        else:
            __M_writer(u'    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.decode.utf8(static.js(group=u'base_vendor')))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.decode.utf8(static.js(group=u'base_application')))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
        __M_writer(u'\n  <script>\n    window.baseUrl = "')
        __M_writer(filters.decode.utf8(settings.STATIC_URL))
        __M_writer(u'";\n    (function (require) {\n      require.config({\n          baseUrl: window.baseUrl\n      });\n    }).call(this, require || RequireJS.require);\n  </script>\n  <script type="text/javascript" src="')
        __M_writer(filters.decode.utf8(static.url("lms/js/require-config.js")))
        __M_writer(u'"></script>\n  ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_overrides'):
            context['self'].js_overrides(**pageargs)
        

        __M_writer(u'\n\n')
        if not disable_courseware_js:
            __M_writer(u'    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.decode.utf8(static.js(group=u'module-js')))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
        __M_writer(u'\n  ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'headextra'):
            context['self'].headextra(**pageargs)
        

        __M_writer(u'\n\n  ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.decode.utf8(static.optional_include_mako(is_theming_enabled=u'True',file=u'head-extra.html')))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n  ')
        runtime._include_file(context, u'widgets/optimizely.html', _template_uri)
        __M_writer(u'\n  ')
        runtime._include_file(context, u'widgets/segment-io.html', _template_uri)
        __M_writer(u'\n\n  <meta name="path_prefix" content="')
        __M_writer(filters.decode.utf8(EDX_ROOT_URL))
        __M_writer(u'">\n  <meta name="google-site-verification" content="_mipQ4AtZQDNmbtOkwehQDOgCxUUV2fb_C0b6wbiRHY" />\n\n')
        ga_acct = static.get_value("GOOGLE_ANALYTICS_ACCOUNT", settings.GOOGLE_ANALYTICS_ACCOUNT) 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['ga_acct'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n')
        if ga_acct:
            __M_writer(u'    <script type="text/javascript">\n    var _gaq = _gaq || [];\n    _gaq.push([\'_setAccount\', \'')
            __M_writer(filters.decode.utf8(ga_acct))
            __M_writer(u"']);\n    _gaq.push(['_trackPageview']);\n\n    (function() {\n      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n    })();\n    </script>\n")
        __M_writer(u'\n</head>\n\n<body class="')
        __M_writer(filters.decode.utf8(static.dir_rtl()))
        __M_writer(u' ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u' lang_')
        __M_writer(filters.decode.utf8(LANGUAGE_CODE))
        __M_writer(u'">\n\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.decode.utf8(static.optional_include_mako(is_theming_enabled=u'True',file=u'body-initial.html')))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n<div id="page-prompt"></div>\n')
        if not disable_window_wrap:
            __M_writer(u'  <div class="window-wrap" dir="')
            __M_writer(filters.decode.utf8(static.dir_rtl()))
            __M_writer(u'">\n')
        __M_writer(u'    <a class="nav-skip" href="#main">')
        __M_writer(filters.decode.utf8(_("Skip to main content")))
        __M_writer(u'</a>\n\n')
        if not disable_header:
            __M_writer(u'        ')
            runtime._include_file(context, (static.get_template_path('header.html')), _template_uri, online_help_token=online_help_token)
            __M_writer(u'\n')
        __M_writer(u'\n    <div class="content-wrapper" id="content">\n      ')
        __M_writer(filters.decode.utf8(self.body()))
        __M_writer(u'\n      ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyextra'):
            context['self'].bodyextra(**pageargs)
        

        __M_writer(u'\n    </div>\n\n')
        if not disable_footer:
            __M_writer(u'        ')
            runtime._include_file(context, (static.get_template_path('footer.html')), _template_uri)
            __M_writer(u'\n')
        __M_writer(u'\n')
        if not disable_window_wrap:
            __M_writer(u'  </div>\n')
        __M_writer(u'\n  ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n  ')
        runtime._include_file(context, u'widgets/segment-io-footer.html', _template_uri)
        __M_writer(u'\n  <script type="text/javascript" src="')
        __M_writer(filters.decode.utf8(static.url('js/vendor/noreferrer.js')))
        __M_writer(u'" charset="utf-8"></script>\n  <script type="text/javascript" src="')
        __M_writer(filters.decode.utf8(static.url('js/utils/navigation.js')))
        __M_writer(u'" charset="utf-8"></script>\n  ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.decode.utf8(static.optional_include_mako(is_theming_enabled=u'True',file=u'body-extra.html')))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n</body>\n</html>\n\n')
        __M_writer(u'\n\n<!-- Performance beacon for onload times -->\n')
        if settings.FEATURES.get('ENABLE_ONLOAD_BEACON', False):
            __M_writer(u'<script>\n  (function () {\n    var sample_rate = ')
            __M_writer(filters.decode.utf8(settings.ONLOAD_BEACON_SAMPLE_RATE))
            __M_writer(u';\n    var roll = Math.floor(Math.random() * 100)/100;\n    var onloadBeaconSent = false;\n\n    if(roll < sample_rate){\n      $(window).load(function() {\n        setTimeout(function(){\n          var t = window.performance.timing;\n\n          var data = {\n            event: "onload",\n            value: t.loadEventEnd - t.navigationStart,\n            page: window.location.href,\n          };\n\n          if (!onloadBeaconSent) {\n            $.ajax({method: "POST", url: "/performance", data: data});\n          }\n          onloadBeaconSent = true;\n        }, 0);\n      });\n    }\n  }());\n</script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_overrides(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        settings = context.get('settings', UNDEFINED)
        def js_overrides():
            return render_js_overrides(context)
        __M_writer = context.writer()
        __M_writer(u'\n    ')
        __M_writer(filters.decode.utf8(render_require_js_path_overrides(settings.REQUIRE_JS_PATH_OVERRIDES)))
        __M_writer(u'\n  ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        self = context.get('self', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        def title():
            return render_title(context)
        __M_writer = context.writer()
        __M_writer(u'\n      <title>\n       ')
        __M_writer(filters.decode.utf8(static.get_page_title_breadcrumbs(self.pagetitle())))
        __M_writer(u'\n      </title>\n  ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def bodyextra():
            return render_bodyextra(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def js_extra():
            return render_js_extra(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_login_query(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        login_redirect_url = context.get('login_redirect_url', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(filters.decode.utf8(
  u"?next={0}".format(urlquote_plus(login_redirect_url)) if login_redirect_url else ""
))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_headextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def headextra():
            return render_headextra(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 10, "18": 15, "34": 13, "37": 0, "66": 2, "67": 7, "68": 10, "69": 13, "70": 14, "74": 14, "75": 22, "76": 24, "77": 24, "78": 25, "79": 25, "80": 26, "81": 26, "82": 35, "83": 37, "88": 42, "89": 44, "90": 45, "91": 54, "92": 55, "98": 57, "99": 59, "100": 59, "101": 61, "102": 61, "110": 63, "113": 63, "121": 64, "124": 64, "125": 66, "126": 67, "127": 68, "135": 68, "138": 68, "146": 69, "149": 69, "150": 70, "151": 71, "159": 71, "162": 71, "170": 72, "173": 72, "174": 74, "175": 77, "183": 77, "186": 77, "194": 78, "197": 78, "198": 80, "199": 82, "200": 82, "201": 89, "202": 89, "207": 92, "208": 94, "209": 95, "217": 95, "220": 95, "221": 97, "226": 98, "234": 100, "237": 100, "238": 102, "239": 102, "240": 103, "241": 103, "242": 105, "243": 105, "244": 108, "248": 108, "249": 109, "250": 110, "251": 112, "252": 112, "253": 122, "254": 125, "255": 125, "260": 125, "261": 125, "262": 125, "270": 127, "273": 127, "274": 129, "275": 130, "276": 130, "277": 130, "278": 132, "279": 132, "280": 132, "281": 134, "282": 135, "283": 135, "284": 135, "285": 137, "286": 139, "287": 139, "292": 140, "293": 143, "294": 144, "295": 144, "296": 144, "297": 146, "298": 147, "299": 148, "300": 150, "305": 151, "306": 153, "307": 153, "308": 154, "309": 154, "310": 155, "311": 155, "319": 156, "322": 156, "323": 162, "324": 165, "325": 166, "326": 168, "327": 168, "333": 125, "344": 90, "351": 90, "352": 91, "353": 91, "359": 38, "367": 38, "368": 40, "369": 40, "375": 140, "386": 37, "395": 151, "406": 160, "411": 160, "419": 98, "430": 419}, "uri": "main.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/main.html"}
__M_END_METADATA
"""
