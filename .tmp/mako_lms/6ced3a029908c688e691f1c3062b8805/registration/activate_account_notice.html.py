# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849746.18354
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/registration/activate_account_notice.html'
_template_uri = 'registration/activate_account_notice.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from openedx.core.djangolib.markup import HTML, Text


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        platform_name = context.get('platform_name', UNDEFINED)
        email = context.get('email', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n<div class="wrapper-msg urgency-high">\n    <div class="msg">\n        <div class="msg-content">\n            <h2 class="title">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("You're almost there!"))))
        __M_writer(u'</h2>\n            <div class="copy">\n                <p class=\'activation-message\'>')
        __M_writer(filters.html_escape(filters.decode.utf8(Text(_(
                        "There's just one more step: Before you "
                        "enroll in a course, you need to activate "
                        "your account. We've sent an email message to "
                        "{email_start}{email}{email_end} with "
                        "instructions for activating your account. If "
                        "you don't receive this message, check your "
                        "spam folder."
                        )).format(email_start=HTML("<strong>"),
                                  email_end=HTML("</strong>"),
                                  email=email,
                                  platform_name=platform_name
                    ))))
        __M_writer(u'\n                </p>\n            </div>\n        </div>\n    </div>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"32": 11, "45": 23, "16": 2, "51": 45, "21": 1, "28": 1, "29": 5, "30": 9, "31": 9}, "uri": "registration/activate_account_notice.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/registration/activate_account_notice.html"}
__M_END_METADATA
"""
