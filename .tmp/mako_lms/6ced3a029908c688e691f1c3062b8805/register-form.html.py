# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849599.527065
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/register-form.html'
_template_uri = 'register-form.html'
_source_encoding = 'utf-8'
_exports = []



import third_party_auth
from third_party_auth import pipeline, provider
from django.utils.translation import ugettext as _
from django_countries import countries
from student.models import UserProfile


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        username = context.get('username', UNDEFINED)
        ask_for_fullname = context.get('ask_for_fullname', UNDEFINED)
        csrf_token = context.get('csrf_token', UNDEFINED)
        extauth_id = context.get('extauth_id', UNDEFINED)
        selected_provider = context.get('selected_provider', UNDEFINED)
        name = context.get('name', UNDEFINED)
        extauth_username = context.get('extauth_username', UNDEFINED)
        ask_for_email = context.get('ask_for_email', UNDEFINED)
        pipeline_urls = context.get('pipeline_urls', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        ask_for_tos = context.get('ask_for_tos', UNDEFINED)
        platform_name = context.get('platform_name', UNDEFINED)
        has_extauth_info = context.get('has_extauth_info', UNDEFINED)
        unicode = context.get('unicode', UNDEFINED)
        running_pipeline = context.get('running_pipeline', UNDEFINED)
        email = context.get('email', UNDEFINED)
        marketing_link = context.get('marketing_link', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\n<input type="hidden" name="csrfmiddlewaretoken" value="')
        __M_writer(filters.decode.utf8( csrf_token ))
        __M_writer(u'">\n\n<!-- status messages -->\n<div role="alert" class="status message">\n  <h3 class="message-title">')
        __M_writer(filters.decode.utf8(_("We're sorry, but this version of your browser is not supported. Try again using a different browser or a newer version of your browser.")))
        __M_writer(u'</h3>\n</div>\n\n<div role="alert" class="status message submission-error" tabindex="-1">\n  <h3 class="message-title">')
        __M_writer(filters.decode.utf8(_("The following errors occurred while processing your registration:")))
        __M_writer(u' </h3>\n  <ul class="message-copy"> </ul>\n</div>\n\n')
        if third_party_auth.is_enabled():
            __M_writer(u'\n')
            if not running_pipeline:
                __M_writer(u'\n  <div class="form-actions form-third-party-auth">\n\n')
                for enabled in provider.Registry.displayed_for_login():
                    __M_writer(u'    <button type="submit" class="button button-primary button-')
                    __M_writer(filters.decode.utf8(enabled.provider_id))
                    __M_writer(u' register-')
                    __M_writer(filters.decode.utf8(enabled.provider_id))
                    __M_writer(u'" onclick="thirdPartySignin(event, \'')
                    __M_writer(filters.decode.utf8(pipeline_urls[enabled.provider_id]))
                    __M_writer(u'\');">\n')
                    if enabled.icon_class:
                        __M_writer(u'      <span class="icon fa ')
                        __M_writer(filters.decode.utf8(enabled.icon_class))
                        __M_writer(u'" aria-hidden="true"></span>\n')
                    else:
                        __M_writer(u'      <span class="icon" aria-hidden="true"><img class="icon-image" src="')
                        __M_writer(filters.decode.utf8(enabled.icon_image.url))
                        __M_writer(u'" alt="')
                        __M_writer(filters.decode.utf8(enabled.name))
                        __M_writer(u' icon" /></span>\n')
                    __M_writer(u'      ')
                    __M_writer(filters.decode.utf8(_('Sign up with {provider_name}').format(provider_name=enabled.name)))
                    __M_writer(u'\n    </button>\n')
                __M_writer(u'\n  </div>\n\n  <span class="deco-divider">\n')
                __M_writer(u'    <span class="copy">')
                __M_writer(filters.decode.utf8(_('or')))
                __M_writer(u'</span>\n  </span>\n\n  <p class="instructions">\n    ')
                __M_writer(filters.decode.utf8(_('Create your own {platform_name} account below').format(platform_name=platform_name)))
                __M_writer(u'\n    <span class="note">')
                __M_writer(filters.decode.utf8(_('Required fields are noted by <strong class="indicator">bold text and an asterisk (*)</strong>.')))
                __M_writer(u'</span>\n  </p>\n\n\n')
            else:
                __M_writer(u'\n  <p class="instructions">\n')
                __M_writer(u'    ')
                __M_writer(filters.decode.utf8(_("You've successfully signed in with {selected_provider}.").format(selected_provider='<strong>%s</strong>' % selected_provider)))
                __M_writer(u'<br />\n    ')
                __M_writer(filters.decode.utf8(_("We just need a little more information before you start learning with {platform_name}.").format(platform_name=settings.PLATFORM_NAME)))
                __M_writer(u'\n  </p>\n\n')
            __M_writer(u'\n')
        else:
            __M_writer(u'\n<p class="instructions">\n  ')
            __M_writer(filters.decode.utf8(_("Please complete the following fields to register for an account. ")))
            __M_writer(u'<br />\n  ')
            __M_writer(filters.decode.utf8(_('Required fields are noted by <strong class="indicator">bold text and an asterisk (*)</strong>.')))
            __M_writer(u'\n</p>\n\n')
        __M_writer(u'\n<div class="group group-form group-form-requiredinformation">\n  <h2 class="sr">')
        __M_writer(filters.decode.utf8(_('Required Information')))
        __M_writer(u'</h2>\n\n')
        if has_extauth_info is UNDEFINED:
            __M_writer(u'\n  <ol class="list-input">\n    <li class="field required text" id="field-email">\n      <label for="email">')
            __M_writer(filters.decode.utf8(_('E-mail')))
            __M_writer(u'</label>\n      <input class="" id="email" type="email" name="email" value="')
            __M_writer(filters.decode.utf8(email))
            __M_writer(u'" placeholder="')
            __M_writer(filters.decode.utf8(_('example: username@domain.com')))
            __M_writer(u'" required aria-required="true" />\n    </li>\n\n    <li class="field required text" id="field-name">\n      <label for="name">')
            __M_writer(filters.decode.utf8(_('Full Name')))
            __M_writer(u'</label>\n      <input id="name" type="text" name="name" value="')
            __M_writer(filters.decode.utf8(name))
            __M_writer(u'" placeholder="')
            __M_writer(filters.decode.utf8(_('example: Jane Doe')))
            __M_writer(u'" required aria-required="true" aria-describedby="name-tip" />\n      <span class="tip tip-input" id="name-tip">')
            __M_writer(filters.decode.utf8(_("Your legal name, used for any certificates you earn.")))
            __M_writer(u'</span>\n    </li>\n    <li class="field required text" id="field-username">\n      <label for="username">')
            __M_writer(filters.decode.utf8(_('Public Username')))
            __M_writer(u'</label>\n      <input id="username" type="text" name="username" value="')
            __M_writer(filters.decode.utf8(username))
            __M_writer(u'" placeholder="')
            __M_writer(filters.decode.utf8(_('example: JaneDoe')))
            __M_writer(u'" required aria-required="true" aria-describedby="username-tip"/>\n      <span class="tip tip-input" id="username-tip">')
            __M_writer(filters.decode.utf8(_('Will be shown in any discussions or forums you participate in')))
            __M_writer(u' <strong>(')
            __M_writer(filters.decode.utf8(_('cannot be changed later')))
            __M_writer(u')</strong></span>\n    </li>\n\n')
            if third_party_auth.is_enabled() and running_pipeline:
                __M_writer(u'\n    <li class="is-disabled field optional password" id="field-password" hidden>\n      <label for="password">')
                __M_writer(filters.decode.utf8(_('Password')))
                __M_writer(u'</label>\n      <input id="password" type="password" name="password" value="" disabled required aria-required="true" />\n    </li>\n\n')
            else:
                __M_writer(u'\n    <li class="field required password" id="field-password">\n      <label for="password">')
                __M_writer(filters.decode.utf8(_('Password')))
                __M_writer(u'</label>\n      <input id="password" type="password" name="password" value="" required aria-required="true" />\n    </li>\n\n')
            __M_writer(u'  </ol>\n\n')
        else:
            __M_writer(u'\n  <div class="message">\n    <h3 class="message-title">')
            __M_writer(filters.decode.utf8(_("Welcome {username}").format(username=extauth_id)))
            __M_writer(u'</h3>\n    <p class="message-copy">')
            __M_writer(filters.decode.utf8(_("Enter a Public Display Name:")))
            __M_writer(u'</p>\n  </div>\n\n  <ol class="list-input">\n\n')
            if ask_for_email:
                __M_writer(u'\n    <li class="field required text" id="field-email">\n      <label for="email">')
                __M_writer(filters.decode.utf8(_("E-mail")))
                __M_writer(u'</label>\n      <input class="" id="email" type="email" name="email" value="" placeholder="')
                __M_writer(filters.decode.utf8(_('example: username@domain.com')))
                __M_writer(u'" />\n    </li>\n\n')
            __M_writer(u'\n    <li class="field required text" id="field-username">\n      <label for="username">')
            __M_writer(filters.decode.utf8(_('Public Display Name')))
            __M_writer(u'</label>\n      <input id="username" type="text" name="username" value="')
            __M_writer(filters.decode.utf8(extauth_username))
            __M_writer(u'" placeholder="')
            __M_writer(filters.decode.utf8(_('example: JaneDoe')))
            __M_writer(u'" required aria-required="true" aria-describedby="username-tip" />\n      <span class="tip tip-input" id="id="username-tip>')
            __M_writer(filters.decode.utf8(_('Will be shown in any discussions or forums you participate in')))
            __M_writer(u' <strong>(')
            __M_writer(filters.decode.utf8(_('cannot be changed later')))
            __M_writer(u')</strong></span>\n    </li>\n\n')
            if ask_for_fullname:
                __M_writer(u'\n    <li class="field required text" id="field-name">\n      <label for="name">')
                __M_writer(filters.decode.utf8(_('Full Name')))
                __M_writer(u'</label>\n      <input id="name" type="text" name="name" value="" placeholder="$_(\'example: Jane Doe\')}" aria-describedby="name-tip" />\n      <span class="tip tip-input" id="name-tip">')
                __M_writer(filters.decode.utf8(_("Your legal name, used for any certificates you earn.")))
                __M_writer(u'</span>\n    </li>\n\n')
            __M_writer(u'\n  </ol>\n\n')
        __M_writer(u'</div>\n\n<div class="group group-form group-form-secondary group-form-personalinformation">\n  <h2 class="sr">')
        __M_writer(filters.decode.utf8(_("Additional Personal Information")))
        __M_writer(u'</h2>\n\n  <ol class="list-input">\n')
        if settings.REGISTRATION_EXTRA_FIELDS['city'] != 'hidden':
            __M_writer(u'    <li class="field ')
            __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['city']))
            __M_writer(u' text" id="field-city">\n      <label for="city">')
            __M_writer(filters.decode.utf8(_('City')))
            __M_writer(u'</label>\n      <input id="city" type="text" name="city" value="" placeholder="')
            __M_writer(filters.decode.utf8(_('example: New York')))
            __M_writer(u'" aria-describedby="city-tip" ')
            __M_writer(filters.decode.utf8('required aria-required="true"' if settings.REGISTRATION_EXTRA_FIELDS['city'] == 'required' else ''))
            __M_writer(u' />\n    </li>\n')
        if settings.REGISTRATION_EXTRA_FIELDS['country'] != 'hidden':
            __M_writer(u'    <li class="field-group">\n    <div class="field ')
            __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['country']))
            __M_writer(u' select" id="field-country">\n        <label for="country">')
            __M_writer(filters.decode.utf8(_("Country")))
            __M_writer(u'</label>\n        <select id="country" name="country" ')
            __M_writer(filters.decode.utf8('required aria-required="true"' if settings.REGISTRATION_EXTRA_FIELDS['country'] == 'required' else ''))
            __M_writer(u'>\n          <option value="">--</option>\n')
            for code, country_name in countries:
                __M_writer(u'          <option value="')
                __M_writer(filters.decode.utf8(code))
                __M_writer(u'">')
                __M_writer(filters.decode.utf8( unicode(country_name) ))
                __M_writer(u'</option>\n')
            __M_writer(u'        </select>\n      </div>\n    </li>\n')
        if settings.REGISTRATION_EXTRA_FIELDS['level_of_education'] != 'hidden':
            __M_writer(u'    <li class="field-group field-education-level">\n    <div class="field ')
            __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['level_of_education']))
            __M_writer(u' select" id="field-education-level">\n        <label for="education-level">')
            __M_writer(filters.decode.utf8(_("Highest Level of Education Completed")))
            __M_writer(u'</label>\n        <select id="education-level" name="level_of_education" ')
            __M_writer(filters.decode.utf8('required aria-required="true"' if settings.REGISTRATION_EXTRA_FIELDS['level_of_education'] == 'required' else ''))
            __M_writer(u'>\n          <option value="">--</option>\n')
            for code, ed_level in UserProfile.LEVEL_OF_EDUCATION_CHOICES:
                __M_writer(u'          <option value="')
                __M_writer(filters.decode.utf8(code))
                __M_writer(u'">')
                __M_writer(filters.decode.utf8(_(ed_level)))
                __M_writer(u'</option>\n')
            __M_writer(u'        </select>\n      </div>\n    </li>\n')
        if settings.REGISTRATION_EXTRA_FIELDS['gender'] != 'hidden':
            __M_writer(u'    <li class="field-group field-gender">\n      <div class="field ')
            __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['gender']))
            __M_writer(u' select" id="field-gender">\n        <label for="gender">')
            __M_writer(filters.decode.utf8(_("Gender")))
            __M_writer(u'</label>\n        <select id="gender" name="gender" ')
            __M_writer(filters.decode.utf8('required aria-required="true"' if settings.REGISTRATION_EXTRA_FIELDS['gender'] == 'required' else ''))
            __M_writer(u'>\n          <option value="">--</option>\n')
            for code, gender in UserProfile.GENDER_CHOICES:
                __M_writer(u'          <option value="')
                __M_writer(filters.decode.utf8(code))
                __M_writer(u'">')
                __M_writer(filters.decode.utf8(_(gender)))
                __M_writer(u'</option>\n')
            __M_writer(u'        </select>\n      </div>\n    </li>\n')
        if settings.REGISTRATION_EXTRA_FIELDS['year_of_birth'] != 'hidden':
            __M_writer(u'    <li class="field-group field-yob">\n      <div class="field ')
            __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['year_of_birth']))
            __M_writer(u' select" id="field-yob">\n        <label for="yob">')
            __M_writer(filters.decode.utf8(_("Year of Birth")))
            __M_writer(u'</label>\n        <select id="yob" name="year_of_birth" ')
            __M_writer(filters.decode.utf8('required aria-required="true"' if settings.REGISTRATION_EXTRA_FIELDS['year_of_birth'] == 'required' else ''))
            __M_writer(u'>\n          <option value="">--</option>\n')
            for year in UserProfile.VALID_YEARS:
                __M_writer(u'          <option value="')
                __M_writer(filters.decode.utf8(year))
                __M_writer(u'">')
                __M_writer(filters.decode.utf8(year))
                __M_writer(u'</option>\n')
            __M_writer(u'        </select>\n      </div>\n')
        __M_writer(u'    </li>\n  </ol>\n</div>\n\n<div class="group group-form group-form-personalinformation2">\n  <ol class="list-input">\n')
        if settings.REGISTRATION_EXTRA_FIELDS['mailing_address'] != 'hidden':
            __M_writer(u'    <li class="field ')
            __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['mailing_address']))
            __M_writer(u' text" id="field-address-mailing">\n      <label for="address-mailing">')
            __M_writer(filters.decode.utf8(_("Mailing Address")))
            __M_writer(u'</label>\n      <textarea id="address-mailing" name="mailing_address" value="" ')
            __M_writer(filters.decode.utf8('required aria-required="true"' if settings.REGISTRATION_EXTRA_FIELDS['mailing_address'] == 'required' else ''))
            __M_writer(u'></textarea>\n    </li>\n')
        __M_writer(u'\n')
        if settings.REGISTRATION_EXTRA_FIELDS['goals'] != 'hidden':
            __M_writer(u'    <li class="field ')
            __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['goals']))
            __M_writer(u' text" id="field-goals">\n      <label for="goals">')
            __M_writer(filters.decode.utf8(_("Please share with us your reasons for registering with {platform_name}").format(platform_name=platform_name)))
            __M_writer(u'</label>\n      <textarea id="goals" name="goals" value="" ')
            __M_writer(filters.decode.utf8('required aria-required="true"' if settings.REGISTRATION_EXTRA_FIELDS['goals'] == 'required' else ''))
            __M_writer(u'></textarea>\n    </li>\n')
        __M_writer(u'  </ol>\n</div>\n\n<div class="group group-form group-form-accountacknowledgements">\n  <h2 class="sr">')
        __M_writer(filters.decode.utf8(_("Account Acknowledgements")))
        __M_writer(u'</h2>\n\n  <ol class="list-input">\n    <li class="field-group">\n\n')
        if has_extauth_info is UNDEFINED or ask_for_tos :
            __M_writer(u'      <div class="field required checkbox" id="field-tos">\n        <input id="tos-yes" type="checkbox" name="terms_of_service" value="true" required aria-required="true" />\n        <label for="tos-yes">')
            __M_writer(filters.decode.utf8(_('I agree to the {link_start}Terms of Service{link_end}').format(
          link_start='<a href="{url}" class="new-vp" tabindex="-1">'.format(url=marketing_link('TOS')),
          link_end='</a>')))
            __M_writer(u'</label>\n      </div>\n')
        __M_writer(u'\n')
        if settings.REGISTRATION_EXTRA_FIELDS['honor_code'] != 'hidden':
            if marketing_link('HONOR') and marketing_link('HONOR') != '#':
                __M_writer(u'      <div class="field ')
                __M_writer(filters.decode.utf8(settings.REGISTRATION_EXTRA_FIELDS['honor_code']))
                __M_writer(u' checkbox" id="field-honorcode">\n        <input id="honorcode-yes" type="checkbox" name="honor_code" value="true" />\n        ')

                honor_code_path = marketing_link('HONOR')
                        
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['honor_code_path'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n        <label for="honorcode-yes">')
                __M_writer(filters.decode.utf8(_('I agree to the {link_start}Honor Code{link_end}').format(
          link_start='<a href="{url}" class="new-vp" tabindex="-1">'.format(url=honor_code_path),
          link_end='</a>')))
                __M_writer(u'</label>\n      </div>\n')
        __M_writer(u'    </li>\n  </ol>\n</div>\n\n<div class="form-actions">\n  <button name="submit" type="submit" id="submit" class="action action-primary action-update register-button">')
        __M_writer(filters.decode.utf8(_('Register')))
        __M_writer(u' <span class="orn-plus">+</span> ')
        __M_writer(filters.decode.utf8(_('Create My Account')))
        __M_writer(u'</button>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 1, "24": 0, "46": 7, "47": 9, "48": 9, "49": 13, "50": 13, "51": 17, "52": 17, "53": 21, "54": 22, "55": 23, "56": 24, "57": 27, "58": 29, "59": 29, "60": 29, "61": 29, "62": 29, "63": 29, "64": 29, "65": 30, "66": 31, "67": 31, "68": 31, "69": 32, "70": 33, "71": 33, "72": 33, "73": 33, "74": 33, "75": 35, "76": 35, "77": 35, "78": 38, "79": 44, "80": 44, "81": 44, "82": 48, "83": 48, "84": 49, "85": 49, "86": 53, "87": 54, "88": 57, "89": 57, "90": 57, "91": 58, "92": 58, "93": 62, "94": 63, "95": 64, "96": 66, "97": 66, "98": 67, "99": 67, "100": 71, "101": 73, "102": 73, "103": 75, "104": 76, "105": 79, "106": 79, "107": 80, "108": 80, "109": 80, "110": 80, "111": 84, "112": 84, "113": 85, "114": 85, "115": 85, "116": 85, "117": 86, "118": 86, "119": 89, "120": 89, "121": 90, "122": 90, "123": 90, "124": 90, "125": 91, "126": 91, "127": 91, "128": 91, "129": 94, "130": 95, "131": 97, "132": 97, "133": 101, "134": 102, "135": 104, "136": 104, "137": 109, "138": 111, "139": 112, "140": 114, "141": 114, "142": 115, "143": 115, "144": 120, "145": 121, "146": 123, "147": 123, "148": 124, "149": 124, "150": 128, "151": 130, "152": 130, "153": 131, "154": 131, "155": 131, "156": 131, "157": 132, "158": 132, "159": 132, "160": 132, "161": 135, "162": 136, "163": 138, "164": 138, "165": 140, "166": 140, "167": 144, "168": 148, "169": 151, "170": 151, "171": 154, "172": 155, "173": 155, "174": 155, "175": 156, "176": 156, "177": 157, "178": 157, "179": 157, "180": 157, "181": 160, "182": 161, "183": 162, "184": 162, "185": 163, "186": 163, "187": 164, "188": 164, "189": 166, "190": 167, "191": 167, "192": 167, "193": 167, "194": 167, "195": 169, "196": 173, "197": 174, "198": 175, "199": 175, "200": 176, "201": 176, "202": 177, "203": 177, "204": 179, "205": 180, "206": 180, "207": 180, "208": 180, "209": 180, "210": 182, "211": 186, "212": 187, "213": 188, "214": 188, "215": 189, "216": 189, "217": 190, "218": 190, "219": 192, "220": 193, "221": 193, "222": 193, "223": 193, "224": 193, "225": 195, "226": 199, "227": 200, "228": 201, "229": 201, "230": 202, "231": 202, "232": 203, "233": 203, "234": 205, "235": 206, "236": 206, "237": 206, "238": 206, "239": 206, "240": 208, "241": 211, "242": 217, "243": 218, "244": 218, "245": 218, "246": 219, "247": 219, "248": 220, "249": 220, "250": 223, "251": 224, "252": 225, "253": 225, "254": 225, "255": 226, "256": 226, "257": 227, "258": 227, "259": 230, "260": 234, "261": 234, "262": 239, "263": 240, "264": 242, "267": 244, "268": 247, "269": 248, "270": 250, "271": 251, "272": 251, "273": 251, "274": 253, "280": 255, "281": 256, "284": 258, "285": 262, "286": 267, "287": 267, "288": 267, "289": 267, "295": 289}, "uri": "register-form.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/register-form.html"}
__M_END_METADATA
"""
