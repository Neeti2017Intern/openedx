# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849019.444249
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/dashboard/_dashboard_status_verification.html'
_template_uri = 'dashboard/_dashboard_status_verification.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        verification_status = context.get('verification_status', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if verification_status == 'approved':
            __M_writer(u'    <li class="status status-verification is-accepted">\n        <span class="title status-title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Current Verification Status: Approved"))))
            __M_writer(u'</span>\n        <p class="status-note">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Your edX verification has been approved. Your verification is effective for one year after submission."))))
            __M_writer(u'</p>\n    </li>\n')
        __M_writer(u'\n')
        if verification_status == 'pending':
            __M_writer(u'    <li class="status status-verification is-pending">\n        <span class="title status-title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Current Verification Status: Pending"))))
            __M_writer(u'</span>\n        <p class="status-note">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Your edX ID verification is pending. Your verification information has been submitted and will be reviewed shortly."))))
            __M_writer(u'</p>\n    </li>\n')
        __M_writer(u'\n')
        if verification_status in ['must_reverify', 'expired']:
            __M_writer(u'    <li class="status status-verification is-denied">\n        <span class="title status-title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Current Verification Status: Expired"))))
            __M_writer(u'</span>\n        <p class="status-note">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Your verification has expired. To receive a verified certificate, you must submit a new photo of yourself and your government-issued photo ID before the verification deadline for your course."))))
            __M_writer(u'</p>\n        <div class="btn-reverify">\n            <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('verify_student_reverify'))))
            __M_writer(u'" class="action action-reverify">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Resubmit Verification"))))
            __M_writer(u'</a>\n        </div>\n    </li>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 3, "28": 2, "31": 1, "37": 1, "38": 2, "39": 6, "40": 8, "41": 9, "42": 10, "43": 10, "44": 11, "45": 11, "46": 14, "47": 15, "48": 16, "49": 17, "50": 17, "51": 18, "52": 18, "53": 21, "54": 22, "55": 23, "56": 24, "57": 24, "58": 25, "59": 25, "60": 27, "61": 27, "62": 27, "63": 27, "69": 63}, "uri": "dashboard/_dashboard_status_verification.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/dashboard/_dashboard_status_verification.html"}
__M_END_METADATA
"""
