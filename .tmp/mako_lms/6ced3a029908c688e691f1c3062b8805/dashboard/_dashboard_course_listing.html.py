# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946129.91922
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/dashboard/_dashboard_course_listing.html'
_template_uri = u'dashboard/_dashboard_course_listing.html'
_source_encoding = 'utf-8'
_exports = []



import urllib

from django.utils.translation import ugettext as _
from django.utils.translation import ungettext
from django.core.urlresolvers import reverse
from course_modes.models import CourseMode
from course_modes.helpers import enrollment_mode_display
from openedx.core.djangolib.js_utils import dump_js_escaped_json
from openedx.core.djangolib.markup import HTML, Text
from student.helpers import (
  VERIFY_STATUS_NEED_TO_VERIFY,
  VERIFY_STATUS_SUBMITTED,
  VERIFY_STATUS_RESUBMITTED,
  VERIFY_STATUS_APPROVED,
  VERIFY_STATUS_MISSED_DEADLINE,
  VERIFY_STATUS_NEED_TO_REVERIFY,
  DISABLE_UNENROLL_CERT_STATES,
)


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,course_overview,enrollment,show_courseware_link,cert_status,can_unenroll,credit_status,show_email_settings,course_mode_info,show_refund_option,is_paid_course,is_course_blocked,verification_status,course_requirements,dashboard_index,share_settings,related_programs,display_course_modes_on_dashboard,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(course_overview=course_overview,can_unenroll=can_unenroll,show_courseware_link=show_courseware_link,is_course_blocked=is_course_blocked,enrollment=enrollment,related_programs=related_programs,credit_status=credit_status,pageargs=pageargs,cert_status=cert_status,is_paid_course=is_paid_course,dashboard_index=dashboard_index,course_requirements=course_requirements,share_settings=share_settings,show_email_settings=show_email_settings,verification_status=verification_status,course_mode_info=course_mode_info,show_refund_option=show_refund_option,display_course_modes_on_dashboard=display_course_modes_on_dashboard)
        user_timezone = context.get('user_timezone', UNDEFINED)
        course_optouts = context.get('course_optouts', UNDEFINED)
        unicode = context.get('unicode', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        use_ecommerce_payment_flow = context.get('use_ecommerce_payment_flow', UNDEFINED)
        ecommerce_payment_page = context.get('ecommerce_payment_page', UNDEFINED)
        user_language = context.get('user_language', UNDEFINED)
        request = context.get('request', UNDEFINED)
        course_program_info = context.get('course_program_info', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        user = context.get('user', UNDEFINED)
        str = context.get('str', UNDEFINED)
        endif = context.get('endif', UNDEFINED)
        isinstance = context.get('isinstance', UNDEFINED)
        marketing_link = context.get('marketing_link', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')

        reverify_link = reverse('verify_student_reverify')
        cert_name_short = course_overview.cert_name_short
        if cert_name_short == "":
          cert_name_short = settings.CERT_NAME_SHORT
        
        cert_name_long = course_overview.cert_name_long
        if cert_name_long == "":
          cert_name_long = settings.CERT_NAME_LONG
        billing_email = settings.PAYMENT_SUPPORT_EMAIL
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['billing_email','cert_name_long','cert_name_short','reverify_link'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n')
        __M_writer(u'\n\n<li class="course-item">\n')
        if display_course_modes_on_dashboard:
            __M_writer(u'    ')

            course_verified_certs = enrollment_mode_display(
                enrollment.mode,
                verification_status.get('status'),
                course_overview.id
            )
                
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_verified_certs'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n    ')

            mode_class = course_verified_certs.get('display_mode', '')
            if mode_class != '':
                mode_class = ' ' + mode_class ;
                
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['mode_class'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n')
        else:
            __M_writer(u'    ')
            mode_class = '' 
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['mode_class'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n')
        __M_writer(u'<div class="course-container">\n<article class="course')
        __M_writer(filters.html_escape(filters.decode.utf8(mode_class)))
        __M_writer(u'">\n  ')
        course_target = reverse('info', args=[unicode(course_overview.id)]) 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_target'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n  <section class="details" aria-labelledby="details-heading-')
        __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
        __M_writer(u'">\n      <h2 class="hd hd-2 sr" id="details-heading-')
        __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
        __M_writer(u'">')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Course details'))))
        __M_writer(u'</h2>\n    <div class="wrapper-course-image" aria-hidden="true">\n')
        if show_courseware_link:
            if not is_course_blocked:
                __M_writer(u'            <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_target)))
                __M_writer(u'" data-course-key="')
                __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
                __M_writer(u'" class="cover">\n              <img src="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.image_urls['small'])))
                __M_writer(u'" class="course-image" alt="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('{course_number} {course_name} Home Page').format(course_number=course_overview.number, course_name=course_overview.display_name_with_default))))
                __M_writer(u'" />\n            </a>\n')
            else:
                __M_writer(u'            <a class="fade-cover">\n              <img src="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.image_urls['small'])))
                __M_writer(u'" class="course-image" alt="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('{course_number} {course_name} Cover Image').format(course_number=course_overview.number, course_name=course_overview.display_name_with_default))))
                __M_writer(u'" />\n            </a>\n')
        else:
            __M_writer(u'        <a class="cover">\n          <img src="')
            __M_writer(filters.html_escape(filters.decode.utf8(course_overview.image_urls['small'])))
            __M_writer(u'" class="course-image" alt="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('{course_number} {course_name} Cover Image').format(course_number=course_overview.number, course_name=course_overview.display_name_with_default))))
            __M_writer(u'" />\n        </a>\n')
        if display_course_modes_on_dashboard and course_verified_certs.get('display_mode') != 'audit':
            __M_writer(u'        <span class="sts-enrollment" title="')
            __M_writer(filters.html_escape(filters.decode.utf8(course_verified_certs.get('enrollment_title'))))
            __M_writer(u'">\n          <span class="label">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Enrolled as: "))))
            __M_writer(u'</span>\n')
            if course_verified_certs.get('show_image'):
                __M_writer(u'              <img class="deco-graphic" src="')
                __M_writer(filters.html_escape(filters.decode.utf8(static.url('images/verified-ribbon.png'))))
                __M_writer(u'" alt="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_verified_certs.get('image_alt'))))
                __M_writer(u'" />\n')
            __M_writer(u'          <div class="sts-enrollment-value">')
            __M_writer(filters.html_escape(filters.decode.utf8(course_verified_certs.get('enrollment_value'))))
            __M_writer(u'</div>\n        </span>\n')
        __M_writer(u'    </div>\n      <div class="wrapper-course-details">\n        <h3 class="course-title">\n')
        if show_courseware_link:
            if not is_course_blocked:
                __M_writer(u'              <a data-course-key="')
                __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
                __M_writer(u'" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_target)))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                __M_writer(u'</a>\n')
            else:
                __M_writer(u'              <a class="disable-look" data-course-key="')
                __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                __M_writer(u'</a>\n')
        else:
            __M_writer(u'            <span>')
            __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
            __M_writer(u'</span>\n')
        __M_writer(u'        </h3>\n        <div class="course-info">\n          <span class="info-university">')
        __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_org_with_default)))
        __M_writer(u' - </span>\n          <span class="info-course-id">')
        __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_number_with_default)))
        __M_writer(u'</span>\n          ')

        if course_overview.start_date_is_still_default:
            container_string = _("Coming Soon")
            course_date = None
        else:
            format = 'shortDate'
            if course_overview.has_ended():
                container_string = _("Ended - {date}")
                course_date = course_overview.end
            elif course_overview.has_started():
                container_string = _("Started - {date}")
                course_date = course_overview.start
            elif course_overview.starts_within(days=5):
                container_string = _("Starts - {date}")
                course_date = course_overview.start
                format = 'defaultFormat'
            else: ## hasn't started yet
                container_string = _("Starts - {date}")
                course_date = course_overview.start
            endif
        endif
                  
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['container_string','course_date','format'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n')
        if isinstance(course_date, str):
            __M_writer(u'                    <span class="info-date-block" data-tooltip="Hi">')
            __M_writer(filters.html_escape(filters.decode.utf8(_(container_string).format(date=course_date))))
            __M_writer(u'</span>\n')
        elif course_date is not None:
            __M_writer(u'                    ')

            course_date_string = course_date.strftime('%Y-%m-%dT%H:%M:%S%z')
                                
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_date_string'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n                    <span class="info-date-block localized-datetime" data-language="')
            __M_writer(filters.html_escape(filters.decode.utf8(user_language)))
            __M_writer(u'" data-tooltip="Hi" data-timezone="')
            __M_writer(filters.html_escape(filters.decode.utf8(user_timezone)))
            __M_writer(u'" data-datetime="')
            __M_writer(filters.html_escape(filters.decode.utf8(course_date_string)))
            __M_writer(u'" data-format=')
            __M_writer(filters.html_escape(filters.decode.utf8(format)))
            __M_writer(u' data-string="')
            __M_writer(filters.html_escape(filters.decode.utf8(container_string)))
            __M_writer(u'"></span>\n')
        __M_writer(u'        </div>\n        <div class="wrapper-course-actions">\n          <div class="course-actions">\n')
        if show_courseware_link:
            if course_overview.has_ended():
                if not is_course_blocked:
                    __M_writer(u'                  <a href="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_target)))
                    __M_writer(u'" class="enter-course archived" data-course-key="')
                    __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
                    __M_writer(u'">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('View Archived Course'))))
                    __M_writer(u'<span class="sr">&nbsp;')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'</span></a>\n')
                else:
                    __M_writer(u'                  <a class="enter-course-blocked archived" data-course-key="')
                    __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
                    __M_writer(u'">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('View Archived Course'))))
                    __M_writer(u'<span class="sr">&nbsp;')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'</span></a>\n')
            else:
                if not is_course_blocked:
                    __M_writer(u'                  <a href="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_target)))
                    __M_writer(u'" class="enter-course" data-course-key="')
                    __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
                    __M_writer(u'">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('View Course'))))
                    __M_writer(u'<span class="sr">&nbsp;')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'</span></a>\n')
                else:
                    __M_writer(u'                  <a class="enter-course-blocked" data-course-key="')
                    __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
                    __M_writer(u'">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('View Course'))))
                    __M_writer(u'<span class="sr">&nbsp;')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'</span></a>\n')
            __M_writer(u'\n')
            if share_settings:
                __M_writer(u'                ')

                if share_settings.get("CUSTOM_COURSE_URLS", False):
                  if course_overview.social_sharing_url:
                    share_url = urllib.quote_plus(course_overview.social_sharing_url)
                  else:
                    share_url = ''
                else:
                  share_url = urllib.quote_plus(request.build_absolute_uri(reverse('about_course', args=[unicode(course_overview.id)])))
                share_window_name = 'shareWindow'
                share_window_config = 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=640, height=480'
                                
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['share_window_name','share_window_config','share_url'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n')
                if share_url and share_settings.get('DASHBOARD_FACEBOOK', False):
                    __M_writer(u'                  ')
                    facebook_url = 'https://www.facebook.com/sharer/sharer.php?u=' + share_url 
                    
                    __M_locals_builtin_stored = __M_locals_builtin()
                    __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['facebook_url'] if __M_key in __M_locals_builtin_stored]))
                    __M_writer(u'\n                    <a\n                      data-tooltip="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Share on Facebook'))))
                    __M_writer(u'"\n                      class="action action-facebook"\n                      aria-haspopup="true"\n                      aria-expanded="false"\n                      href="')
                    __M_writer(filters.html_escape(filters.decode.utf8(facebook_url)))
                    __M_writer(u'"\n                      target="_blank"\n                      title="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Share on Facebook'))))
                    __M_writer(u'"\n                      onclick="window.open(\'')
                    __M_writer(filters.html_escape(filters.decode.utf8(facebook_url)))
                    __M_writer(u"', '")
                    __M_writer(filters.html_escape(filters.decode.utf8(share_window_name)))
                    __M_writer(u"', '")
                    __M_writer(filters.html_escape(filters.decode.utf8(share_window_config)))
                    __M_writer(u'\'); return false;">\n                      <span class="sr">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Facebook'))))
                    __M_writer(u'</span>\n                      <span class="fa fa-facebook" aria-hidden="true"></span>\n                    </a>\n')
                if share_url and share_settings.get('DASHBOARD_TWITTER', False):
                    __M_writer(u'                  ')
                    share_text_default = _("I'm learning on {platform_name}:").format(platform_name=settings.PLATFORM_NAME) 
                    
                    __M_locals_builtin_stored = __M_locals_builtin()
                    __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['share_text_default'] if __M_key in __M_locals_builtin_stored]))
                    __M_writer(u'\n                  ')
                    share_text = urllib.quote_plus(share_settings.get('DASHBOARD_TWITTER_TEXT', share_text_default)) 
                    
                    __M_locals_builtin_stored = __M_locals_builtin()
                    __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['share_text'] if __M_key in __M_locals_builtin_stored]))
                    __M_writer(u'\n                  ')
                    twitter_url = 'https://twitter.com/intent/tweet?text=' + share_text + '%20' + share_url 
                    
                    __M_locals_builtin_stored = __M_locals_builtin()
                    __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['twitter_url'] if __M_key in __M_locals_builtin_stored]))
                    __M_writer(u'\n                    <a\n                      data-tooltip="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Share on Twitter'))))
                    __M_writer(u'"\n                      class="action action-twitter"\n                      aria-haspopup="true"\n                      aria-expanded="false"\n                      href="')
                    __M_writer(filters.html_escape(filters.decode.utf8(twitter_url)))
                    __M_writer(u'"\n                      target="_blank"\n                      title="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Share on Twitter'))))
                    __M_writer(u'"\n                      onclick="window.open(\'')
                    __M_writer(filters.html_escape(filters.decode.utf8(twitter_url)))
                    __M_writer(u"', '")
                    __M_writer(filters.html_escape(filters.decode.utf8(share_window_name)))
                    __M_writer(u"', '")
                    __M_writer(filters.html_escape(filters.decode.utf8(share_window_config)))
                    __M_writer(u'\'); return false;">\n                      <span class="sr">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Twitter'))))
                    __M_writer(u'</span>\n                      <span class="fa fa-twitter" aria-hidden="true"></span>\n                    </a>\n')
        __M_writer(u'            <div class="wrapper-action-more" data-course-key="')
        __M_writer(filters.html_escape(filters.decode.utf8(enrollment.course_id)))
        __M_writer(u'">\n              <button type="button" class="action action-more" id="actions-dropdown-link-')
        __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
        __M_writer(u'" aria-haspopup="true" aria-expanded="false" aria-controls="actions-dropdown-')
        __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
        __M_writer(u'" data-course-number="')
        __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
        __M_writer(u'" data-course-name="')
        __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
        __M_writer(u'" data-dashboard-index="')
        __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
        __M_writer(u'">\n                <span class="sr">')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Course options for'))))
        __M_writer(u'</span>\n                <span class="sr">&nbsp;\n                  ')
        __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
        __M_writer(u'\n                </span>\n                <span class="fa fa-cog" aria-hidden="true"></span>\n              </button>\n              <div class="actions-dropdown" id="actions-dropdown-')
        __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
        __M_writer(u'" tabindex="-1">\n                <ul class="actions-dropdown-list" id="actions-dropdown-list-')
        __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
        __M_writer(u'" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Available Actions'))))
        __M_writer(u'" role="menu">\n')
        if can_unenroll:
            __M_writer(u'                    <li class="actions-item" id="actions-item-unenroll-')
            __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
            __M_writer(u'">\n')
            if is_paid_course and show_refund_option:
                if not is_course_blocked:
                    __M_writer(u'                      <a href="#unenroll-modal" class="action action-unenroll" rel="leanModal" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the purchased course %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("You will be refunded the amount you paid."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
                else:
                    __M_writer(u'                      <a class="action action-unenroll is-disabled" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the purchased course %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("You will be refunded the amount you paid."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
            elif is_paid_course and not show_refund_option:
                if not is_course_blocked:
                    __M_writer(u'                      <a href="#unenroll-modal" class="action action-unenroll" rel="leanModal" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the purchased course %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("You will not be refunded the amount you paid."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
                else:
                    __M_writer(u'                      <a class="action action-unenroll is-disabled" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the purchased course %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("You will not be refunded the amount you paid."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
            elif enrollment.mode != "verified":
                if not is_course_blocked:
                    __M_writer(u'                      <a href="#unenroll-modal" class="action action-unenroll" rel="leanModal" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
                else:
                    __M_writer(u'                      <a class="action action-unenroll is-disabled" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
            elif show_refund_option:
                if not is_course_blocked:
                    __M_writer(u'                      <a href="#unenroll-modal" class="action action-unenroll" rel="leanModal" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'" data-cert-name-long="')
                    __M_writer(filters.html_escape(filters.decode.utf8(cert_name_long)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the verified %(cert_name_long)s track of %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("You will be refunded the amount you paid."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
                else:
                    __M_writer(u'                      <a class="action action-unenroll is-disabled" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'" data-cert-name-long="')
                    __M_writer(filters.html_escape(filters.decode.utf8(cert_name_long)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the verified %(cert_name_long)s track of %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("You will be refunded the amount you paid."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
            else:
                if not is_course_blocked:
                    __M_writer(u'                      <a href="#unenroll-modal" class="action action-unenroll" rel="leanModal" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'" data-cert-name-long="')
                    __M_writer(filters.html_escape(filters.decode.utf8(cert_name_long)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the verified %(cert_name_long)s track of %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("The refund deadline for this course has passed, so you will not receive a refund."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
                else:
                    __M_writer(u'                      <a class="action action-unenroll is-disabled" data-course-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                    __M_writer(u'" data-course-number="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                    __M_writer(u'" data-course-name="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_overview.display_name_with_default)))
                    __M_writer(u'" data-dashboard-index="')
                    __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                    __M_writer(u'" data-cert-name-long="')
                    __M_writer(filters.html_escape(filters.decode.utf8(cert_name_long)))
                    __M_writer(u'"\n                         data-track-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Are you sure you want to unenroll from the verified %(cert_name_long)s track of %(course_name)s (%(course_number)s)?"))))
                    __M_writer(u'"\n                         data-refund-info="')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("The refund deadline for this course has passed, so you will not receive a refund."))))
                    __M_writer(u'">\n                        ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Unenroll'))))
                    __M_writer(u'\n                      </a>\n')
            __M_writer(u'                  </li>\n')
        __M_writer(u'                  <li class="actions-item" id="actions-item-email-settings-')
        __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
        __M_writer(u'">\n')
        if show_email_settings:
            if not is_course_blocked:
                __M_writer(u'                        <a href="#email-settings-modal" class="action action-email-settings" rel="leanModal" data-course-id="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                __M_writer(u'" data-course-number="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                __M_writer(u'" data-dashboard-index="')
                __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                __M_writer(u'" data-optout="')
                __M_writer(filters.html_escape(filters.decode.utf8(unicode(course_overview.id) in course_optouts)))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Email Settings'))))
                __M_writer(u'</a>\n')
            else:
                __M_writer(u'                        <a class="action action-email-settings is-disabled" data-course-id="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                __M_writer(u'" data-course-number="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.number)))
                __M_writer(u'" data-dashboard-index="')
                __M_writer(filters.html_escape(filters.decode.utf8(dashboard_index)))
                __M_writer(u'" data-optout="')
                __M_writer(filters.html_escape(filters.decode.utf8(unicode(course_overview.id) in course_optouts)))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Email Settings'))))
                __M_writer(u'</a>\n')
        __M_writer(u'                  </li>\n                </ul>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n  </section>\n  <footer class="wrapper-messages-primary">\n    <ul class="messages-list">\n')
        if related_programs:
            __M_writer(u'      <div class="message message-related-programs is-shown">\n        <span class="related-programs-preface" tabindex="0">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Related Programs'))))
            __M_writer(u':</span>\n        <ul>\n')
            for program in related_programs:
                __M_writer(u'          <li>\n            <span class="category-icon ')
                __M_writer(filters.html_escape(filters.decode.utf8(program['category'].lower())))
                __M_writer(u'-icon" aria-hidden="true"></span>\n            <span><a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(program['detail_url'])))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(u'{name} {category}'.format(name=program['name'], category=program['category']))))
                __M_writer(u'</a></span>\n          </li>\n')
            __M_writer(u'        </ul>\n      </div>\n')
        __M_writer(u'\n')
        if course_overview.may_certify() and cert_status:
            __M_writer(u'        ')
            runtime._include_file(context, u'_dashboard_certificate_information.html', _template_uri, cert_status=cert_status,course_overview=course_overview, enrollment=enrollment, reverify_link=reverify_link)
            __M_writer(u'\n')
        __M_writer(u'\n')
        if credit_status is not None:
            __M_writer(u'        ')
            runtime._include_file(context, u'_dashboard_credit_info.html', _template_uri, credit_status=credit_status)
            __M_writer(u'\n')
        __M_writer(u'\n')
        if verification_status.get('status') in [VERIFY_STATUS_NEED_TO_VERIFY, VERIFY_STATUS_SUBMITTED, VERIFY_STATUS_RESUBMITTED, VERIFY_STATUS_APPROVED, VERIFY_STATUS_NEED_TO_REVERIFY] and not is_course_blocked:
            __M_writer(u'        <div class="message message-status wrapper-message-primary is-shown">\n')
            if verification_status['status'] == VERIFY_STATUS_NEED_TO_VERIFY:
                __M_writer(u'            <div class="verification-reminder">\n')
                if verification_status['days_until_deadline'] is not None:
                    __M_writer(u'                <h4 class="message-title">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Verification not yet complete.'))))
                    __M_writer(u'</h4>\n                <p class="message-copy">')
                    __M_writer(filters.html_escape(filters.decode.utf8(ungettext(
                  'You only have {days} day left to verify for this course.',
                  'You only have {days} days left to verify for this course.',
                  verification_status['days_until_deadline']
                ).format(days=verification_status['days_until_deadline']))))
                    __M_writer(u'</p>\n')
                else:
                    __M_writer(u'                <h4 class="message-title">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Almost there!'))))
                    __M_writer(u'</h4>\n                <p class="message-copy">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('You still need to verify for this course.'))))
                    __M_writer(u'</p>\n')
                __M_writer(u'            </div>\n            <div class="verification-cta">\n              <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(reverse('verify_student_verify_now', kwargs={'course_id': unicode(course_overview.id)}))))
                __M_writer(u'" class="btn" data-course-id="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Verify Now'))))
                __M_writer(u'</a>\n            </div>\n')
            elif verification_status['status'] == VERIFY_STATUS_SUBMITTED:
                __M_writer(u'            <h4 class="message-title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('You have submitted your verification information.'))))
                __M_writer(u'</h4>\n            <p class="message-copy">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('You will see a message on your dashboard when the verification process is complete (usually within 1-2 days).'))))
                __M_writer(u'</p>\n')
            elif verification_status['status'] == VERIFY_STATUS_RESUBMITTED:
                __M_writer(u'            <h4 class="message-title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your current verification will expire soon!'))))
                __M_writer(u'</h4>\n            <p class="message-copy">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('You have submitted your reverification information. You will see a message on your dashboard when the verification process is complete (usually within 1-2 days).'))))
                __M_writer(u'</p>\n')
            elif verification_status['status'] == VERIFY_STATUS_APPROVED:
                __M_writer(u'            <h4 class="message-title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('You have successfully verified your ID with edX'))))
                __M_writer(u'</h4>\n')
                if verification_status.get('verification_good_until') is not None:
                    __M_writer(u'              <p class="message-copy">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Your current verification is effective until {date}.').format(date=verification_status['verification_good_until']))))
                    __M_writer(u'\n')
            elif verification_status['status'] == VERIFY_STATUS_NEED_TO_REVERIFY:
                __M_writer(u'            <h4 class="message-title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your current verification will expire soon.'))))
                __M_writer(u'</h4>\n')
                __M_writer(u'            <p class="message-copy">')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_('Your current verification will expire in {days} days. {start_link}Re-verify your identity now{end_link} using a webcam and a government-issued photo ID.')).format(
                start_link=HTML('<a href="{href}">').format(href=reverse('verify_student_reverify')),
                end_link=HTML('</a>'),
                days=settings.VERIFY_STUDENT.get("EXPIRING_SOON_WINDOW")
              ))))
                __M_writer(u'\n            </p>\n')
            __M_writer(u'        </div>\n')
        __M_writer(u'\n')
        if course_mode_info['show_upsell'] and not is_course_blocked:
            __M_writer(u'          <div class="message message-upsell has-actions is-shown">\n\n            <div class="wrapper-extended">\n                <p class="message-copy" align="justify">\n                  <b class="message-copy-bold">\n                    ')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Pursue a {cert_name_long} to highlight the knowledge and skills you gain in this course.").format(cert_name_long=cert_name_long))))
            __M_writer(u'\n                  </b><br>\n                    ')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_("It's official. It's easily shareable. "
                        "It's a proven motivator to complete the course. {line_break}"
                        "{link_start}Learn more about the verified {cert_name_long}{link_end}.")).format(
                          line_break=HTML('<br>'),
                          link_start=HTML('<a href="{}" class="verified-info" data-course-key="{}">').format(
                            marketing_link('WHAT_IS_VERIFIED_CERT'),
                            enrollment.course_id
                          ),
                          link_end=HTML('</a>'),
                          cert_name_long=cert_name_long
                        ))))
            __M_writer(u'\n                </p>\n                <div class="action-upgrade-container">\n')
            if use_ecommerce_payment_flow and course_mode_info['verified_sku']:
                __M_writer(u'                    <a class="action action-upgrade" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(ecommerce_payment_page)))
                __M_writer(u'?sku=')
                __M_writer(filters.html_escape(filters.decode.utf8(course_mode_info['verified_sku'])))
                __M_writer(u'">\n')
            else:
                __M_writer(u'                    <a class="action action-upgrade" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(reverse('verify_student_upgrade_and_verify', kwargs={'course_id': unicode(course_overview.id)}))))
                __M_writer(u'" data-course-id="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_overview.id)))
                __M_writer(u'" data-user="')
                __M_writer(filters.html_escape(filters.decode.utf8(user.username)))
                __M_writer(u'">\n')
            __M_writer(u'                      <span class="action-upgrade-icon" aria-hidden="true"></span>\n                    <span class="wrapper-copy">\n                      <span class="copy" id="upgrade-to-verified">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Upgrade to Verified"))))
            __M_writer(u'</span>\n                        <span class="sr">&nbsp;')
            __M_writer(filters.html_escape(filters.decode.utf8(_(course_overview.display_name_with_default))))
            __M_writer(u'</span>\n                    </span>\n                  </a>\n                </div>\n            </div>\n          </div>\n')
        __M_writer(u'\n')
        if course_program_info and course_program_info.get('category'):
            for program_data in course_program_info.get('course_program_list', []):
                __M_writer(u'            ')
                runtime._include_file(context, u'_dashboard_program_info.html', _template_uri, program_data=program_data, enrollment_mode=enrollment.mode, category=course_program_info['category'])
                __M_writer(u'\n')
        __M_writer(u'\n')
        if is_course_blocked:
            __M_writer(u'          <p id="block-course-msg" class="course-block">\n            ')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_("You can no longer access this course because payment has not yet been received. "
                "You can {contact_link_start}contact the account holder{contact_link_end} "
                "to request payment, or you can "
                "{unenroll_link_start}unenroll{unenroll_link_end} "
                "from this course")).format(
              contact_link_start=HTML('<button type="button">'),
              contact_link_end=HTML('</button>'),
              unenroll_link_start=HTML(
                '<a id="unregister_block_course" rel="leanModal" '
                'data-course-id="{course_id}" data-course-number="{course_number}" data-course-name="{course_name}" '
                'href="#unenroll-modal">'
              ).format(
                course_id=course_overview.id,
                course_number=course_overview.number,
                course_name=course_overview.display_name_with_default,
              ),
              unenroll_link_end=HTML('</a>'),
            ))))
            __M_writer(u'\n          </p>\n')
        __M_writer(u'\n\n')
        if course_requirements:
            __M_writer(u'        ')
            prc_target = reverse('about_course', args=[unicode(course_requirements['courses'][0]['key'])]) 
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['prc_target'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n        <li class="prerequisites">\n          <p class="tip">\n            ')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_("You must successfully complete {link_start}{prc_display}{link_end} before you begin this course.")).format(
                link_start=HTML('<a href="{}">').format(prc_target),
                link_end=HTML('</a>'),
                prc_display=course_requirements['courses'][0]['display'],
              ))))
            __M_writer(u'\n          </p>\n        </li>\n')
        __M_writer(u'    </ul>\n  </footer>\n</article>\n</div>\n</li>\n<script>\n           $( document ).ready(function() {\n\n               if("')
        __M_writer(dump_js_escaped_json(is_course_blocked ))
        __M_writer(u'" == \'true\'){\n                   $( "#unregister_block_course" ).click(function() {\n                       $(\'.disable-look-unregister\').click();\n                   });\n               }\n           });\n</script>\n\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                __M_writer(u'\n    DateUtilFactory.transform(iterationKey=".localized-datetime");\n')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module_async(class_name=u'DateUtilFactory',module_name=u'js/dateutil_factory'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 3, "44": 36, "47": 1, "67": 1, "68": 22, "69": 24, "83": 34, "84": 36, "85": 39, "86": 40, "87": 40, "97": 46, "98": 47, "106": 51, "107": 52, "108": 53, "109": 53, "113": 53, "114": 55, "115": 56, "116": 56, "117": 57, "121": 57, "122": 58, "123": 58, "124": 59, "125": 59, "126": 59, "127": 59, "128": 61, "129": 62, "130": 63, "131": 63, "132": 63, "133": 63, "134": 63, "135": 64, "136": 64, "137": 64, "138": 64, "139": 66, "140": 67, "141": 68, "142": 68, "143": 68, "144": 68, "145": 71, "146": 72, "147": 73, "148": 73, "149": 73, "150": 73, "151": 76, "152": 77, "153": 77, "154": 77, "155": 78, "156": 78, "157": 79, "158": 80, "159": 80, "160": 80, "161": 80, "162": 80, "163": 82, "164": 82, "165": 82, "166": 85, "167": 88, "168": 89, "169": 90, "170": 90, "171": 90, "172": 90, "173": 90, "174": 90, "175": 90, "176": 91, "177": 92, "178": 92, "179": 92, "180": 92, "181": 92, "182": 94, "183": 95, "184": 95, "185": 95, "186": 97, "187": 99, "188": 99, "189": 100, "190": 100, "191": 101, "216": 122, "217": 124, "218": 125, "219": 125, "220": 125, "221": 126, "222": 127, "223": 127, "229": 129, "230": 130, "231": 130, "232": 130, "233": 130, "234": 130, "235": 130, "236": 130, "237": 130, "238": 130, "239": 130, "240": 132, "241": 135, "242": 136, "243": 137, "244": 138, "245": 138, "246": 138, "247": 138, "248": 138, "249": 138, "250": 138, "251": 138, "252": 138, "253": 139, "254": 140, "255": 140, "256": 140, "257": 140, "258": 140, "259": 140, "260": 140, "261": 142, "262": 143, "263": 144, "264": 144, "265": 144, "266": 144, "267": 144, "268": 144, "269": 144, "270": 144, "271": 144, "272": 145, "273": 146, "274": 146, "275": 146, "276": 146, "277": 146, "278": 146, "279": 146, "280": 149, "281": 150, "282": 151, "283": 151, "297": 161, "298": 162, "299": 163, "300": 163, "304": 163, "305": 165, "306": 165, "307": 169, "308": 169, "309": 171, "310": 171, "311": 172, "312": 172, "313": 172, "314": 172, "315": 172, "316": 172, "317": 173, "318": 173, "319": 177, "320": 178, "321": 178, "325": 178, "326": 179, "330": 179, "331": 180, "335": 180, "336": 182, "337": 182, "338": 186, "339": 186, "340": 188, "341": 188, "342": 189, "343": 189, "344": 189, "345": 189, "346": 189, "347": 189, "348": 190, "349": 190, "350": 196, "351": 196, "352": 196, "353": 197, "354": 197, "355": 197, "356": 197, "357": 197, "358": 197, "359": 197, "360": 197, "361": 197, "362": 197, "363": 198, "364": 198, "365": 200, "366": 200, "367": 204, "368": 204, "369": 205, "370": 205, "371": 205, "372": 205, "373": 206, "374": 207, "375": 207, "376": 207, "377": 208, "378": 209, "379": 210, "380": 210, "381": 210, "382": 210, "383": 210, "384": 210, "385": 210, "386": 210, "387": 210, "388": 211, "389": 211, "390": 212, "391": 212, "392": 213, "393": 213, "394": 215, "395": 216, "396": 216, "397": 216, "398": 216, "399": 216, "400": 216, "401": 216, "402": 216, "403": 216, "404": 217, "405": 217, "406": 218, "407": 218, "408": 219, "409": 219, "410": 222, "411": 223, "412": 224, "413": 224, "414": 224, "415": 224, "416": 224, "417": 224, "418": 224, "419": 224, "420": 224, "421": 225, "422": 225, "423": 226, "424": 226, "425": 227, "426": 227, "427": 229, "428": 230, "429": 230, "430": 230, "431": 230, "432": 230, "433": 230, "434": 230, "435": 230, "436": 230, "437": 231, "438": 231, "439": 232, "440": 232, "441": 233, "442": 233, "443": 236, "444": 237, "445": 238, "446": 238, "447": 238, "448": 238, "449": 238, "450": 238, "451": 238, "452": 238, "453": 238, "454": 239, "455": 239, "456": 240, "457": 240, "458": 242, "459": 243, "460": 243, "461": 243, "462": 243, "463": 243, "464": 243, "465": 243, "466": 243, "467": 243, "468": 244, "469": 244, "470": 245, "471": 245, "472": 248, "473": 249, "474": 250, "475": 250, "476": 250, "477": 250, "478": 250, "479": 250, "480": 250, "481": 250, "482": 250, "483": 250, "484": 250, "485": 251, "486": 251, "487": 252, "488": 252, "489": 253, "490": 253, "491": 255, "492": 256, "493": 256, "494": 256, "495": 256, "496": 256, "497": 256, "498": 256, "499": 256, "500": 256, "501": 256, "502": 256, "503": 257, "504": 257, "505": 258, "506": 258, "507": 259, "508": 259, "509": 262, "510": 263, "511": 264, "512": 264, "513": 264, "514": 264, "515": 264, "516": 264, "517": 264, "518": 264, "519": 264, "520": 264, "521": 264, "522": 265, "523": 265, "524": 266, "525": 266, "526": 267, "527": 267, "528": 269, "529": 270, "530": 270, "531": 270, "532": 270, "533": 270, "534": 270, "535": 270, "536": 270, "537": 270, "538": 270, "539": 270, "540": 271, "541": 271, "542": 272, "543": 272, "544": 273, "545": 273, "546": 277, "547": 279, "548": 279, "549": 279, "550": 280, "551": 281, "552": 282, "553": 282, "554": 282, "555": 282, "556": 282, "557": 282, "558": 282, "559": 282, "560": 282, "561": 282, "562": 282, "563": 283, "564": 284, "565": 284, "566": 284, "567": 284, "568": 284, "569": 284, "570": 284, "571": 284, "572": 284, "573": 284, "574": 284, "575": 287, "576": 297, "577": 298, "578": 299, "579": 299, "580": 301, "581": 302, "582": 303, "583": 303, "584": 304, "585": 304, "586": 304, "587": 304, "588": 307, "589": 310, "590": 311, "591": 312, "592": 312, "593": 312, "594": 314, "595": 315, "596": 316, "597": 316, "598": 316, "599": 318, "600": 319, "601": 320, "602": 321, "603": 322, "604": 323, "605": 324, "606": 324, "607": 324, "608": 325, "613": 329, "614": 330, "615": 331, "616": 331, "617": 331, "618": 332, "619": 332, "620": 334, "621": 336, "622": 336, "623": 336, "624": 336, "625": 336, "626": 336, "627": 338, "628": 339, "629": 339, "630": 339, "631": 340, "632": 340, "633": 341, "634": 342, "635": 342, "636": 342, "637": 343, "638": 343, "639": 344, "640": 345, "641": 345, "642": 345, "643": 346, "644": 347, "645": 347, "646": 347, "647": 349, "648": 350, "649": 350, "650": 350, "651": 353, "652": 353, "657": 357, "658": 360, "659": 362, "660": 363, "661": 364, "662": 369, "663": 369, "664": 371, "675": 381, "676": 384, "677": 385, "678": 385, "679": 385, "680": 385, "681": 385, "682": 386, "683": 387, "684": 387, "685": 387, "686": 387, "687": 387, "688": 387, "689": 387, "690": 389, "691": 391, "692": 391, "693": 392, "694": 392, "695": 399, "696": 400, "697": 401, "698": 402, "699": 402, "700": 402, "701": 405, "702": 406, "703": 407, "704": 408, "722": 425, "723": 428, "724": 430, "725": 432, "726": 432, "730": 432, "731": 435, "736": 439, "737": 443, "738": 451, "739": 451, "743": 459, "748": 459, "751": 461, "757": 751}, "uri": "dashboard/_dashboard_course_listing.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/dashboard/_dashboard_course_listing.html"}
__M_END_METADATA
"""
