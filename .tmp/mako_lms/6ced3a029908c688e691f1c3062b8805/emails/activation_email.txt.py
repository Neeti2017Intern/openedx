# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849744.523146
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/emails/activation_email.txt'
_template_uri = 'emails/activation_email.txt'
_source_encoding = 'utf-8'
_exports = []


from django.utils.translation import ugettext as _ 

from openedx.core.djangoapps.site_configuration import helpers as configuration_helpers 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        is_secure = context.get('is_secure', UNDEFINED)
        site = context.get('site', UNDEFINED)
        key = context.get('key', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(filters.decode.utf8(_("Thank you for creating an account with {platform_name}!").format(
    platform_name=configuration_helpers.get_value('PLATFORM_NAME', settings.PLATFORM_NAME)
)))
        __M_writer(u'\n\n')
        __M_writer(filters.decode.utf8(_("There's just one more step before you can enroll in a course: "
    "you need to activate your {platform_name} account. To activate "
    "your account, click the following link. If that doesn't work, "
    "copy and paste the link into your browser's address bar.").format(
        platform_name=configuration_helpers.get_value('PLATFORM_NAME', settings.PLATFORM_NAME)
    )))
        __M_writer(u'\n\n')
        if is_secure:
            __M_writer(u'  https://')
            __M_writer(filters.decode.utf8( site ))
            __M_writer(u'/activate/')
            __M_writer(filters.decode.utf8( key ))
            __M_writer(u'\n')
        else:
            __M_writer(u'  http://')
            __M_writer(filters.decode.utf8( site ))
            __M_writer(u'/activate/')
            __M_writer(filters.decode.utf8( key ))
            __M_writer(u'\n')
        __M_writer(filters.decode.utf8(_("If you didn't create an account, you don't need to do anything; you "
      "won't receive any more email from us. If you need assistance, please "
      "do not reply to this email message. Check the help section of the "
      "{platform_name} website.").format(platform_name=configuration_helpers.get_value('PLATFORM_NAME', settings.PLATFORM_NAME))))
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 1, "18": 2, "20": 0, "29": 1, "30": 2, "31": 3, "34": 5, "35": 7, "41": 12, "42": 14, "43": 15, "44": 15, "45": 15, "46": 15, "47": 15, "48": 16, "49": 17, "50": 17, "51": 17, "52": 17, "53": 17, "54": 19, "58": 22, "64": 58}, "uri": "emails/activation_email.txt", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/emails/activation_email.txt"}
__M_END_METADATA
"""
