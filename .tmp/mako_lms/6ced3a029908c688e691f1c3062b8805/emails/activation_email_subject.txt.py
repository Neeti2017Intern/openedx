# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849744.443714
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/emails/activation_email_subject.txt'
_template_uri = 'emails/activation_email_subject.txt'
_source_encoding = 'utf-8'
_exports = []


from django.utils.translation import ugettext as _ 

from openedx.core.djangoapps.site_configuration import helpers as configuration_helpers 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(filters.decode.utf8(_("Activate Your {platform_name} Account").format(
    platform_name=configuration_helpers.get_value('PLATFORM_NAME', settings.PLATFORM_NAME
))))
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"37": 31, "16": 1, "18": 2, "20": 0, "26": 1, "27": 2, "28": 4, "31": 6}, "uri": "emails/activation_email_subject.txt", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/emails/activation_email_subject.txt"}
__M_END_METADATA
"""
