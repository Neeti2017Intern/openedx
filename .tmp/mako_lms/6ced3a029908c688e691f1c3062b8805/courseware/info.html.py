# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946144.423095
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/info.html'
_template_uri = 'courseware/info.html'
_source_encoding = 'utf-8'
_exports = [u'pagetitle', u'headextra', u'js_extra', u'bodyclass', 'online_help_token']



from datetime import datetime
from pytz import timezone, utc

from django.utils.translation import ugettext as _

from courseware.courses import get_course_info_section, get_course_date_blocks
from openedx.core.djangoapps.self_paced.models import SelfPacedConfiguration
from openedx.core.djangolib.markup import HTML, Text


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'../main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        user_timezone = context.get('user_timezone', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        masquerade_user = context.get('masquerade_user', UNDEFINED)
        masquerade = context.get('masquerade', UNDEFINED)
        user_language = context.get('user_language', UNDEFINED)
        request = context.get('request', UNDEFINED)
        studio_url = context.get('studio_url', UNDEFINED)
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        course = context.get('course', UNDEFINED)
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        platform_key = context.get('platform_key', UNDEFINED)
        show_enroll_banner = context.get('show_enroll_banner', UNDEFINED)
        course_review_key = context.get('course_review_key', UNDEFINED)
        show_coursetalk_widget = context.get('show_coursetalk_widget', UNDEFINED)
        def headextra():
            return render_headextra(context._locals(__M_locals))
        url_to_enroll = context.get('url_to_enroll', UNDEFINED)
        last_accessed_courseware_url = context.get('last_accessed_courseware_url', UNDEFINED)
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'headextra'):
            context['self'].headextra(**pageargs)
        

        __M_writer(u'\n\n')
        if show_enroll_banner:
            __M_writer(u'<div class="wrapper-msg urgency-low" id="failed-verification-banner">\n  <div class="msg msg-reverify is-dismissable">\n    <div class="msg-content">\n      <h2 class="title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("You are not enrolled yet"))))
            __M_writer(u'</h2>\n      <div class="copy">\n        <p class=\'enroll-message\'>\n          ')
            __M_writer(filters.html_escape(filters.decode.utf8(Text(_("You are not currently enrolled in this course. {link_start}Sign up now!{link_end}")).format(
                link_start=HTML("<a href={}>").format(url_to_enroll),
                link_end=HTML("</a>")
          ))))
            __M_writer(u'\n        </p>\n      </div>\n    </div>\n  </div>\n</div>\n')
        __M_writer(u'\n')
        runtime._include_file(context, u'/courseware/course_navigation.html', _template_uri, active_page='info')
        __M_writer(u'\n\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                __M_writer(u'\n        ToggleElementVisibility();\n')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module_async(class_name=u'ToggleElementVisibility',module_name=u'js/courseware/toggle_element_visibility'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                __M_writer(u'\n        CourseHomeEvents();\n')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module_async(class_name=u'CourseHomeEvents',module_name=u'js/courseware/course_home_events'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u'\n\n<main id="main" aria-label="Content" tabindex="-1">\n    <div class="container">\n      <div class="home">\n        <div class="page-header-main">\n            <h3 class="page-title">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Welcome to {org}'s {course_name}!").format(org=course.display_org_with_default, course_name=course.display_number_with_default))))
        __M_writer(u'\n              <div class="page-subtitle">')
        __M_writer(filters.html_escape(filters.decode.utf8(course.display_name_with_default)))
        __M_writer(u'</div>\n            </h3>\n        </div>\n')
        if last_accessed_courseware_url:
            __M_writer(u'          <div class="page-header-secondary">\n              <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(last_accessed_courseware_url)))
            __M_writer(u'" class="last-accessed-link">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Resume Course"))))
            __M_writer(u'</a>\n          </div>\n')
        __M_writer(u'      </div>\n      <div class="info-wrapper">\n')
        if user.is_authenticated():
            __M_writer(u'        <section class="updates">\n')
            if studio_url is not None and masquerade and masquerade.role == 'staff':
                __M_writer(u'            <div class="wrap-instructor-info studio-view">\n              <a class="instructor-info-action" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(studio_url)))
                __M_writer(u'">\n                ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("View Updates in Studio"))))
                __M_writer(u'\n              </a>\n            </div>\n')
            __M_writer(u'\n          <h4>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course Updates and News"))))
            __M_writer(u'</h4>\n          ')
            __M_writer(filters.html_escape(filters.decode.utf8(HTML(get_course_info_section(request, masquerade_user, course, 'updates')))))
            __M_writer(u'\n\n')
            if show_coursetalk_widget:
                __M_writer(u'            <div class="coursetalk-write-reviews">\n              <div id="ct-custom-read-review-widget" data-provider="')
                __M_writer(filters.html_escape(filters.decode.utf8(platform_key)))
                __M_writer(u'" data-course="')
                __M_writer(filters.html_escape(filters.decode.utf8(course_review_key)))
                __M_writer(u'"></div>\n            </div>\n')
            __M_writer(u'        </section>\n\n        <section aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Handout Navigation'))))
            __M_writer(u'" class="handouts">\n\n')
            if SelfPacedConfiguration.current().enable_course_home_improvements:
                __M_writer(u'                <h4 class="handouts-header">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Important Course Dates"))))
                __M_writer(u'</h4>\n')
                __M_writer(u'\n')
                for course_date in get_course_date_blocks(course, user):
                    __M_writer(u'                    <div class="date-summary-container">\n                        <div class="date-summary date-summary-')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_date.css_class)))
                    __M_writer(u'">\n')
                    if course_date.title:
                        if course_date.title == 'current_datetime':
                            __M_writer(u'                                    <h3 class="heading localized-datetime" data-datetime="')
                            __M_writer(filters.html_escape(filters.decode.utf8(course_date.date)))
                            __M_writer(u'" data-string="')
                            __M_writer(filters.html_escape(filters.decode.utf8(_(u'Today is {date}'))))
                            __M_writer(u'" data-timezone="')
                            __M_writer(filters.html_escape(filters.decode.utf8(user_timezone)))
                            __M_writer(u'" data-language="')
                            __M_writer(filters.html_escape(filters.decode.utf8(user_language)))
                            __M_writer(u'"></h3>\n')
                        else:
                            __M_writer(u'                                    <h3 class="heading">')
                            __M_writer(filters.html_escape(filters.decode.utf8(course_date.title)))
                            __M_writer(u'</h3>\n')
                    if course_date.date and course_date.title != 'current_datetime':
                        __M_writer(u'                                <h4 class="date localized-datetime" data-format="shortDate" data-datetime="')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_date.date)))
                        __M_writer(u'" data-timezone="')
                        __M_writer(filters.html_escape(filters.decode.utf8(user_timezone)))
                        __M_writer(u'" data-language="')
                        __M_writer(filters.html_escape(filters.decode.utf8(user_language)))
                        __M_writer(u'" data-string="')
                        __M_writer(filters.html_escape(filters.decode.utf8(_(course_date.relative_datestring))))
                        __M_writer(u'"></h4>\n')
                    if course_date.description:
                        __M_writer(u'                              <p class="description">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_date.description)))
                        __M_writer(u'</p>\n')
                    if course_date.link and course_date.link_text:
                        __M_writer(u'                              <span class="date-summary-link">\n                                  <a href="')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_date.link)))
                        __M_writer(u'">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_date.link_text)))
                        __M_writer(u'</a>\n                              </span>\n')
                    __M_writer(u'                        </div>\n                    </div>\n')
            __M_writer(u'            <h4 class="handouts-header">')
            __M_writer(filters.html_escape(filters.decode.utf8(_(course.info_sidebar_name))))
            __M_writer(u'</h4>\n            ')
            __M_writer(filters.html_escape(filters.decode.utf8(HTML(get_course_info_section(request, masquerade_user, course, 'handouts')))))
            __M_writer(u'\n        </section>\n')
        else:
            __M_writer(u'        <section class="updates">\n          <h4 class="handouts-header">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course Updates and News"))))
            __M_writer(u'</h4>\n          ')
            __M_writer(filters.html_escape(filters.decode.utf8(HTML(get_course_info_section(request, masquerade_user, course, 'guest_updates')))))
            __M_writer(u'\n        </section>\n        <section aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Handout Navigation'))))
            __M_writer(u'" class="handouts">\n          <h4 class="handouts-header">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course Handouts"))))
            __M_writer(u'</h4>\n          ')
            __M_writer(filters.html_escape(filters.decode.utf8(HTML(get_course_info_section(request, masquerade_user, course, 'guest_handouts')))))
            __M_writer(u'\n        </section>\n')
        __M_writer(u'      </div>\n  </div>\n</main>\n\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                __M_writer(u'\n    DateUtilFactory.transform(iterationKey=".localized-datetime");\n')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module_async(class_name=u'DateUtilFactory',module_name=u'js/dateutil_factory'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def pagetitle():
            return render_pagetitle(context)
        course = context.get('course', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(filters.html_escape(filters.decode.utf8(_("{course_number} Course Info").format(course_number=course.display_number_with_default))))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_headextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def headextra():
            return render_headextra(context)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-course-vendor'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-course'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        show_coursetalk_widget = context.get('show_coursetalk_widget', UNDEFINED)
        def js_extra():
            return render_js_extra(context)
        __M_writer = context.writer()
        __M_writer(u'\n')
        if show_coursetalk_widget:
            __M_writer(u'      <script src="//d3q6qq2zt8nhwv.cloudfront.net/s/js/widgets/coursetalk-write-reviews.js"></script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        course = context.get('course', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        __M_writer(u'view-in-course view-course-info ')
        __M_writer(filters.html_escape(filters.decode.utf8(course.css_class or '')))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_online_help_token(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        return "courseinfo" 
        
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 5, "34": 4, "40": 1, "68": 1, "69": 2, "70": 3, "71": 4, "72": 14, "77": 16, "82": 21, "83": 23, "84": 24, "85": 27, "86": 27, "87": 30, "91": 33, "92": 40, "93": 41, "94": 41, "98": 43, "103": 43, "106": 45, "110": 46, "115": 46, "118": 48, "123": 55, "128": 57, "129": 63, "130": 63, "131": 64, "132": 64, "133": 67, "134": 68, "135": 69, "136": 69, "137": 69, "138": 69, "139": 72, "140": 74, "141": 75, "142": 76, "143": 77, "144": 78, "145": 78, "146": 79, "147": 79, "148": 83, "149": 84, "150": 84, "151": 85, "152": 85, "153": 88, "154": 89, "155": 90, "156": 90, "157": 90, "158": 90, "159": 93, "160": 95, "161": 95, "162": 97, "163": 98, "164": 98, "165": 98, "166": 100, "167": 101, "168": 102, "169": 103, "170": 103, "171": 104, "172": 105, "173": 106, "174": 106, "175": 106, "176": 106, "177": 106, "178": 106, "179": 106, "180": 106, "181": 106, "182": 107, "183": 108, "184": 108, "185": 108, "186": 111, "187": 112, "188": 112, "189": 112, "190": 112, "191": 112, "192": 112, "193": 112, "194": 112, "195": 112, "196": 114, "197": 115, "198": 115, "199": 115, "200": 117, "201": 118, "202": 119, "203": 119, "204": 119, "205": 119, "206": 122, "207": 126, "208": 126, "209": 126, "210": 127, "211": 127, "212": 129, "213": 130, "214": 131, "215": 131, "216": 132, "217": 132, "218": 134, "219": 134, "220": 135, "221": 135, "222": 136, "223": 136, "224": 139, "228": 143, "233": 143, "236": 145, "242": 16, "249": 16, "255": 18, "262": 18, "270": 19, "273": 19, "281": 20, "284": 20, "290": 50, "297": 50, "298": 52, "299": 53, "305": 57, "312": 57, "313": 57, "319": 3, "323": 3, "330": 323}, "uri": "courseware/info.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/info.html"}
__M_END_METADATA
"""
