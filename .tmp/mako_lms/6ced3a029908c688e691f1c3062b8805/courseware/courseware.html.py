# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946366.469041
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/courseware.html'
_template_uri = 'courseware/courseware.html'
_source_encoding = 'utf-8'
_exports = ['course_name', u'bodyclass', u'title', 'online_help_token', u'js_extra', u'headextra', u'header_extras']



from django.utils.translation import ugettext as _
from django.conf import settings

from edxnotes.helpers import is_feature_enabled as is_edxnotes_enabled
from openedx.core.djangolib.markup import HTML
from openedx.core.djangolib.js_utils import js_escaped_string


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'/static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def course_name():
            return render_course_name(context._locals(__M_locals))
        language_preference = context.get('language_preference', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        accordion = context.get('accordion', UNDEFINED)
        int = context.get('int', UNDEFINED)
        bookmarks_api_url = context.get('bookmarks_api_url', UNDEFINED)
        def title():
            return render_title(context._locals(__M_locals))
        entrance_exam_current_score = context.get('entrance_exam_current_score', UNDEFINED)
        course = context.get('course', UNDEFINED)
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        section_title = context.get('section_title', UNDEFINED)
        disable_accordion = context.get('disable_accordion', UNDEFINED)
        fragment = context.get('fragment', UNDEFINED)
        def headextra():
            return render_headextra(context._locals(__M_locals))
        def header_extras():
            return render_header_extras(context._locals(__M_locals))
        staff_access = context.get('staff_access', UNDEFINED)
        default_tab = context.get('default_tab', UNDEFINED)
        round = context.get('round', UNDEFINED)
        getattr = context.get('getattr', UNDEFINED)
        entrance_exam_passed = context.get('entrance_exam_passed', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')

        include_special_exams = settings.FEATURES.get('ENABLE_SPECIAL_EXAMS', False) and (course.enable_proctored_exams or course.enable_timed_exams)
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['include_special_exams'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header_extras'):
            context['self'].header_extras(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'headextra'):
            context['self'].headextra(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n<div class="message-banner" aria-live="polite"></div>\n\n')
        if default_tab:
            __M_writer(u'  ')
            runtime._include_file(context, u'/courseware/course_navigation.html', _template_uri)
            __M_writer(u'\n')
        else:
            __M_writer(u'  ')
            runtime._include_file(context, u'/courseware/course_navigation.html', _template_uri, active_page='courseware')
            __M_writer(u'\n')
        __M_writer(u'\n<div class="container">\n  <div class="course-wrapper" role="presentation">\n\n')
        if disable_accordion is UNDEFINED or not disable_accordion:
            __M_writer(u'    <div class="course-index">\n\n      <div class="wrapper-course-modes">\n\n          <div class="courseware-bookmarks-button" data-bookmarks-api-url="')
            __M_writer(filters.html_escape(filters.decode.utf8(bookmarks_api_url)))
            __M_writer(u'">\n              <button type="button" class="bookmarks-list-button is-inactive" aria-pressed="false">\n                  ')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Bookmarks'))))
            __M_writer(u'\n              </button>\n          </div>\n\n')
            if settings.FEATURES.get('ENABLE_COURSEWARE_SEARCH'):
                __M_writer(u'            <div id="courseware-search-bar" class="search-bar courseware-search-bar" role="search" aria-label="Course">\n              <form>\n                <label for="course-search-input" class="sr">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Course Search'))))
                __M_writer(u'</label>\n                <div class="search-field-wrapper">\n                  <input id="course-search-input" type="text" class="search-field"/>\n                  <button type="submit" class="search-button">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Search'))))
                __M_writer(u'</button>\n                  <button type="button" class="cancel-button" title="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Clear search'))))
                __M_writer(u'">\n                    <span class="icon fa fa-remove" aria-hidden="true"></span>\n                  </button>\n                </div>\n              </form>\n            </div>\n')
            __M_writer(u'\n      </div>\n\n      <div class="accordion">\n        <nav class="course-navigation" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Course'))))
            __M_writer(u'">\n')
            if accordion.strip():
                __M_writer(u'            ')
                __M_writer(filters.html_escape(filters.decode.utf8(HTML(accordion))))
                __M_writer(u'\n')
            else:
                __M_writer(u'            <div class="chapter">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("No content has been added to this course"))))
                __M_writer(u'</div>\n')
            __M_writer(u'        </nav>\n      </div>\n\n    </div>\n')
        __M_writer(u'    <section class="course-content" id="course-content">\n        <div class="path"></div>\n        <main id="main" aria-label="Content" tabindex="-1">\n')
        if getattr(course, 'entrance_exam_enabled') and \
           getattr(course, 'entrance_exam_minimum_score_pct') and \
           entrance_exam_current_score is not UNDEFINED:
            if not entrance_exam_passed:
                __M_writer(u'            <p class="sequential-status-message">\n                ')
                __M_writer(filters.html_escape(filters.decode.utf8(_('To access course materials, you must score {required_score}% or higher on this \
                exam. Your current score is {current_score}%.').format(
                    required_score=int(round(course.entrance_exam_minimum_score_pct * 100)),
                    current_score=int(round(entrance_exam_current_score * 100))
                ))))
                __M_writer(u'\n            </p>\n            <script type="text/javascript">\n            $(document).ajaxSuccess(function(event, xhr, settings) {\n                if (settings.url.indexOf("xmodule_handler/problem_check") > -1) {\n                    var data = JSON.parse(xhr.responseText);\n                    if (data.entrance_exam_passed){\n                        location.reload();\n                    }\n                }\n            });\n            </script>\n')
            else:
                __M_writer(u'              <p class="sequential-status-message">\n                ')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your score is {current_score}%. You have passed the entrance exam.').format(
                    current_score=int(round(entrance_exam_current_score * 100))
                ))))
                __M_writer(u'\n            </p>\n')
        __M_writer(u'\n          ')
        __M_writer(filters.html_escape(filters.decode.utf8(HTML(fragment.body_html()))))
        __M_writer(u'\n        </main>\n    </section>\n\n    <section class="courseware-results-wrapper">\n      <div id="loading-message" aria-live="polite" aria-relevant="all"></div>\n      <div id="error-message" aria-live="polite"></div>\n      <div class="courseware-results search-results" data-course-id="')
        __M_writer(filters.html_escape(filters.decode.utf8(course.id)))
        __M_writer(u'" data-lang-code="')
        __M_writer(filters.html_escape(filters.decode.utf8(language_preference)))
        __M_writer(u'"></div>\n    </section>\n\n  </div>\n</div>\n<div class="container-footer">\n')
        if settings.FEATURES.get("LICENSING", False):
            __M_writer(u'    <div class="course-license">\n')
            if getattr(course, "license", None):
                __M_writer(u'      ')
                runtime._include_file(context, u'../license.html', _template_uri, license=course.license)
                __M_writer(u'\n')
            else:
                __M_writer(u'      ')
                runtime._include_file(context, u'../license.html', _template_uri, license='all-rights-reserved')
                __M_writer(u'\n')
            __M_writer(u'    </div>\n')
        __M_writer(u'</div>\n\n<nav class="nav-utilities ')
        __M_writer(filters.html_escape(filters.decode.utf8("has-utility-calculator" if course.show_calculator else "")))
        __M_writer(u'" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Course Utilities'))))
        __M_writer(u'">\n')
        if is_edxnotes_enabled(course):
            __M_writer(u'    ')
            runtime._include_file(context, u'/edxnotes/toggle_notes.html', _template_uri, course=course)
            __M_writer(u'\n')
        __M_writer(u'\n')
        if course.show_calculator:
            __M_writer(u'    ')
            runtime._include_file(context, u'/calculator/toggle_calculator.html', _template_uri)
            __M_writer(u'\n')
        __M_writer(u'</nav>\n\n')
        runtime._include_file(context, u'../modal/accessible_confirm.html', _template_uri)
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_course_name(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        course = context.get('course', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n ')
        return _("{course_number} Courseware").format(course_number=course.display_number_with_default) 
        
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        course = context.get('course', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        __M_writer(u'view-in-course view-courseware courseware ')
        __M_writer(filters.html_escape(filters.decode.utf8(course.css_class or '')))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        static = _mako_get_namespace(context, 'static')
        section_title = context.get('section_title', UNDEFINED)
        def course_name():
            return render_course_name(context)
        def title():
            return render_title(context)
        __M_writer = context.writer()
        __M_writer(u'<title>\n')
        if section_title:
            __M_writer(filters.html_escape(filters.decode.utf8(static.get_page_title_breadcrumbs(section_title, course_name()))))
            __M_writer(u'\n')
        else:
            __M_writer(filters.html_escape(filters.decode.utf8(static.get_page_title_breadcrumbs(course_name()))))
            __M_writer(u'\n')
        __M_writer(u'</title>')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_online_help_token(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        return "courseware" 
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        fragment = context.get('fragment', UNDEFINED)
        course = context.get('course', UNDEFINED)
        def js_extra():
            return render_js_extra(context)
        static = _mako_get_namespace(context, 'static')
        staff_access = context.get('staff_access', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n  <script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('common/js/vendor/jquery.scrollTo.js'))))
        __M_writer(u'"></script>\n  <script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('js/vendor/flot/jquery.flot.js'))))
        __M_writer(u'"></script>\n\n')
        __M_writer(u'  <script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('js/vendor/codemirror-compressed.js'))))
        __M_writer(u'"></script>\n\n  ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.js(group=u'courseware'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n  ')
        runtime._include_file(context, u'/mathjax_include.html', _template_uri, disable_fast_preview=True)
        __M_writer(u'\n\n')
        if settings.FEATURES.get('ENABLE_COURSEWARE_SEARCH'):
            __M_writer(u'    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    __M_writer(u"\n        var courseId = $('.courseware-results').data('courseId');\n        CourseSearchFactory(courseId);\n    ")
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.require_module(class_name=u'CourseSearchFactory',module_name=u'js/search/course/course_search_factory'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
        __M_writer(u'\n  ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                __M_writer(u'\n    CoursewareFactory();\n  ')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module(class_name=u'CoursewareFactory',module_name=u'js/courseware/courseware_factory'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n')
        if staff_access:
            __M_writer(u'  \t')
            runtime._include_file(context, u'xqa_interface.html', _template_uri)
            __M_writer(u'\n')
        __M_writer(u'\n  <script type="text/javascript">\n    var $$course_id = "')
        __M_writer(js_escaped_string(course.id ))
        __M_writer(u'";\n  </script>\n\n')
        __M_writer(filters.html_escape(filters.decode.utf8(HTML(fragment.foot_html()))))
        __M_writer(u'\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_headextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        fragment = context.get('fragment', UNDEFINED)
        def headextra():
            return render_headextra(context)
        static = _mako_get_namespace(context, 'static')
        course = context.get('course', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-course-vendor'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-course'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        if is_edxnotes_enabled(course):
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-student-notes'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
        __M_writer(u'\n<script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('js/jquery.autocomplete.js'))))
        __M_writer(u'"></script>\n<script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('js/src/tooltip_manager.js'))))
        __M_writer(u'"></script>\n\n<link href="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('css/vendor/jquery.autocomplete.css'))))
        __M_writer(u'" rel="stylesheet" type="text/css">\n  ')
        __M_writer(filters.html_escape(filters.decode.utf8(HTML(fragment.head_html()))))
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header_extras(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        include_special_exams = context.get('include_special_exams', UNDEFINED)
        def header_extras():
            return render_header_extras(context)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n\n')
        for template_name in ["image-modal"]:
            __M_writer(u'<script type="text/template" id="')
            __M_writer(filters.html_escape(filters.decode.utf8(template_name)))
            __M_writer(u'-tpl">\n    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.include(path=u'common/templates/' + (template_name) + u'.underscore'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n</script>\n')
        __M_writer(u'\n')
        if settings.FEATURES.get('ENABLE_COURSEWARE_SEARCH'):
            for template_name in ["course_search_item", "course_search_results", "search_loading", "search_error"]:
                __M_writer(u'        <script type="text/template" id="')
                __M_writer(filters.html_escape(filters.decode.utf8(template_name)))
                __M_writer(u'-tpl">\n            ')
                def ccall(caller):
                    def body():
                        __M_writer = context.writer()
                        return ''
                    return [body]
                context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
                try:
                    __M_writer(filters.html_escape(filters.decode.utf8(static.include(path=u'search/' + (template_name) + u'.underscore'))))
                finally:
                    context.caller_stack.nextcaller = None
                __M_writer(u'\n        </script>\n')
        __M_writer(u'\n')
        if include_special_exams:
            for template_name in ["proctored-exam-status"]:
                __M_writer(u'    <script type="text/template" id="')
                __M_writer(filters.html_escape(filters.decode.utf8(template_name)))
                __M_writer(u'-tpl">\n        ')
                def ccall(caller):
                    def body():
                        __M_writer = context.writer()
                        return ''
                    return [body]
                context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
                try:
                    __M_writer(filters.html_escape(filters.decode.utf8(static.include(path=u'courseware/' + (template_name) + u'.underscore'))))
                finally:
                    context.caller_stack.nextcaller = None
                __M_writer(u'\n    </script>\n')
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 5, "32": 3, "38": 1, "70": 1, "71": 2, "72": 3, "73": 4, "74": 12, "75": 13, "81": 15, "82": 18, "87": 20, "92": 28, "97": 54, "102": 69, "107": 102, "108": 106, "109": 107, "110": 107, "111": 107, "112": 108, "113": 109, "114": 109, "115": 109, "116": 111, "117": 115, "118": 116, "119": 120, "120": 120, "121": 122, "122": 122, "123": 126, "124": 127, "125": 129, "126": 129, "127": 132, "128": 132, "129": 133, "130": 133, "131": 140, "132": 144, "133": 144, "134": 145, "135": 146, "136": 146, "137": 146, "138": 147, "139": 148, "140": 148, "141": 148, "142": 150, "143": 155, "144": 158, "147": 161, "148": 162, "149": 163, "154": 167, "155": 179, "156": 180, "157": 181, "160": 183, "161": 187, "162": 188, "163": 188, "164": 195, "165": 195, "166": 195, "167": 195, "168": 201, "169": 202, "170": 203, "171": 204, "172": 204, "173": 204, "174": 205, "175": 207, "176": 207, "177": 207, "178": 209, "179": 211, "180": 213, "181": 213, "182": 213, "183": 213, "184": 215, "185": 216, "186": 216, "187": 216, "188": 218, "189": 220, "190": 221, "191": 221, "192": 221, "193": 223, "194": 225, "195": 225, "201": 16, "206": 16, "207": 17, "209": 17, "215": 20, "222": 20, "223": 20, "229": 22, "239": 22, "240": 23, "241": 24, "242": 24, "243": 25, "244": 26, "245": 26, "246": 28, "252": 4, "256": 4, "263": 71, "273": 71, "274": 72, "275": 72, "276": 73, "277": 73, "278": 76, "279": 76, "280": 76, "288": 78, "291": 78, "292": 79, "293": 79, "294": 81, "295": 82, "299": 82, "304": 82, "307": 85, "308": 87, "312": 88, "317": 88, "320": 90, "321": 92, "322": 93, "323": 93, "324": 93, "325": 95, "326": 97, "327": 97, "328": 100, "329": 100, "335": 56, "344": 56, "352": 57, "355": 57, "363": 58, "366": 58, "367": 60, "375": 61, "378": 61, "379": 63, "380": 64, "381": 64, "382": 65, "383": 65, "384": 67, "385": 67, "386": 68, "387": 68, "393": 30, "401": 30, "402": 32, "403": 33, "404": 33, "405": 33, "413": 34, "416": 34, "417": 37, "418": 38, "419": 39, "420": 40, "421": 40, "422": 40, "430": 41, "433": 41, "434": 45, "435": 46, "436": 47, "437": 48, "438": 48, "439": 48, "447": 49, "450": 49, "451": 53, "457": 451}, "uri": "courseware/courseware.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/courseware.html"}
__M_END_METADATA
"""
