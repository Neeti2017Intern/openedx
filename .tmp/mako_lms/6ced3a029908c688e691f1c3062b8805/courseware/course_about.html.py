# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849075.471665
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/course_about.html'
_template_uri = 'courseware/course_about.html'
_source_encoding = 'utf-8'
_exports = [u'pagetitle', u'headextra', u'js_extra']



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from courseware.courses import get_course_about_section
from django.conf import settings
from edxmako.shortcuts import marketing_link
from openedx.core.lib.courses import course_image_url


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'../main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        is_shib_course = context.get('is_shib_course', UNDEFINED)
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        course = context.get('course', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        unicode = context.get('unicode', UNDEFINED)
        course_target = context.get('course_target', UNDEFINED)
        isinstance = context.get('isinstance', UNDEFINED)
        professional_mode = context.get('professional_mode', UNDEFINED)
        reg_then_add_to_cart_link = context.get('reg_then_add_to_cart_link', UNDEFINED)
        can_enroll = context.get('can_enroll', UNDEFINED)
        studio_url = context.get('studio_url', UNDEFINED)
        course_image_urls = context.get('course_image_urls', UNDEFINED)
        invitation_only = context.get('invitation_only', UNDEFINED)
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        platform_key = context.get('platform_key', UNDEFINED)
        course_price = context.get('course_price', UNDEFINED)
        pre_requisite_courses = context.get('pre_requisite_courses', UNDEFINED)
        registered = context.get('registered', UNDEFINED)
        active_reg_button = context.get('active_reg_button', UNDEFINED)
        show_courseware_link = context.get('show_courseware_link', UNDEFINED)
        is_course_full = context.get('is_course_full', UNDEFINED)
        ecommerce_checkout_link = context.get('ecommerce_checkout_link', UNDEFINED)
        user = context.get('user', UNDEFINED)
        is_cosmetic_price_enabled = context.get('is_cosmetic_price_enabled', UNDEFINED)
        show_coursetalk_widget = context.get('show_coursetalk_widget', UNDEFINED)
        ecommerce_checkout = context.get('ecommerce_checkout', UNDEFINED)
        staff_access = context.get('staff_access', UNDEFINED)
        cart_link = context.get('cart_link', UNDEFINED)
        request = context.get('request', UNDEFINED)
        in_cart = context.get('in_cart', UNDEFINED)
        can_add_course_to_cart = context.get('can_add_course_to_cart', UNDEFINED)
        str = context.get('str', UNDEFINED)
        course_review_key = context.get('course_review_key', UNDEFINED)
        def headextra():
            return render_headextra(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'headextra'):
            context['self'].headextra(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n\n<section class="course-info">\n  <header class="course-profile">\n    <div class="intro-inner-wrapper">\n      <div class="table">\n      <section class="intro">\n        <div class="heading-group">\n          <h1>\n            ')
        __M_writer(filters.decode.utf8(course.display_name_with_default_escaped))
        __M_writer(u'\n            <button type="button">')
        __M_writer(filters.html_escape(filters.decode.utf8(course.display_org_with_default )))
        __M_writer(u'</button>\n          </h1>\n        </div>\n\n        <div class="main-cta">\n')
        if user.is_authenticated() and registered:
            if show_courseware_link:
                __M_writer(u'            <a href="')
                __M_writer(filters.decode.utf8(course_target))
                __M_writer(u'">\n')
            __M_writer(u'\n          <span class="register disabled">')
            __M_writer(filters.decode.utf8(_("You are enrolled in this course")))
            __M_writer(u'</span>\n\n')
            if show_courseware_link:
                __M_writer(u'            <strong>')
                __M_writer(filters.decode.utf8(_("View Course")))
                __M_writer(u'</strong>\n            </a>\n')
            __M_writer(u'\n')
        elif in_cart:
            __M_writer(u'          <span class="add-to-cart">\n            ')
            __M_writer(filters.decode.utf8(_('This course is in your <a href="{cart_link}">cart</a>.').format(cart_link=cart_link)))
            __M_writer(u'\n          </span>\n')
        elif is_course_full:
            __M_writer(u'          <span class="register disabled">\n            ')
            __M_writer(filters.decode.utf8(_("Course is full")))
            __M_writer(u'\n          </span>\n')
        elif invitation_only and not can_enroll:
            __M_writer(u'          <span class="register disabled">')
            __M_writer(filters.decode.utf8(_("Enrollment in this course is by invitation only")))
            __M_writer(u'</span>\n')
        elif not is_shib_course and not can_enroll:
            __M_writer(u'          <span class="register disabled">')
            __M_writer(filters.decode.utf8(_("Enrollment is Closed")))
            __M_writer(u'</span>\n')
        elif can_add_course_to_cart:
            __M_writer(u'          ')

            if user.is_authenticated():
              reg_href = "#"
              reg_element_id = "add_to_cart_post"
            else:
              reg_href = reg_then_add_to_cart_link
              reg_element_id = "reg_then_add_to_cart"
            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['reg_element_id','reg_href'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n          ')
            if ecommerce_checkout:
                         reg_href = ecommerce_checkout_link
                         reg_element_id = ""
                     
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['reg_element_id','reg_href'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n          <a href="')
            __M_writer(filters.decode.utf8(reg_href))
            __M_writer(u'" class="add-to-cart" id="')
            __M_writer(filters.decode.utf8(reg_element_id))
            __M_writer(u'">\n            ')
            __M_writer(filters.decode.utf8(_("Add {course_name} to Cart <span>({price} USD)</span>")\
              .format(course_name=course.display_number_with_default, price=course_price)))
            __M_writer(u'\n          </a>\n          <div id="register_error"></div>\n')
        else:
            __M_writer(u'          ')
 
            if ecommerce_checkout:
              reg_href = ecommerce_checkout_link
            else:
              reg_href="#"
            if professional_mode:
              href_class = "add-to-cart"
            else:
              href_class = "register"
                      
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['href_class','reg_href'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n          <a href="')
            __M_writer(filters.decode.utf8(reg_href))
            __M_writer(u'" class="')
            __M_writer(filters.decode.utf8(href_class))
            __M_writer(u'">\n            ')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Enroll in {course_name}").format(course_name=course.display_number_with_default) )))
            __M_writer(u'\n          </a>\n          <div id="register_error"></div>\n')
        __M_writer(u'        </div>\n\n      </section>\n')
        if get_course_about_section(request, course, "video"):
            __M_writer(u'      <a href="#video-modal" class="media" rel="leanModal">\n        <div class="hero">\n          <img src="')
            __M_writer(filters.decode.utf8(course_image_urls['large']))
            __M_writer(u'" alt="" />\n          <div class="play-intro"></div>\n        </div>\n      </a>\n')
        else:
            __M_writer(u'      <div class="media">\n        <div class="hero">\n          <img src="')
            __M_writer(filters.decode.utf8(course_image_urls['large']))
            __M_writer(u'" alt="" />\n        </div>\n      </div>\n')
        __M_writer(u'    </div>\n      </div>\n  </header>\n\n  <div class="container">\n    <div class="details">\n')
        if staff_access and studio_url is not None:
            __M_writer(u'        <div class="wrap-instructor-info studio-view">\n          <a class="instructor-info-action" href="')
            __M_writer(filters.decode.utf8(studio_url))
            __M_writer(u'">')
            __M_writer(filters.decode.utf8(_("View About Page in studio")))
            __M_writer(u'</a>\n        </div>\n')
        __M_writer(u'\n      <div class="inner-wrapper">\n        ')
        __M_writer(filters.decode.utf8(get_course_about_section(request, course, "overview")))
        __M_writer(u'\n      </div>\n  </div>\n\n    <div class="course-sidebar">\n      <div class="course-summary">\n\n        ')
        runtime._include_file(context, u'course_about_sidebar_header.html', _template_uri)
        __M_writer(u'\n\n        <ol class="important-dates">\n          <li class="important-dates-item"><span class="icon fa fa-info-circle" aria-hidden="true"></span><p class="important-dates-item-title">')
        __M_writer(filters.decode.utf8(_("Course Number")))
        __M_writer(u'</p><span class="important-dates-item-text course-number">')
        __M_writer(filters.html_escape(filters.decode.utf8(course.display_number_with_default )))
        __M_writer(u'</span></li>\n')
        if not course.start_date_is_still_default:
            __M_writer(u'              ')

            course_start_date = course.start
                          
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_start_date'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n            <li class="important-dates-item">\n              <span class="icon fa fa-calendar" aria-hidden="true"></span>\n              <p class="important-dates-item-title">')
            __M_writer(filters.decode.utf8(_("Classes Start")))
            __M_writer(u'</p>\n')
            if isinstance(course_start_date, str):
                __M_writer(u'                  <span class="important-dates-item-text start-date">')
                __M_writer(filters.decode.utf8(course_start_date))
                __M_writer(u'</span>\n')
            else:
                __M_writer(u'                  ')

                course_date_string = course_start_date.strftime('%Y-%m-%dT%H:%M:%S%z')
                                  
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_date_string'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n                  <span class="important-dates-item-text start-date localized_datetime" data-format="shortDate" data-datetime="')
                __M_writer(filters.decode.utf8(course_date_string))
                __M_writer(u'"></span>\n')
            __M_writer(u'            </li>\n')
        if get_course_about_section(request, course, "end_date") or course.end:
            __M_writer(u'                ')

            course_end_date = course.end
                            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_end_date'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n\n            <li class="important-dates-item">\n                <span class="icon fa fa-calendar" aria-hidden="true"></span>\n                <p class="important-dates-item-title">')
            __M_writer(filters.decode.utf8(_("Classes End")))
            __M_writer(u'</p>\n')
            if isinstance(course_end_date, str):
                __M_writer(u'                      <span class="important-dates-item-text final-date">')
                __M_writer(filters.decode.utf8(course_end_date))
                __M_writer(u'</span>\n')
            else:
                __M_writer(u'                    ')

                course_date_string = course_end_date.strftime('%Y-%m-%dT%H:%M:%S%z')
                                    
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['course_date_string'] if __M_key in __M_locals_builtin_stored]))
                __M_writer(u'\n                    <span class="important-dates-item-text final-date localized_datetime" data-format="shortDate" data-datetime="')
                __M_writer(filters.decode.utf8(course_date_string))
                __M_writer(u'"></span>\n')
            __M_writer(u'            </li>\n')
        __M_writer(u'\n')
        if get_course_about_section(request, course, "effort"):
            __M_writer(u'            <li class="important-dates-item"><span class="icon fa fa-pencil" aria-hidden="true"></span><p class="important-dates-item-title">')
            __M_writer(filters.decode.utf8(_("Estimated Effort")))
            __M_writer(u'</p><span class="important-dates-item-text effort">')
            __M_writer(filters.decode.utf8(get_course_about_section(request, course, "effort")))
            __M_writer(u'</span></li>\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        if course_price and (can_add_course_to_cart or is_cosmetic_price_enabled):
            __M_writer(u'            <li class="important-dates-item">\n              <span class="icon fa fa-money" aria-hidden="true"></span>\n              <p class="important-dates-item-title">')
            __M_writer(filters.decode.utf8(_("Price")))
            __M_writer(u'</p>\n              <span class="important-dates-item-text">')
            __M_writer(filters.decode.utf8(course_price))
            __M_writer(u'</span>\n            </li>\n')
        __M_writer(u'\n')
        if pre_requisite_courses:
            __M_writer(u'          ')
            prc_target = reverse('about_course', args=[unicode(pre_requisite_courses[0]['key'])]) 
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['prc_target'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n          <li class="prerequisite-course important-dates-item">\n            <span class="icon fa fa-list-ul" aria-hidden="true"></span>\n            <p class="important-dates-item-title">')
            __M_writer(filters.decode.utf8(_("Prerequisites")))
            __M_writer(u'</p>\n')
            __M_writer(u'            <span class="important-dates-item-text pre-requisite"><a href="')
            __M_writer(filters.decode.utf8(prc_target))
            __M_writer(u'">')
            __M_writer(filters.decode.utf8(pre_requisite_courses[0]['display']))
            __M_writer(u'</a></span>\n            <p class="tip">\n            ')
            __M_writer(filters.decode.utf8(_("You must successfully complete {link_start}{prc_display}{link_end} before you begin this course.").format(
              link_start='<a href="{}">'.format(prc_target),
              link_end='</a>',
              prc_display=pre_requisite_courses[0]['display'],
            )))
            __M_writer(u'\n            </p>\n          </li>\n')
        if get_course_about_section(request, course, "prerequisites"):
            __M_writer(u'            <li class="important-dates-item"><span class="icon fa fa-book" aria-hidden="true"></span><p class="important-dates-item-title">')
            __M_writer(filters.decode.utf8(_("Requirements")))
            __M_writer(u'</p><span class="important-dates-item-text prerequisites">')
            __M_writer(filters.decode.utf8(get_course_about_section(request, course, "prerequisites")))
            __M_writer(u'</span></li>\n')
        __M_writer(u'        </ol>\n    </div>\n\n')
        if show_coursetalk_widget:
            __M_writer(u'      <div class="coursetalk-read-reviews">\n          <div id="ct-custom-read-review-widget" data-provider="')
            __M_writer(filters.decode.utf8(platform_key))
            __M_writer(u'" data-course="')
            __M_writer(filters.decode.utf8(course_review_key))
            __M_writer(u'"></div>\n      </div>\n')
        __M_writer(u'\n')
        if get_course_about_section(request, course, "ocw_links"):
            __M_writer(u'      <div class="additional-resources">\n        <header>\n          <h1>')
            __M_writer(filters.decode.utf8(_("Additional Resources")))
            __M_writer(u'</h1>\n      </div>\n\n        <div>\n')
            __M_writer(u'          <h2 class="opencourseware">MITOpenCourseware</h2>\n             ')
            __M_writer(filters.decode.utf8(get_course_about_section(request, course, "ocw_links")))
            __M_writer(u'\n        </div>\n    </div>\n')
        __M_writer(u'\n  </div>\n\n  </div>\n</div>\n\n')
        if active_reg_button or is_shib_course:
            __M_writer(u'  <div style="display: none;">\n    <form id="class_enroll_form" method="post" data-remote="true" action="')
            __M_writer(filters.decode.utf8(reverse('change_enrollment')))
            __M_writer(u'">\n      <fieldset class="enroll_fieldset">\n        <legend class="sr">')
            __M_writer(filters.decode.utf8(_("Enroll")))
            __M_writer(u'</legend>\n        <input name="course_id" type="hidden" value="')
            __M_writer(filters.html_escape(filters.decode.utf8(course.id )))
            __M_writer(u'">\n        <input name="enrollment_action" type="hidden" value="enroll">\n      </fieldset>\n      <div class="submit">\n        <input name="submit" type="submit" value="')
            __M_writer(filters.decode.utf8(_('enroll')))
            __M_writer(u'">\n      </div>\n    </form>\n  </div>\n')
        __M_writer(u'\n')
        runtime._include_file(context, u'../video_modal.html', _template_uri)
        __M_writer(u'\n\n')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                __M_writer(u'\n    DateUtilFactory.transform(iterationKey=".localized_datetime");\n')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.decode.utf8(static.require_module_async(class_name=u'DateUtilFactory',module_name=u'js/dateutil_factory')))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def pagetitle():
            return render_pagetitle(context)
        course = context.get('course', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(filters.decode.utf8(course.display_name_with_default_escaped))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_headextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def headextra():
            return render_headextra(context)
        request = context.get('request', UNDEFINED)
        course = context.get('course', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'  <meta property="og:title" content="')
        __M_writer(filters.decode.utf8(course.display_name_with_default_escaped))
        __M_writer(u'" />\n  <meta property="og:description" content="')
        __M_writer(filters.decode.utf8(get_course_about_section(request, course, 'short_description')))
        __M_writer(u'" />\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        cart_link = context.get('cart_link', UNDEFINED)
        reg_then_add_to_cart_link = context.get('reg_then_add_to_cart_link', UNDEFINED)
        course = context.get('course', UNDEFINED)
        def js_extra():
            return render_js_extra(context)
        static = _mako_get_namespace(context, 'static')
        show_coursetalk_widget = context.get('show_coursetalk_widget', UNDEFINED)
        can_add_course_to_cart = context.get('can_add_course_to_cart', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        if show_coursetalk_widget:
            __M_writer(u'      <script src="//d3q6qq2zt8nhwv.cloudfront.net/s/js/widgets/coursetalk-read-reviews.js"></script>\n')
        __M_writer(u'  <script type="text/javascript">\n  (function() {\n    $(".register").click(function(event) {\n      $("#class_enroll_form").submit();\n      event.preventDefault();\n    });\n\n')
        if can_add_course_to_cart:
            __M_writer(u'      add_course_complete_handler = function(jqXHR, textStatus) {\n        if (jqXHR.status == 200) {\n          location.href = "')
            __M_writer(filters.decode.utf8(cart_link))
            __M_writer(u'";\n        }\n        if (jqXHR.status == 400) {\n          $("#register_error")\n            .html(jqXHR.responseText ? jqXHR.responseText : "')
            __M_writer(filters.decode.utf8(_("An error occurred. Please try again later.")))
            __M_writer(u'")\n            .css("display", "block");\n        }\n        else if (jqXHR.status == 403) {\n            location.href = "')
            __M_writer(filters.decode.utf8(reg_then_add_to_cart_link))
            __M_writer(u'";\n        }\n      };\n\n      $("#add_to_cart_post").click(function(event){\n        $.ajax({\n          url: "')
            __M_writer(filters.decode.utf8(reverse('add_course_to_cart', args=[course.id.to_deprecated_string()])))
            __M_writer(u'",\n          type: "POST",\n          /* Rant: HAD TO USE COMPLETE B/C PROMISE.DONE FOR SOME REASON DOES NOT WORK ON THIS PAGE. */\n          complete: add_course_complete_handler\n        })\n        event.preventDefault();\n      });\n')
        __M_writer(u'\n')
        if settings.FEATURES.get('RESTRICT_ENROLL_BY_REG_METHOD') and course.enrollment_domain:
            __M_writer(u'      ')

            perms_error = _('The currently logged-in user account does not have permission to enroll in this course. '
                            'You may need to {start_logout_tag}log out{end_tag} then try the enroll button again. '
                            'Please visit the {start_help_tag}help page{end_tag} for a possible solution.').format(
                              start_help_tag="<a href='{url}'>".format(url=marketing_link('FAQ')), end_tag='</a>',
                              start_logout_tag="<a href='{url}'>".format(url=reverse('logout'))
                              )
                  
            
            __M_writer(u'\n    $(\'#class_enroll_form\').on(\'ajax:complete\', function(event, xhr) {\n      if(xhr.status == 200) {\n        location.href = "')
            __M_writer(filters.decode.utf8(reverse('dashboard')))
            __M_writer(u'";\n      } else if (xhr.status == 403) {\n        location.href = "')
            __M_writer(filters.decode.utf8(reverse('course-specific-register', args=[course.id.to_deprecated_string()])))
            __M_writer(u'?course_id=')
            __M_writer(filters.url_escape(filters.decode.utf8(course.id )))
            __M_writer(u'&enrollment_action=enroll";\n      } else if (xhr.status == 400) { //This means the user did not have permission\n        $(\'#register_error\').html("')
            __M_writer(filters.decode.utf8(perms_error))
            __M_writer(u'").css("display", "block");\n      } else {\n        $(\'#register_error\').html(\n            (xhr.responseText ? xhr.responseText : "')
            __M_writer(filters.decode.utf8(_("An error occurred. Please try again later.")))
            __M_writer(u'")\n        ).css("display", "block");\n      }\n    });\n\n')
        else:
            __M_writer(u'\n    $(\'#class_enroll_form\').on(\'ajax:complete\', function(event, xhr) {\n      if(xhr.status == 200) {\n        if (xhr.responseText == "") {\n          location.href = "')
            __M_writer(filters.decode.utf8(reverse('dashboard')))
            __M_writer(u'";\n        }\n        else {\n          location.href = xhr.responseText;\n        }\n      } else if (xhr.status == 403) {\n          location.href = "')
            __M_writer(filters.decode.utf8(reverse('register_user')))
            __M_writer(u'?course_id=')
            __M_writer(filters.url_escape(filters.decode.utf8(course.id )))
            __M_writer(u'&enrollment_action=enroll";\n      } else {\n        $(\'#register_error\').html(\n            (xhr.responseText ? xhr.responseText : "')
            __M_writer(filters.decode.utf8(_("An error occurred. Please try again later.")))
            __M_writer(u'")\n        ).css("display", "block");\n      }\n    });\n\n')
        __M_writer(u'\n  })(this)\n  </script>\n\n  <script src="')
        __M_writer(filters.decode.utf8(static.url('js/course_info.js')))
        __M_writer(u'"></script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "32": 1, "38": 0, "80": 1, "81": 9, "82": 11, "87": 17, "92": 106, "97": 108, "98": 117, "99": 117, "100": 118, "101": 118, "102": 123, "103": 124, "104": 125, "105": 125, "106": 125, "107": 127, "108": 128, "109": 128, "110": 130, "111": 131, "112": 131, "113": 131, "114": 134, "115": 135, "116": 136, "117": 137, "118": 137, "119": 139, "120": 140, "121": 141, "122": 141, "123": 143, "124": 144, "125": 144, "126": 144, "127": 148, "128": 149, "129": 149, "130": 149, "131": 150, "132": 151, "133": 151, "144": 158, "145": 159, "152": 162, "153": 163, "154": 163, "155": 163, "156": 163, "157": 164, "159": 165, "160": 168, "161": 169, "162": 169, "175": 178, "176": 179, "177": 179, "178": 179, "179": 179, "180": 180, "181": 180, "182": 184, "183": 187, "184": 188, "185": 190, "186": 190, "187": 194, "188": 195, "189": 197, "190": 197, "191": 201, "192": 207, "193": 208, "194": 209, "195": 209, "196": 209, "197": 209, "198": 212, "199": 214, "200": 214, "201": 221, "202": 221, "203": 224, "204": 224, "205": 224, "206": 224, "207": 225, "208": 226, "209": 226, "215": 228, "216": 231, "217": 231, "218": 232, "219": 233, "220": 233, "221": 233, "222": 234, "223": 235, "224": 235, "230": 237, "231": 238, "232": 238, "233": 240, "234": 244, "235": 245, "236": 245, "242": 247, "243": 251, "244": 251, "245": 252, "246": 253, "247": 253, "248": 253, "249": 254, "250": 255, "251": 255, "257": 257, "258": 258, "259": 258, "260": 260, "261": 262, "262": 263, "263": 264, "264": 264, "265": 264, "266": 264, "267": 264, "268": 266, "269": 268, "270": 269, "271": 270, "272": 272, "273": 272, "274": 273, "275": 273, "276": 276, "277": 277, "278": 278, "279": 278, "283": 278, "284": 281, "285": 281, "286": 283, "287": 283, "288": 283, "289": 283, "290": 283, "291": 285, "296": 289, "297": 293, "298": 294, "299": 294, "300": 294, "301": 294, "302": 294, "303": 296, "304": 300, "305": 301, "306": 302, "307": 302, "308": 302, "309": 302, "310": 305, "311": 307, "312": 308, "313": 310, "314": 310, "315": 315, "316": 316, "317": 316, "318": 320, "319": 329, "320": 330, "321": 331, "322": 331, "323": 333, "324": 333, "325": 334, "326": 334, "327": 338, "328": 338, "329": 343, "330": 344, "331": 344, "335": 346, "340": 346, "343": 348, "349": 108, "356": 108, "362": 12, "370": 12, "371": 15, "372": 15, "373": 15, "374": 16, "375": 16, "381": 19, "393": 19, "394": 21, "395": 22, "396": 24, "397": 31, "398": 32, "399": 34, "400": 34, "401": 38, "402": 38, "403": 42, "404": 42, "405": 48, "406": 48, "407": 56, "408": 58, "409": 59, "410": 59, "419": 66, "420": 69, "421": 69, "422": 71, "423": 71, "424": 71, "425": 71, "426": 73, "427": 73, "428": 76, "429": 76, "430": 81, "431": 82, "432": 86, "433": 86, "434": 92, "435": 92, "436": 92, "437": 92, "438": 95, "439": 95, "440": 101, "441": 105, "442": 105, "448": 442}, "uri": "courseware/course_about.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/course_about.html"}
__M_END_METADATA
"""
