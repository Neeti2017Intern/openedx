# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946145.302567
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/course_navigation.html'
_template_uri = u'/courseware/course_navigation.html'
_source_encoding = 'utf-8'
_exports = [u'extratabs']



from django.utils.translation import ugettext as _
from courseware.tabs import get_course_tab_list
from django.core.urlresolvers import reverse
from django.conf import settings
from openedx.core.djangoapps.course_groups.partition_scheme import get_cohorted_user_partition
from openedx.core.djangolib.js_utils import dump_js_escaped_json
from openedx.core.djangolib.markup import HTML, Text
from student.models import CourseEnrollment


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'/static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,active_page=None,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs,active_page=active_page)
        disable_student_access = context.get('disable_student_access', UNDEFINED)
        disable_tabs = context.get('disable_tabs', UNDEFINED)
        tab_image = context.get('tab_image', UNDEFINED)
        masquerade = context.get('masquerade', UNDEFINED)
        request = context.get('request', UNDEFINED)
        disable_preview_menu = context.get('disable_preview_menu', UNDEFINED)
        def extratabs():
            return render_extratabs(context._locals(__M_locals))
        course = context.get('course', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        sorted = context.get('sorted', UNDEFINED)
        active_page_context = context.get('active_page_context', UNDEFINED)
        default_tab = context.get('default_tab', UNDEFINED)
        staff_access = context.get('staff_access', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')

        if active_page is None and active_page_context is not UNDEFINED:
          # If active_page is not passed in as an argument, it may be in the context as active_page_context
          active_page = active_page_context
        
        def selected(is_selected):
          return "selected" if is_selected else ""
        
        show_preview_menu = not disable_preview_menu and staff_access and active_page in ["courseware", "info"]
        cohorted_user_partition = get_cohorted_user_partition(course)
        masquerade_user_name = masquerade.user_name if masquerade else None
        masquerade_group_id = masquerade.group_id if masquerade else None
        staff_selected = selected(not masquerade or masquerade.role != "student")
        specific_student_selected = selected(not staff_selected and masquerade.user_name)
        student_selected = selected(not staff_selected and not specific_student_selected and not masquerade_group_id)
        include_special_exams = settings.FEATURES.get('ENABLE_SPECIAL_EXAMS', False) and (course.enable_proctored_exams or course.enable_timed_exams)
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['masquerade_user_name','include_special_exams','selected','specific_student_selected','show_preview_menu','cohorted_user_partition','active_page','staff_selected','masquerade_group_id','student_selected'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n')
        if include_special_exams:
            __M_writer(u'    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.js(group=u'proctoring'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
            for template_name in ["proctored-exam-status"]:
                __M_writer(u'        <script type="text/template" id="')
                __M_writer(filters.html_escape(filters.decode.utf8(template_name)))
                __M_writer(u'-tpl">\n            ')
                def ccall(caller):
                    def body():
                        __M_writer = context.writer()
                        return ''
                    return [body]
                context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
                try:
                    __M_writer(filters.html_escape(filters.decode.utf8(static.include(path=u'courseware/' + (template_name) + u'.underscore'))))
                finally:
                    context.caller_stack.nextcaller = None
                __M_writer(u'\n        </script>\n')
            __M_writer(u'    <div class="proctored_exam_status"></div>\n')
        if show_preview_menu:
            __M_writer(u'    <nav class="wrapper-preview-menu" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Course View'))))
            __M_writer(u'">\n        <div class="preview-menu">\n            <ol class="preview-actions">\n                <li class="action-preview">\n                    <form action="#" class="action-preview-form" method="post">\n                        <label for="action-preview-select" class="action-preview-label">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("View this course as:"))))
            __M_writer(u'</label>\n                        <select class="action-preview-select" id="action-preview-select" name="select">\n                            <option value="staff" ')
            __M_writer(filters.html_escape(filters.decode.utf8(staff_selected)))
            __M_writer(u'>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Staff"))))
            __M_writer(u'</option>\n                            <option value="student" ')
            __M_writer(filters.html_escape(filters.decode.utf8(student_selected)))
            __M_writer(u'>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Student"))))
            __M_writer(u'</option>\n                            <option value="specific student" ')
            __M_writer(filters.html_escape(filters.decode.utf8(specific_student_selected)))
            __M_writer(u'>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Specific student"))))
            __M_writer(u'</option>\n')
            if cohorted_user_partition:
                for group in sorted(cohorted_user_partition.groups, key=lambda group: group.name):
                    __M_writer(u'                                <option value="group.id" data-group-id="')
                    __M_writer(filters.html_escape(filters.decode.utf8(group.id)))
                    __M_writer(u'" ')
                    __M_writer(filters.html_escape(filters.decode.utf8(selected(masquerade_group_id == group.id))))
                    __M_writer(u'>\n                                    ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Student in {content_group}").format(content_group=group.name))))
                    __M_writer(u'\n                                </option>\n')
            __M_writer(u'                        </select>\n                        <div class="action-preview-username-container">\n                          <label for="action-preview-username" class="action-preview-label">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Username or email:"))))
            __M_writer(u'</label>\n                          <input type="text" class="action-preview-username" id="action-preview-username">\n                        </div>\n                        <button type="submit" class="sr" name="submit" value="submit">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Set preview mode"))))
            __M_writer(u'</button>\n                    </form>\n                </li>\n            </ol>\n')
            if specific_student_selected:
                __M_writer(u'                <div class="preview-specific-student-notice">\n                    <p>\n                        ')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_("You are now viewing the course as {i_start}{user_name}{i_end}.")).format(
                            user_name=masquerade_user_name,
                            i_start=HTML(u'<i>'),
                            i_end=HTML(u'</i>'),
                        ))))
                __M_writer(u'\n                    </p>\n                </div>\n')
            __M_writer(u'        </div>\n    </nav>\n')
        __M_writer(u'\n')
        if disable_tabs is UNDEFINED or not disable_tabs:
            __M_writer(u'    <nav class="')
            __M_writer(filters.html_escape(filters.decode.utf8(active_page)))
            __M_writer(u' wrapper-course-material" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Course Material'))))
            __M_writer(u'">\n        <div class="course-material">\n            ')

            tab_list = get_course_tab_list(request, course)
            tabs_tmpl = static.get_template_path('/courseware/tabs.html')
            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['tab_list','tabs_tmpl'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n            <ol class="tabs course-tabs">\n                ')
            runtime._include_file(context, (tabs_tmpl), _template_uri, tab_list=tab_list,active_page=active_page,default_tab=default_tab,tab_image=tab_image)
            __M_writer(u'\n                ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'extratabs'):
                context['self'].extratabs(**pageargs)
            

            __M_writer(u'\n            </ol>\n        </div>\n    </nav>\n')
        __M_writer(u'\n')
        if show_preview_menu:
            __M_writer(u'    ')

            preview_options = {
                "courseId": course.id,
                "disableStudentAccess": disable_student_access if disable_student_access is not UNDEFINED else False,
                "specificStudentSelected": specific_student_selected,
                "cohortedUserPartitionId": cohorted_user_partition.id if cohorted_user_partition else None,
                "masqueradeUsername" : masquerade_user_name if masquerade_user_name is not UNDEFINED else None,
            }
            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['preview_options'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n    ')
            def ccall(caller):
                def body():
                    __M_writer = context.writer()
                    __M_writer(u'\n        PreviewFactory(')
                    __M_writer(dump_js_escaped_json(preview_options ))
                    __M_writer(u');\n    ')
                    return ''
                return [body]
            context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
            try:
                __M_writer(filters.html_escape(filters.decode.utf8(static.require_module_async(class_name=u'PreviewFactory',module_name=u'lms/js/preview/preview_factory'))))
            finally:
                context.caller_stack.nextcaller = None
            __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_extratabs(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def extratabs():
            return render_extratabs(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"128": 54, "129": 55, "130": 55, "131": 55, "132": 55, "133": 55, "134": 56, "135": 56, "136": 60, "137": 62, "138": 62, "139": 65, "140": 65, "141": 69, "142": 70, "143": 72, "16": 4, "148": 76, "149": 80, "150": 83, "151": 84, "152": 85, "153": 85, "154": 85, "155": 85, "156": 85, "157": 87, "34": 3, "164": 90, "37": 2, "166": 92, "171": 93, "172": 98, "173": 99, "174": 100, "175": 100, "56": 2, "57": 3, "58": 13, "59": 15, "191": 109, "192": 110, "193": 110, "198": 109, "201": 111, "79": 31, "80": 33, "81": 34, "89": 34, "218": 207, "207": 93, "92": 34, "93": 35, "94": 36, "95": 36, "96": 36, "165": 92, "187": 108, "104": 37, "107": 37, "108": 40, "109": 42, "110": 43, "111": 43, "112": 43, "113": 48, "114": 48, "115": 50, "116": 50, "117": 50, "118": 50, "119": 51, "120": 51, "121": 51, "122": 51, "123": 52, "124": 52, "125": 52, "126": 52, "127": 53}, "uri": "/courseware/course_navigation.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/templates/courseware/course_navigation.html"}
__M_END_METADATA
"""
