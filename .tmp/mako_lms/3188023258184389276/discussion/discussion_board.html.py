# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497946422.220975
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/djangoapps/discussion/templates/discussion/discussion_board.html'
_template_uri = 'discussion/discussion_board.html'
_source_encoding = 'utf-8'
_exports = [u'content', u'bodyclass', 'online_help_token', u'pagetitle', u'js_extra', u'headextra', u'base_js_dependencies']


main_css = "style-discussion-main" 


import json
from django.utils.translation import ugettext as _
from django.template.defaultfilters import escapejs
from django.core.urlresolvers import reverse

from django_comment_client.permissions import has_permission
from openedx.core.djangolib.js_utils import dump_js_escaped_json, js_escaped_string


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'../main.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        can_create_subcomment = context.get('can_create_subcomment', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        sort_preference = context.get('sort_preference', UNDEFINED)
        user_cohort = context.get('user_cohort', UNDEFINED)
        course = context.get('course', UNDEFINED)
        def js_extra():
            return render_js_extra(context._locals(__M_locals))
        static = _mako_get_namespace(context, 'static')
        user = context.get('user', UNDEFINED)
        def pagetitle():
            return render_pagetitle(context._locals(__M_locals))
        flag_moderator = context.get('flag_moderator', UNDEFINED)
        def headextra():
            return render_headextra(context._locals(__M_locals))
        can_create_comment = context.get('can_create_comment', UNDEFINED)
        course_id = context.get('course_id', UNDEFINED)
        def base_js_dependencies():
            return render_base_js_dependencies(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u'\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'pagetitle'):
            context['self'].pagetitle(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'headextra'):
            context['self'].headextra(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'base_js_dependencies'):
            context['self'].base_js_dependencies(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'js_extra'):
            context['self'].js_extra(**pageargs)
        

        __M_writer(u'\n\n')
        runtime._include_file(context, u'../courseware/course_navigation.html', _template_uri, active_page='discussion')
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer(u'\n\n')
        runtime._include_file(context, u'_underscore_templates.html', _template_uri)
        __M_writer(u'\n')
        runtime._include_file(context, u'_thread_list_template.html', _template_uri)
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        can_create_subcomment = context.get('can_create_subcomment', UNDEFINED)
        user_cohort = context.get('user_cohort', UNDEFINED)
        sort_preference = context.get('sort_preference', UNDEFINED)
        def content():
            return render_content(context)
        course = context.get('course', UNDEFINED)
        can_create_comment = context.get('can_create_comment', UNDEFINED)
        flag_moderator = context.get('flag_moderator', UNDEFINED)
        course_id = context.get('course_id', UNDEFINED)
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n<section class="discussion discussion-board container" id="discussion-container"\n         data-course-id="')
        __M_writer(filters.html_escape(filters.decode.utf8(course_id)))
        __M_writer(u'"\n         data-user-create-comment="')
        __M_writer(filters.html_escape(filters.decode.utf8(json.dumps(can_create_comment))))
        __M_writer(u'"\n         data-user-create-subcomment="')
        __M_writer(filters.html_escape(filters.decode.utf8(json.dumps(can_create_subcomment))))
        __M_writer(u'"\n         data-read-only="false"\n         data-sort-preference="')
        __M_writer(filters.html_escape(filters.decode.utf8(sort_preference)))
        __M_writer(u'"\n         data-flag-moderator="')
        __M_writer(filters.html_escape(filters.decode.utf8(json.dumps(flag_moderator))))
        __M_writer(u'"\n         data-user-cohort-id="')
        __M_writer(filters.html_escape(filters.decode.utf8(user_cohort)))
        __M_writer(u'">\n    <header class="page-header has-secondary">\n')
        __M_writer(u'        <div class="page-header-main">\n            <nav aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_('Discussions'))))
        __M_writer(u'" class="sr-is-focusable" tabindex="-1">\n                <div class="has-breadcrumbs"></div>\n            </nav>\n        </div>\n        <div class="page-header-secondary">\n')
        if has_permission(user, 'create_thread', course.id):
            __M_writer(u'            <div class="form-actions">\n                <button class="btn btn-small new-post-btn">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Add a Post"))))
            __M_writer(u'</button>\n            </div>\n')
        __M_writer(u'            <div class="forum-search"></div>\n        </div>\n    </header>\n    <div class="page-content">\n        <div class="discussion-body layout layout-1t2t">\n            <aside class="forum-nav layout-col layout-col-a" role="complementary" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Discussion thread list"))))
        __M_writer(u'">\n                ')
        runtime._include_file(context, u'_filter_dropdown.html', _template_uri)
        __M_writer(u'\n                <div class="discussion-thread-list-container"></div>\n            </aside>\n\n            <main id="main" aria-label="Content" tabindex="-1" class="discussion-column layout-col layout-col-b">\n                <article class="new-post-article is-hidden" style="display: none" tabindex="-1" aria-label="')
        __M_writer(filters.html_escape(filters.decode.utf8(_("New topic form"))))
        __M_writer(u'"></article>\n                <div class="forum-content"></div>\n            </main>\n        </div>\n    </div>\n</section>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        __M_writer(u'discussion')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_online_help_token(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        return "discussions" 
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_pagetitle(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def pagetitle():
            return render_pagetitle(context)
        course = context.get('course', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(filters.html_escape(filters.decode.utf8(_("Discussion - {course_number}").format(course_number=course.display_number_with_default))))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js_extra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def js_extra():
            return render_js_extra(context)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        def ccall(caller):
            def body():
                annotated_content_info = context.get('annotated_content_info', UNDEFINED)
                course_settings = context.get('course_settings', UNDEFINED)
                roles = context.get('roles', UNDEFINED)
                thread_pages = context.get('thread_pages', UNDEFINED)
                sort_preference = context.get('sort_preference', UNDEFINED)
                user_info = context.get('user_info', UNDEFINED)
                course = context.get('course', UNDEFINED)
                unicode = context.get('unicode', UNDEFINED)
                threads = context.get('threads', UNDEFINED)
                __M_writer = context.writer()
                __M_writer(u"\nDiscussionBoardFactory({\n    courseId: '")
                __M_writer(js_escaped_string(unicode(course.id) ))
                __M_writer(u'\',\n    $el: $(".discussion-board"),\n    user_info: ')
                __M_writer(dump_js_escaped_json(user_info ))
                __M_writer(u',\n    roles: ')
                __M_writer(dump_js_escaped_json(roles ))
                __M_writer(u",\n    sort_preference: '")
                __M_writer(js_escaped_string(sort_preference ))
                __M_writer(u"',\n    threads: ")
                __M_writer(dump_js_escaped_json(threads ))
                __M_writer(u",\n    thread_pages: '")
                __M_writer(js_escaped_string(thread_pages ))
                __M_writer(u"',\n    content_info: ")
                __M_writer(dump_js_escaped_json(annotated_content_info ))
                __M_writer(u",\n    course_name: '")
                __M_writer(js_escaped_string(course.display_name_with_default ))
                __M_writer(u"',\n    course_settings: ")
                __M_writer(dump_js_escaped_json(course_settings ))
                __M_writer(u'\n});\n')
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.require_module(class_name=u'DiscussionBoardFactory',module_name=u'discussion/js/discussion_board_factory'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_headextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def headextra():
            return render_headextra(context)
        __M_writer = context.writer()
        __M_writer(u'\n')
        runtime._include_file(context, u'../discussion/_js_head_dependencies.html', _template_uri)
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_base_js_dependencies(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def base_js_dependencies():
            return render_base_js_dependencies(context)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'    ')
        runtime._include_file(context, u'/discussion/_js_body_dependencies.html', _template_uri, disable_fast_preview=False)
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 3, "18": 9, "35": 7, "41": 5, "67": 2, "68": 3, "69": 5, "70": 6, "71": 7, "72": 8, "73": 17, "78": 19, "83": 20, "88": 24, "93": 29, "98": 46, "99": 48, "100": 48, "105": 91, "106": 93, "107": 93, "108": 94, "109": 94, "115": 50, "129": 50, "130": 52, "131": 52, "132": 53, "133": 53, "134": 54, "135": 54, "136": 56, "137": 56, "138": 57, "139": 57, "140": 58, "141": 58, "142": 61, "143": 62, "144": 62, "145": 68, "146": 69, "147": 70, "148": 70, "149": 74, "150": 79, "151": 79, "152": 80, "153": 80, "154": 85, "155": 85, "161": 19, "167": 19, "173": 8, "177": 8, "184": 20, "191": 20, "197": 31, "204": 31, "217": 32, "218": 34, "219": 34, "220": 36, "221": 36, "222": 37, "223": 37, "224": 38, "225": 38, "226": 39, "227": 39, "228": 40, "229": 40, "230": 41, "231": 41, "232": 42, "233": 42, "234": 43, "235": 43, "240": 32, "243": 45, "249": 22, "255": 22, "256": 23, "257": 23, "263": 26, "269": 26, "270": 28, "271": 28, "272": 28, "278": 272}, "uri": "discussion/discussion_board.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/lms/djangoapps/discussion/templates/discussion/discussion_board.html"}
__M_END_METADATA
"""
