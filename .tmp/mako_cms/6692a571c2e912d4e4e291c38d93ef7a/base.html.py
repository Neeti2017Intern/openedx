# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849144.301136
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/base.html'
_template_uri = u'base.html'
_source_encoding = 'utf-8'
_exports = [u'bodyclass', u'title', u'jsextra', u'view_notes', u'content', u'page_alert', u'header_extras', u'modal_placeholder', u'requirejs']


main_css = "style-main-v1" 


from django.utils.translation import ugettext as _

from openedx.core.djangolib.js_utils import (
    dump_js_escaped_json, js_escaped_string
)


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def requirejs():
            return render_requirejs(context._locals(__M_locals))
        context_course = context.get('context_course', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        self = context.get('self', UNDEFINED)
        def title():
            return render_title(context._locals(__M_locals))
        def jsextra():
            return render_jsextra(context._locals(__M_locals))
        def view_notes():
            return render_view_notes(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        EDX_ROOT_URL = context.get('EDX_ROOT_URL', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        user = context.get('user', UNDEFINED)
        LANGUAGE_CODE = context.get('LANGUAGE_CODE', UNDEFINED)
        def page_alert():
            return render_page_alert(context._locals(__M_locals))
        def header_extras():
            return render_header_extras(context._locals(__M_locals))
        def modal_placeholder():
            return render_modal_placeholder(context._locals(__M_locals))
        context_library = context.get('context_library', UNDEFINED)
        hasattr = context.get('hasattr', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n<!doctype html>\n<!--[if lte IE 9]><html class="ie9 lte9" lang="')
        __M_writer(filters.html_escape(filters.decode.utf8(LANGUAGE_CODE)))
        __M_writer(u'"><![endif]-->\n<!--[if !IE]><<!--><html lang="')
        __M_writer(filters.html_escape(filters.decode.utf8(LANGUAGE_CODE)))
        __M_writer(u'"><!--<![endif]-->\n  <head dir="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.dir_rtl())))
        __M_writer(u'">\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">\n    <title>\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer(u' |\n')
        if context_course:
            __M_writer(u'        ')
            ctx_loc = context_course.location 
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['ctx_loc'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n        ')
            __M_writer(filters.html_escape(filters.decode.utf8(context_course.display_name_with_default)))
            __M_writer(u' |\n')
        elif context_library:
            __M_writer(u'        ')
            __M_writer(filters.html_escape(filters.decode.utf8(context_library.display_name_with_default)))
            __M_writer(u' |\n')
        __M_writer(u'        ')
        __M_writer(filters.html_escape(filters.decode.utf8(settings.STUDIO_NAME)))
        __M_writer(u'\n    </title>\n\n    ')

        jsi18n_path = "js/i18n/{language}/djangojs.js".format(language=LANGUAGE_CODE)
            
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['jsi18n_path'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n\n    <script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url(jsi18n_path))))
        __M_writer(u'"></script>\n    <meta name="viewport" content="width=device-width,initial-scale=1">\n    <meta name="path_prefix" content="')
        __M_writer(filters.html_escape(filters.decode.utf8(EDX_ROOT_URL)))
        __M_writer(u'">\n\n    ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-vendor'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n    ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-vendor-tinymce-content'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n    ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=u'style-vendor-tinymce-skin'))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n    ')
        def ccall(caller):
            def body():
                __M_writer = context.writer()
                return ''
            return [body]
        context.caller_stack.nextcaller = runtime.Namespace('caller', context, callables=ccall(__M_caller))
        try:
            __M_writer(filters.html_escape(filters.decode.utf8(static.css(group=(self.attr.main_css)))))
        finally:
            context.caller_stack.nextcaller = None
        __M_writer(u'\n\n    ')
        runtime._include_file(context, u'widgets/segment-io.html', _template_uri)
        __M_writer(u'\n\n    ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header_extras'):
            context['self'].header_extras(**pageargs)
        

        __M_writer(u'\n  </head>\n\n  <body class="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.dir_rtl())))
        __M_writer(u' ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u' lang_')
        __M_writer(filters.html_escape(filters.decode.utf8(LANGUAGE_CODE)))
        __M_writer(u'">\n    ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'view_notes'):
            context['self'].view_notes(**pageargs)
        

        __M_writer(u'\n\n    <a class="nav-skip" href="#main">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Skip to main content"))))
        __M_writer(u'</a>\n\n    <script type="text/javascript">\n      window.baseUrl = "')
        __M_writer(js_escaped_string(settings.STATIC_URL ))
        __M_writer(u'";\n      var require = {baseUrl: window.baseUrl};\n    </script>\n    <script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url("common/js/vendor/require.js"))))
        __M_writer(u'"></script>\n    <script type="text/javascript" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url("cms/js/require-config.js"))))
        __M_writer(u'"></script>\n\n    <!-- view -->\n    <div class="wrapper wrapper-view" dir="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.dir_rtl())))
        __M_writer(u'">\n        ')
        online_help_token = self.online_help_token() if hasattr(self, 'online_help_token') else None 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['online_help_token'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n        ')
        runtime._include_file(context, u'widgets/header.html', _template_uri, online_help_token=online_help_token)
        __M_writer(u'\n\n      <div id="page-alert">\n      ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'page_alert'):
            context['self'].page_alert(**pageargs)
        

        __M_writer(u'\n      </div>\n\n      <main id="main" aria-label="Content" tabindex="-1">\n          <div id="content">\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer(u'\n          </div>\n      </main>\n\n')
        if user.is_authenticated():
            __M_writer(u'        ')
            runtime._include_file(context, u'widgets/sock.html', _template_uri, online_help_token=online_help_token)
            __M_writer(u'\n')
        __M_writer(u'      ')
        runtime._include_file(context, u'widgets/footer.html', _template_uri)
        __M_writer(u'\n\n      <div id="page-notification"></div>\n    </div>\n\n    <div id="page-prompt"></div>\n\n    ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'modal_placeholder'):
            context['self'].modal_placeholder(**pageargs)
        

        __M_writer(u'\n\n    ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'jsextra'):
            context['self'].jsextra(**pageargs)
        

        __M_writer(u'\n    <script type="text/javascript">\n      require([\'common/js/common_libraries\'], function () {\n          require([\'js/factories/base\'], function () {\n              require([\'js/models/course\'], function(Course) {\n')
        if context_course:
            __M_writer(u'                      window.course = new Course({\n                          id: "')
            __M_writer(js_escaped_string(context_course.id ))
            __M_writer(u'",\n                          name: "')
            __M_writer(js_escaped_string(context_course.display_name_with_default ))
            __M_writer(u'",\n                          url_name: "')
            __M_writer(js_escaped_string(context_course.location.name ))
            __M_writer(u'",\n                          org: "')
            __M_writer(js_escaped_string(context_course.location.org ))
            __M_writer(u'",\n                          num: "')
            __M_writer(js_escaped_string(context_course.location.course ))
            __M_writer(u'",\n                          display_course_number: "')
            __M_writer(js_escaped_string(context_course.display_coursenumber ))
            __M_writer(u'",\n                          revision: "')
            __M_writer(js_escaped_string(context_course.location.revision ))
            __M_writer(u'",\n                          self_paced: ')
            __M_writer(dump_js_escaped_json(context_course.self_paced ))
            __M_writer(u'\n                      });\n')
        if user.is_authenticated():
            __M_writer(u"                      require(['js/sock']);\n")
        __M_writer(u'                  ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'requirejs'):
            context['self'].requirejs(**pageargs)
        

        __M_writer(u'\n              });\n          });\n      });\n    </script>\n    ')
        runtime._include_file(context, u'widgets/segment-io-footer.html', _template_uri)
        __M_writer(u'\n    <div class="modal-cover"></div>\n  </body>\n</html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def title():
            return render_title(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_jsextra(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def jsextra():
            return render_jsextra(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_view_notes(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def view_notes():
            return render_view_notes(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_page_alert(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def page_alert():
            return render_page_alert(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header_extras(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header_extras():
            return render_header_extras(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_modal_placeholder(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def modal_placeholder():
            return render_modal_placeholder(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_requirejs(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def requirejs():
            return render_requirejs(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 5, "18": 9, "33": 8, "36": 17, "68": 2, "69": 5, "70": 8, "71": 15, "72": 17, "73": 19, "74": 19, "75": 20, "76": 20, "77": 21, "78": 21, "83": 25, "84": 26, "85": 27, "86": 27, "90": 27, "91": 28, "92": 28, "93": 29, "94": 30, "95": 30, "96": 30, "97": 32, "98": 32, "99": 32, "100": 35, "106": 37, "107": 39, "108": 39, "109": 41, "110": 41, "118": 43, "121": 43, "129": 44, "132": 44, "140": 45, "143": 45, "151": 47, "154": 47, "155": 49, "156": 49, "161": 51, "162": 54, "163": 54, "168": 54, "169": 54, "170": 54, "175": 55, "176": 57, "177": 57, "178": 60, "179": 60, "180": 63, "181": 63, "182": 64, "183": 64, "184": 67, "185": 67, "186": 68, "190": 68, "191": 69, "192": 69, "197": 72, "202": 77, "203": 81, "204": 82, "205": 82, "206": 82, "207": 84, "208": 84, "209": 84, "214": 91, "219": 93, "220": 98, "221": 99, "222": 100, "223": 100, "224": 101, "225": 101, "226": 102, "227": 102, "228": 103, "229": 103, "230": 104, "231": 104, "232": 105, "233": 105, "234": 106, "235": 106, "236": 107, "237": 107, "238": 110, "239": 111, "240": 113, "245": 113, "246": 118, "247": 118, "253": 54, "264": 25, "275": 93, "286": 55, "297": 77, "308": 72, "319": 51, "330": 91, "341": 113, "352": 341}, "uri": "base.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/base.html"}
__M_END_METADATA
"""
