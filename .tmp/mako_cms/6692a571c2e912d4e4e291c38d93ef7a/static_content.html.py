# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849144.383361
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/common/djangoapps/pipeline_mako/templates/static_content.html'
_template_uri = u'static_content.html'
_source_encoding = 'utf-8'
_exports = ['get_page_title_breadcrumbs', 'is_request_in_themed_site', 'get_released_languages', 'marketing_link', 'url', 'get_tech_support_email_address', 'get_value', 'optional_include_mako', 'js', 'dir_rtl', 'get_platform_name', 'require_module', 'require_module_async', 'show_language_selector', 'include', 'get_template_path', 'css', 'certificate_asset_url']



from django.contrib.staticfiles.storage import staticfiles_storage
from pipeline_mako import compressed_css, compressed_js
from django.utils.translation import get_language_bidi
from mako.exceptions import TemplateLookupException
from edxmako.shortcuts import marketing_link

from openedx.core.djangolib.js_utils import js_escaped_string
from openedx.core.djangoapps.site_configuration.helpers import (
  page_title_breadcrumbs,
  get_value,
)

from openedx.core.djangoapps.theming.helpers import (
  get_template_path,
  is_request_in_themed_site,
)
from certificates.api import get_asset_url_by_slug
from openedx.core.djangoapps.lang_pref.api import released_languages


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_get_page_title_breadcrumbs(context,*args):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        return page_title_breadcrumbs(*args)
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_is_request_in_themed_site(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        return is_request_in_themed_site()
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_get_released_languages(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        return released_languages()
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_marketing_link(context,name):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        link = marketing_link(name)
        return "/" if link == "#" else link
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_url(context,file,raw=False):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        try:
            url = staticfiles_storage.url(file)
        except:
            url = file
        ## HTML-escaping must be handled by caller
        
        
        __M_writer(filters.decode.utf8(url ))
        __M_writer(filters.html_escape(filters.decode.utf8("?raw" if raw else "")))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_get_tech_support_email_address(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()

        return get_value('email_from_address', settings.TECH_SUPPORT_EMAIL)
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_get_value(context,val_name,default=None,**kwargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        return get_value(val_name, default=default, **kwargs)
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_optional_include_mako(context,file,is_theming_enabled=False):
    __M_caller = context.caller_stack._push_frame()
    try:
        self = context.get('self', UNDEFINED)
        __M_writer = context.writer()

# http://stackoverflow.com/q/21219531
        if is_theming_enabled:
            file = get_template_path(file)
        try:
            tmpl = self.get_template(file)
        except TemplateLookupException:
            pass
        else:
            tmpl.render_context(context)
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_js(context,group):
    __M_caller = context.caller_stack._push_frame()
    try:
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        if settings.PIPELINE_ENABLED:
            __M_writer(u'    ')
            __M_writer(filters.decode.utf8(compressed_js(group) ))
            __M_writer(u'\n')
        else:
            for filename in settings.PIPELINE_JS[group]['source_filenames']:
                __M_writer(u'      <script type="text/javascript" src="')
                __M_writer(filters.html_escape(filters.decode.utf8(staticfiles_storage.url(filename.replace('.coffee', '.js')))))
                __M_writer(u'"></script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_dir_rtl(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        return 'rtl' if get_language_bidi() else 'ltr'
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_get_platform_name(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()

        return get_value('platform_name', settings.PLATFORM_NAME)
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_require_module(context,module_name,class_name):
    __M_caller = context.caller_stack._push_frame()
    try:
        caller = context.get('caller', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n    ')
        __M_writer(u'\n')
        if not settings.REQUIRE_DEBUG:
            __M_writer(u'      <script type="text/javascript" src="')
            __M_writer(filters.html_escape(filters.decode.utf8(staticfiles_storage.url(module_name + '.js') + '?raw')))
            __M_writer(u'"></script>\n')
        __M_writer(u'    <script type="text/javascript">\n        (function (require) {\n            require([\'')
        __M_writer(js_escaped_string(module_name ))
        __M_writer(u"'], function (")
        __M_writer(filters.decode.utf8(class_name ))
        __M_writer(u') {\n                ')
        __M_writer(filters.decode.utf8(caller.body() ))
        __M_writer(u'\n            });\n        }).call(this, require || RequireJS.require);\n    </script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_require_module_async(context,module_name,class_name):
    __M_caller = context.caller_stack._push_frame()
    try:
        caller = context.get('caller', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n  ')
        __M_writer(u'\n  <script type="text/javascript">\n    (function (require) {\n')
        if settings.REQUIRE_DEBUG:
            __M_writer(u"          (function (require) {\n              require(['")
            __M_writer(js_escaped_string(module_name ))
            __M_writer(u"'], function (")
            __M_writer(filters.decode.utf8(class_name ))
            __M_writer(u') {\n                  ')
            __M_writer(filters.decode.utf8(caller.body() ))
            __M_writer(u'\n              });\n          }).call(this, require || RequireJS.require);\n')
        else:
            __M_writer(u"        require(['")
            __M_writer(js_escaped_string(staticfiles_storage.url(module_name + ".js") + "?raw" ))
            __M_writer(u"'], function () {\n          require(['")
            __M_writer(js_escaped_string(module_name ))
            __M_writer(u"'], function (")
            __M_writer(filters.decode.utf8(class_name ))
            __M_writer(u') {\n            ')
            __M_writer(filters.decode.utf8(caller.body() ))
            __M_writer(u'\n          });\n        });\n')
        __M_writer(u'    }).call(this, require || RequireJS.require);\n  </script>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_show_language_selector(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()

        return get_value('SHOW_LANGUAGE_SELECTOR', settings.FEATURES.get('SHOW_LANGUAGE_SELECTOR', False))
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_include(context,path):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        from django.conf import settings
        from django.template.engine import Engine
        from django.template.loaders.filesystem import Loader
        engine = Engine(dirs=settings.DEFAULT_TEMPLATE_ENGINE['DIRS'])
        source, template_path = Loader(engine).load_template_source(path)
        
        
        __M_writer(filters.decode.utf8(source ))
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_get_template_path(context,relative_path,**kwargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        return get_template_path(relative_path, **kwargs)
        
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_css(context,group,raw=False):
    __M_caller = context.caller_stack._push_frame()
    try:
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n  ')

        rtl_group = '{}-rtl'.format(group)
        
        if get_language_bidi() and rtl_group in settings.PIPELINE_CSS:
          group = rtl_group
          
        
        __M_writer(u'\n\n')
        if settings.PIPELINE_ENABLED:
            __M_writer(u'    ')
            __M_writer(filters.decode.utf8(compressed_css(group, raw=raw) ))
            __M_writer(u'\n')
        else:
            for filename in settings.PIPELINE_CSS[group]['source_filenames']:
                __M_writer(u'      <link rel="stylesheet" href="')
                __M_writer(filters.html_escape(filters.decode.utf8(staticfiles_storage.url(filename.replace('.scss', '.css')))))
                __M_writer(filters.html_escape(filters.decode.utf8("?raw" if raw else "")))
                __M_writer(u'" type="text/css" media="all" / >\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_certificate_asset_url(context,slug):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()

        try:
            url = get_asset_url_by_slug(slug)
        except:
            url = ''
        ## HTML-escaping must be handled by caller
        
        
        __M_writer(filters.decode.utf8(url ))
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "37": 1, "42": 1, "43": 21, "44": 26, "45": 34, "46": 42, "47": 59, "48": 69, "49": 78, "50": 86, "51": 104, "52": 133, "53": 145, "54": 150, "55": 154, "56": 158, "57": 162, "58": 166, "59": 170, "60": 174, "61": 178, "67": 148, "71": 148, "80": 164, "84": 164, "93": 176, "97": 176, "106": 23, "110": 23, "120": 28, "124": 28, "132": 34, "133": 34, "139": 168, "144": 168, "153": 156, "157": 156, "166": 135, "171": 135, "188": 61, "193": 61, "194": 62, "195": 63, "196": 63, "197": 63, "198": 64, "199": 65, "200": 66, "201": 66, "202": 66, "208": 76, "212": 76, "221": 152, "226": 152, "235": 88, "241": 88, "242": 93, "243": 94, "244": 95, "245": 95, "246": 95, "247": 97, "248": 99, "249": 99, "250": 99, "251": 99, "252": 100, "253": 100, "259": 106, "265": 106, "266": 111, "267": 114, "268": 115, "269": 116, "270": 116, "271": 116, "272": 116, "273": 117, "274": 117, "275": 120, "276": 125, "277": 125, "278": 125, "279": 126, "280": 126, "281": 126, "282": 126, "283": 127, "284": 127, "285": 131, "291": 172, "296": 172, "305": 80, "309": 80, "317": 86, "323": 160, "327": 160, "336": 44, "341": 44, "342": 45, "349": 50, "350": 52, "351": 53, "352": 53, "353": 53, "354": 54, "355": 55, "356": 56, "357": 56, "358": 56, "359": 56, "365": 36, "369": 36, "377": 42, "383": 377}, "uri": "static_content.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/common/djangoapps/pipeline_mako/templates/static_content.html"}
__M_END_METADATA
"""
