# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849144.987047
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/footer.html'
_template_uri = u'widgets/footer.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        is_any_marketing_link_set = context.get('is_any_marketing_link_set', UNDEFINED)
        is_marketing_link_set = context.get('is_marketing_link_set', UNDEFINED)
        marketing_link = context.get('marketing_link', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\n<div class="wrapper-footer wrapper">\n  <footer class="primary" role="contentinfo">\n\n    <div class="footer-content-primary">\n      <div class="colophon">\n        <p>&copy; ')
        __M_writer(filters.decode.utf8(settings.COPYRIGHT_YEAR))
        __M_writer(u' <a data-rel="edx.org" href="')
        __M_writer(filters.decode.utf8(marketing_link('ROOT')))
        __M_writer(u'" rel="external">')
        __M_writer(filters.decode.utf8(settings.PLATFORM_NAME))
        __M_writer(u'</a>.</p>\n      </div>\n\n')
        if is_any_marketing_link_set(['TOS', 'PRIVACY']):
            __M_writer(u'        <nav class="nav-peripheral">\n          <ol>\n')
            if is_marketing_link_set('TOS'):
                __M_writer(u'              <li class="nav-item nav-peripheral-tos">\n                <a data-rel="edx.org" href="')
                __M_writer(filters.decode.utf8(marketing_link('TOS')))
                __M_writer(u'">')
                __M_writer(filters.decode.utf8(_("Terms of Service")))
                __M_writer(u'</a>\n              </li>\n')
            if is_marketing_link_set('PRIVACY'):
                __M_writer(u'              <li class="nav-item nav-peripheral-pp">\n                <a data-rel="edx.org" href="')
                __M_writer(filters.decode.utf8(marketing_link('PRIVACY')))
                __M_writer(u'">')
                __M_writer(filters.decode.utf8(_("Privacy Policy")))
                __M_writer(u'</a>\n              </li>\n')
            __M_writer(u'          </ol>\n        </nav>\n')
        __M_writer(u'    </div>\n\n    <div class="footer-content-secondary" aria-label="{_(\'Legal\')}">\n      <div class="footer-about-copyright">\n')
        __M_writer(u'        <p>\n')
        __M_writer(u'          ')
        __M_writer(filters.decode.utf8(_("EdX, Open edX, Studio, and the edX and Open edX logos are registered trademarks or trademarks of {link_start}edX Inc.{link_end}").format(
            link_start=u"<a data-rel='edx.org' href='https://www.edx.org/'>",
            link_end=u"</a>"
          )))
        __M_writer(u'\n        </p>\n      </div>\n\n      <div class="footer-about-openedx">\n        <a href="http://open.edx.org" title="')
        __M_writer(filters.decode.utf8(_("Powered by Open edX")))
        __M_writer(u'">\n          <img alt="')
        __M_writer(filters.decode.utf8(_("Powered by Open edX")))
        __M_writer(u'" src="https://files.edx.org/openedx-logos/edx-openedx-logo-tag.png">\n        </a>\n      </div>\n    </div>\n  </footer>\n</div>\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 1, "21": 0, "30": 4, "31": 11, "32": 11, "33": 11, "34": 11, "35": 11, "36": 11, "37": 14, "38": 15, "39": 17, "40": 18, "41": 19, "42": 19, "43": 19, "44": 19, "45": 22, "46": 23, "47": 24, "48": 24, "49": 24, "50": 24, "51": 27, "52": 30, "53": 35, "54": 37, "55": 37, "59": 40, "60": 45, "61": 45, "62": 46, "63": 46, "69": 63}, "uri": "widgets/footer.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/footer.html"}
__M_END_METADATA
"""
