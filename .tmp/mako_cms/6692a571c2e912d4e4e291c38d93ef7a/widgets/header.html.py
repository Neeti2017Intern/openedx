# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849144.825152
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/header.html'
_template_uri = u'widgets/header.html'
_source_encoding = 'utf-8'
_exports = []



from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from contentstore.context_processors import doc_url


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,online_help_token,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs,online_help_token=online_help_token)
        csrf_token = context.get('csrf_token', UNDEFINED)
        context_course = context.get('context_course', UNDEFINED)
        context_library = context.get('context_library', UNDEFINED)
        len = context.get('len', UNDEFINED)
        show_programs_header = context.get('show_programs_header', UNDEFINED)
        get_online_help_info = context.get('get_online_help_info', UNDEFINED)
        dict = context.get('dict', UNDEFINED)
        user = context.get('user', UNDEFINED)
        unicode = context.get('unicode', UNDEFINED)
        LANGUAGE_CODE = context.get('LANGUAGE_CODE', UNDEFINED)
        static = _mako_get_namespace(context, 'static')
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n<div class="wrapper-header wrapper" id="view-top">\n  <header class="primary" role="banner">\n\n    <div class="wrapper wrapper-l">\n      <h1 class="branding">\n        <a class="brand-link" href="/">\n          <img class="brand-image" src="')
        __M_writer(filters.html_escape(filters.decode.utf8(static.url('images/studio-logo.png'))))
        __M_writer(u'" alt="')
        __M_writer(filters.html_escape(filters.decode.utf8(settings.STUDIO_NAME)))
        __M_writer(u'" />\n        </a>\n      </h1>\n\n')
        if context_course:
            __M_writer(u'      ')

            course_key = context_course.id
            index_url = reverse('contentstore.views.course_handler', kwargs={'course_key_string': unicode(course_key)})
            course_team_url = reverse('contentstore.views.course_team_handler', kwargs={'course_key_string': unicode(course_key)})
            assets_url = reverse('contentstore.views.assets_handler', kwargs={'course_key_string': unicode(course_key)})
            textbooks_url = reverse('contentstore.views.textbooks_list_handler', kwargs={'course_key_string': unicode(course_key)})
            videos_url = reverse('contentstore.views.videos_handler', kwargs={'course_key_string': unicode(course_key)})
            import_url = reverse('contentstore.views.import_handler', kwargs={'course_key_string': unicode(course_key)})
            course_info_url = reverse('contentstore.views.course_info_handler', kwargs={'course_key_string': unicode(course_key)})
            export_url = reverse('contentstore.views.export_handler', kwargs={'course_key_string': unicode(course_key)})
            settings_url = reverse('contentstore.views.settings_handler', kwargs={'course_key_string': unicode(course_key)})
            grading_url = reverse('contentstore.views.grading_handler', kwargs={'course_key_string': unicode(course_key)})
            advanced_settings_url = reverse('contentstore.views.advanced_settings_handler', kwargs={'course_key_string': unicode(course_key)})
            tabs_url = reverse('contentstore.views.tabs_handler', kwargs={'course_key_string': unicode(course_key)})
            certificates_url = ''
            if settings.FEATURES.get("CERTIFICATES_HTML_VIEW") and context_course.cert_html_view_enabled:
                certificates_url = reverse('contentstore.views.certificates.certificates_list_handler', kwargs={'course_key_string': unicode(course_key)})
                  
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['assets_url','index_url','import_url','course_team_url','course_key','export_url','settings_url','textbooks_url','course_info_url','certificates_url','videos_url','grading_url','advanced_settings_url','tabs_url'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n      <h2 class="info-course">\n        <span class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Current Course:"))))
            __M_writer(u'</span>\n        <a class="course-link" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(index_url)))
            __M_writer(u'">\n          <span class="course-org">')
            __M_writer(filters.html_escape(filters.decode.utf8(context_course.display_org_with_default)))
            __M_writer(u'</span><span class="course-number">')
            __M_writer(filters.html_escape(filters.decode.utf8(context_course.display_number_with_default)))
            __M_writer(u'</span>\n          <span class="course-title" title="')
            __M_writer(filters.html_escape(filters.decode.utf8(context_course.display_name_with_default)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(context_course.display_name_with_default)))
            __M_writer(u'</span>\n        </a>\n      </h2>\n\n      <nav class="nav-course nav-dd ui-left" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Course'))))
            __M_writer(u'">\n        <h2 class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course Navigation"))))
            __M_writer(u'</h2>\n        <ol>\n          <li class="nav-item nav-course-courseware">\n            <h3 class="title"><span class="label"><span class="label-prefix sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course"))))
            __M_writer(u' </span>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Content"))))
            __M_writer(u'</span> <span class="icon fa fa-caret-down ui-toggle-dd" aria-hidden="true"></span></h3>\n\n            <div class="wrapper wrapper-nav-sub">\n              <div class="nav-sub">\n                <ul>\n                  <li class="nav-item nav-course-courseware-outline">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(index_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Outline"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-courseware-updates">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(course_info_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Updates"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-courseware-pages">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(tabs_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Pages"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-courseware-uploads">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(assets_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Files & Uploads"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-courseware-textbooks">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(textbooks_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Textbooks"))))
            __M_writer(u'</a>\n                  </li>\n')
            if context_course.video_pipeline_configured:
                __M_writer(u'                  <li class="nav-item nav-course-courseware-videos">\n                    <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(videos_url)))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Video Uploads"))))
                __M_writer(u'</a>\n                  </li>\n')
            __M_writer(u'                </ul>\n              </div>\n            </div>\n          </li>\n\n          <li class="nav-item nav-course-settings">\n            <h3 class="title"><span class="label"><span class="label-prefix sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course"))))
            __M_writer(u' </span>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Settings"))))
            __M_writer(u'</span> <span class="icon fa fa-caret-down ui-toggle-dd" aria-hidden="true"></span></h3>\n\n            <div class="wrapper wrapper-nav-sub">\n              <div class="nav-sub">\n                <ul>\n                  <li class="nav-item nav-course-settings-schedule">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(settings_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Schedule & Details"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-settings-grading">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(grading_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Grading"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-settings-team">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(course_team_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course Team"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-settings-group-configurations">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('contentstore.views.group_configurations_list_handler', kwargs={'course_key_string': unicode(course_key)}))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Group Configurations"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-settings-advanced">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(advanced_settings_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Advanced Settings"))))
            __M_writer(u'</a>\n                  </li>\n')
            if certificates_url:
                __M_writer(u'                  <li class="nav-item nav-course-settings-certificates">\n                    <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(certificates_url)))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Certificates"))))
                __M_writer(u'</a>\n                  </li>\n')
            __M_writer(u'                </ul>\n              </div>\n            </div>\n          </li>\n\n          <li class="nav-item nav-course-tools">\n            <h3 class="title"><span class="label">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Tools"))))
            __M_writer(u'</span> <span class="icon fa fa-caret-down ui-toggle-dd" aria-hidden="true"></span></h3>\n            <div class="wrapper wrapper-nav-sub">\n              <div class="nav-sub">\n                <ul>\n                  <li class="nav-item nav-course-tools-import">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(import_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Import"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-tools-export">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(export_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Export"))))
            __M_writer(u'</a>\n                  </li>\n')
            if settings.FEATURES.get('ENABLE_EXPORT_GIT') and context_course.giturl:
                __M_writer(u'                  <li class="nav-item nav-course-tools-export-git">\n                    <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(reverse('export_git', kwargs=dict(course_key_string=unicode(course_key))))))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Export to Git"))))
                __M_writer(u'</a>\n                  </li>\n')
            __M_writer(u'                </ul>\n              </div>\n            </div>\n          </li>\n        </ol>\n      </nav>\n')
        elif context_library:
            __M_writer(u'       ')

            library_key = context_library.location.course_key
            index_url = reverse('contentstore.views.library_handler', kwargs={'library_key_string': unicode(library_key)})
            import_url = reverse('contentstore.views.import_handler', kwargs={'course_key_string': unicode(library_key)})
            lib_users_url = reverse('contentstore.views.manage_library_users', kwargs={'library_key_string': unicode(library_key)})
            export_url = reverse('contentstore.views.export_handler', kwargs={'course_key_string': unicode(library_key)})
                  
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['library_key','import_url','lib_users_url','export_url','index_url'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n      <h2 class="info-course">\n        <span class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Current Library:"))))
            __M_writer(u'</span>\n        <a class="course-link" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(index_url)))
            __M_writer(u'">\n          <span class="course-org">')
            __M_writer(filters.html_escape(filters.decode.utf8(context_library.display_org_with_default)))
            __M_writer(u'</span><span class="course-number">')
            __M_writer(filters.html_escape(filters.decode.utf8(context_library.display_number_with_default)))
            __M_writer(u'</span>\n          <span class="course-title" title="')
            __M_writer(filters.html_escape(filters.decode.utf8(context_library.display_name_with_default)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(context_library.display_name_with_default)))
            __M_writer(u'</span>\n        </a>\n      </h2>\n\n      <nav class="nav-course nav-dd ui-left" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Course'))))
            __M_writer(u'">\n        <h2 class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Course Navigation"))))
            __M_writer(u'</h2>\n        <ol>\n\n          <li class="nav-item nav-library-settings">\n            <h3 class="title"><span class="label"><span class="label-prefix sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Library"))))
            __M_writer(u' </span>')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Settings"))))
            __M_writer(u'</span> <span class="icon fa fa-caret-down ui-toggle-dd" aria-hidden="true"></span></h3>\n            <div class="wrapper wrapper-nav-sub">\n              <div class="nav-sub">\n                <ul>\n                  <li class="nav-item nav-library-settings-team">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(lib_users_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("User Access"))))
            __M_writer(u'</a>\n                  </li>\n                </ul>\n              </div>\n            </div>\n          </li>\n          <li class="nav-item nav-course-tools">\n            <h3 class="title"><span class="label">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Tools"))))
            __M_writer(u'</span> <span class="icon fa fa-caret-down ui-toggle-dd" aria-hidden="true"></span></h3>\n\n            <div class="wrapper wrapper-nav-sub">\n              <div class="nav-sub">\n                <ul>\n                  <li class="nav-item nav-course-tools-import">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(import_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Import"))))
            __M_writer(u'</a>\n                  </li>\n                  <li class="nav-item nav-course-tools-export">\n                    <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(export_url)))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Export"))))
            __M_writer(u'</a>\n                  </li>\n                </ul>\n              </div>\n            </div>\n          </li>\n        </ol>\n      </nav>\n')
        elif show_programs_header:
            __M_writer(u'      <h2 class="info-course">\n        <span class="course-org">')
            __M_writer(filters.html_escape(filters.decode.utf8(settings.PLATFORM_NAME)))
            __M_writer(u'</span><span class="course-number">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Programs"))))
            __M_writer(u'</span>\n        <span class="course-title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Program Administration"))))
            __M_writer(u'</span>\n      </h2>\n')
        __M_writer(u'    </div>\n\n    <div class="wrapper wrapper-r">\n')
        if static.show_language_selector():
            __M_writer(u'        ')
            languages = static.get_released_languages() 
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['languages'] if __M_key in __M_locals_builtin_stored]))
            __M_writer(u'\n')
            if len(languages) > 1:
                __M_writer(u'        <nav class="user-language-selector" aria-label="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Language preference'))))
                __M_writer(u'">\n          <form action="/i18n/setlang/" method="post" class="settings-language-form" id="language-settings-form">\n              <input type="hidden" id="csrf_token" name="csrfmiddlewaretoken" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(csrf_token)))
                __M_writer(u'">\n')
                if user.is_authenticated():
                    __M_writer(u'              <input title="preference api" type="hidden" id="preference-api-url" class="url-endpoint" value="')
                    __M_writer(filters.html_escape(filters.decode.utf8(reverse('preferences_api', kwargs={'username': user.username}))))
                    __M_writer(u'" data-user-is-authenticated="true">\n')
                else:
                    __M_writer(u'              <input title="session update url" type="hidden" id="update-session-url" class="url-endpoint" value="')
                    __M_writer(filters.html_escape(filters.decode.utf8(reverse('session_language'))))
                    __M_writer(u'" data-user-is-authenticated="false">\n')
                __M_writer(u'              <label><span class="sr">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Choose Language"))))
                __M_writer(u'</span>\n              <select class="input select language-selector" id="settings-language-value" name="language">\n')
                for language in languages:
                    if language[0] == LANGUAGE_CODE:
                        __M_writer(u'                 <option value="')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[0])))
                        __M_writer(u'" selected="selected">')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[1])))
                        __M_writer(u'</option>\n')
                    else:
                        __M_writer(u'                 <option value="')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[0])))
                        __M_writer(u'" >')
                        __M_writer(filters.html_escape(filters.decode.utf8(language[1])))
                        __M_writer(u'</option>\n')
                __M_writer(u'              </select>\n              </label>\n          </form>\n        </nav>\n')
        if user.is_authenticated():
            __M_writer(u'      <nav class="nav-account nav-is-signedin nav-dd ui-right" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Account'))))
            __M_writer(u'">\n        <h2 class="sr-only">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Account Navigation"))))
            __M_writer(u'</h2>\n        <ol>\n          <li class="nav-item nav-account-help">\n            <h3 class="title"><span class="label"><a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(get_online_help_info(online_help_token)['doc_url'])))
            __M_writer(u'" title="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Contextual Online Help'))))
            __M_writer(u'" target="_blank">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Help"))))
            __M_writer(u'</a></span></h3>\n          </li>\n          <li class="nav-item nav-account-user">\n            ')
            runtime._include_file(context, u'user_dropdown.html', _template_uri, online_help_token=online_help_token)
            __M_writer(u'\n          </li>\n        </ol>\n      </nav>\n\n')
        else:
            __M_writer(u'      <nav class="nav-not-signedin nav-pitch" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Account'))))
            __M_writer(u'">\n        <h2 class="sr-only">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Account Navigation"))))
            __M_writer(u'</h2>\n        <ol>\n          <li class="nav-item nav-not-signedin-help">\n            <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(get_online_help_info(online_help_token)['doc_url'])))
            __M_writer(u'" title="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Contextual Online Help'))))
            __M_writer(u'" target="_blank">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Help"))))
            __M_writer(u'</a>\n          </li>\n          <li class="nav-item nav-not-signedin-signup">\n            <a class="action action-signup" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('signup'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Sign Up"))))
            __M_writer(u'</a>\n          </li>\n          <li class="nav-item nav-not-signedin-signin">\n            <a class="action action-signin" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('login'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Sign In"))))
            __M_writer(u'</a>\n          </li>\n        </ol>\n      </nav>\n')
        __M_writer(u'    </div>\n  </header>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 3, "30": 2, "33": 1, "49": 1, "50": 2, "51": 8, "52": 15, "53": 15, "54": 15, "55": 15, "56": 19, "57": 20, "58": 20, "79": 37, "80": 39, "81": 39, "82": 40, "83": 40, "84": 41, "85": 41, "86": 41, "87": 41, "88": 42, "89": 42, "90": 42, "91": 42, "92": 46, "93": 46, "94": 47, "95": 47, "96": 50, "97": 50, "98": 50, "99": 50, "100": 56, "101": 56, "102": 56, "103": 56, "104": 59, "105": 59, "106": 59, "107": 59, "108": 62, "109": 62, "110": 62, "111": 62, "112": 65, "113": 65, "114": 65, "115": 65, "116": 68, "117": 68, "118": 68, "119": 68, "120": 70, "121": 71, "122": 72, "123": 72, "124": 72, "125": 72, "126": 75, "127": 81, "128": 81, "129": 81, "130": 81, "131": 87, "132": 87, "133": 87, "134": 87, "135": 90, "136": 90, "137": 90, "138": 90, "139": 93, "140": 93, "141": 93, "142": 93, "143": 96, "144": 96, "145": 96, "146": 96, "147": 99, "148": 99, "149": 99, "150": 99, "151": 101, "152": 102, "153": 103, "154": 103, "155": 103, "156": 103, "157": 106, "158": 112, "159": 112, "160": 117, "161": 117, "162": 117, "163": 117, "164": 120, "165": 120, "166": 120, "167": 120, "168": 122, "169": 123, "170": 124, "171": 124, "172": 124, "173": 124, "174": 127, "175": 133, "176": 134, "177": 134, "187": 140, "188": 142, "189": 142, "190": 143, "191": 143, "192": 144, "193": 144, "194": 144, "195": 144, "196": 145, "197": 145, "198": 145, "199": 145, "200": 149, "201": 149, "202": 150, "203": 150, "204": 154, "205": 154, "206": 154, "207": 154, "208": 159, "209": 159, "210": 159, "211": 159, "212": 166, "213": 166, "214": 172, "215": 172, "216": 172, "217": 172, "218": 175, "219": 175, "220": 175, "221": 175, "222": 183, "223": 184, "224": 185, "225": 185, "226": 185, "227": 185, "228": 186, "229": 186, "230": 189, "231": 192, "232": 193, "233": 193, "237": 193, "238": 194, "239": 195, "240": 195, "241": 195, "242": 197, "243": 197, "244": 198, "245": 199, "246": 199, "247": 199, "248": 200, "249": 201, "250": 201, "251": 201, "252": 203, "253": 203, "254": 203, "255": 205, "256": 206, "257": 207, "258": 207, "259": 207, "260": 207, "261": 207, "262": 208, "263": 209, "264": 209, "265": 209, "266": 209, "267": 209, "268": 212, "269": 218, "270": 219, "271": 219, "272": 219, "273": 220, "274": 220, "275": 223, "276": 223, "277": 223, "278": 223, "279": 223, "280": 223, "281": 226, "282": 226, "283": 231, "284": 232, "285": 232, "286": 232, "287": 233, "288": 233, "289": 236, "290": 236, "291": 236, "292": 236, "293": 236, "294": 236, "295": 239, "296": 239, "297": 239, "298": 239, "299": 242, "300": 242, "301": 242, "302": 242, "303": 247, "309": 303}, "uri": "widgets/header.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/header.html"}
__M_END_METADATA
"""
