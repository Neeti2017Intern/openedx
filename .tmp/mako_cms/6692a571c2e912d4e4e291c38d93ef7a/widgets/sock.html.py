# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849144.954905
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/sock.html'
_template_uri = u'widgets/sock.html'
_source_encoding = 'utf-8'
_exports = []



from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse



from django.conf import settings

partner_email = settings.PARTNER_SUPPORT_EMAIL

links = [{
    'href': 'http://docs.edx.org',
    'sr_mouseover_text': _('Access documentation on http://docs.edx.org'),
    'text': _('edX Documentation'),
    'condition': True
}, {
    'href': 'https://open.edx.org',
    'sr_mouseover_text': _('Access the Open edX Portal'),
    'text': _('Open edX Portal'),
    'condition': True
}, {
    'href': 'https://www.edx.org/course/overview-creating-edx-course-edx-edx101#.VO4eaLPF-n1',
    'sr_mouseover_text': _('Enroll in edX101: Overview of Creating an edX Course'),
    'text': _('Enroll in edX101'),
    'condition': True
}, {
    'href': 'https://www.edx.org/course/creating-course-edx-studio-edx-studiox',
    'sr_mouseover_text': _('Enroll in StudioX: Creating a Course with edX Studio'),
    'text': _('Enroll in StudioX'),
    'condition': True
}, {
    'href': 'mailto:{email}'.format(email=partner_email),
    'sr_mouseover_text': _('Send an email to {email}').format(email=partner_email),
    'text': _('Contact Us'),
    'condition': bool(partner_email)
}]


def render_body(context,online_help_token,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs,online_help_token=online_help_token)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n<div class="wrapper-sock wrapper">\n  <ul class="list-actions list-cta">\n    <li class="action-item">\n      <a href="#sock" class="cta cta-show-sock"><span class="icon fa fa-question-circle" aria-hidden="true"></span>\n        <span class="copy-show is-shown">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Looking for help with {studio_name}?").format(studio_name=settings.STUDIO_SHORT_NAME))))
        __M_writer(u'</span>\n        <span class="copy-hide is-hidden">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("Hide {studio_name} Help").format(studio_name=settings.STUDIO_SHORT_NAME))))
        __M_writer(u'</span>\n      </a>\n    </li>\n  </ul>\n\n  <div class="wrapper-inner wrapper">\n    <section class="sock" id="sock" aria-labelledby="sock-heading">\n        <h2 id="sock-heading" class="title sr-only">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("{studio_name} Documentation").format(studio_name=settings.STUDIO_NAME))))
        __M_writer(u'</h2>\n      <div class="support">\n        ')
        __M_writer(u'\n\n        <ul class="list-actions">\n')
        for link in links:
            if link['condition']:
                __M_writer(u'              <li class="action-item">\n                <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(link['href'])))
                __M_writer(u'" title="')
                __M_writer(filters.html_escape(filters.decode.utf8(link['sr_mouseover_text'])))
                __M_writer(u'" rel="external" class="action action-primary">')
                __M_writer(filters.html_escape(filters.decode.utf8(link['text'])))
                __M_writer(u'</a>\n                <span class="tip">')
                __M_writer(filters.html_escape(filters.decode.utf8(link['sr_mouseover_text'])))
                __M_writer(u'</span>\n              </li>\n')
        __M_writer(u'        </ul>\n      </div>\n    </section>\n  </div>\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "21": 20, "54": 1, "59": 1, "60": 5, "61": 10, "62": 10, "63": 11, "64": 11, "65": 18, "66": 18, "67": 51, "68": 54, "69": 55, "70": 56, "71": 57, "72": 57, "73": 57, "74": 57, "75": 57, "76": 57, "77": 58, "78": 58, "79": 62, "85": 79}, "uri": "widgets/sock.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/sock.html"}
__M_END_METADATA
"""
