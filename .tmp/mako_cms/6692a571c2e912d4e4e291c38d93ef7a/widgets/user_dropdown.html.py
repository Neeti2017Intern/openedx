# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849144.922503
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/user_dropdown.html'
_template_uri = u'widgets/user_dropdown.html'
_source_encoding = 'utf-8'
_exports = [u'navigation_dropdown_menu_links']



from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from student.roles import GlobalStaff


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    ns = runtime.TemplateNamespace(u'static', context._clean_inheritance_tokens(), templateuri=u'../static_content.html', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, u'static')] = ns

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def navigation_dropdown_menu_links():
            return render_navigation_dropdown_menu_links(context._locals(__M_locals))
        uses_pattern_library = context.get('uses_pattern_library', UNDEFINED)
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        if uses_pattern_library:
            __M_writer(u'  <div class="wrapper-user-menu dropdown-menu-container logged-in js-header-user-menu">\n      <h3 class="title menu-title">\n          <span class="sr-only">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Currently signed in as:"))))
            __M_writer(u'</span>\n          <span class="account-username" title="')
            __M_writer(filters.html_escape(filters.decode.utf8( user.username )))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8( user.username )))
            __M_writer(u'</span>\n      </h3>\n      <button type="button" class="menu-button button-more has-dropdown js-dropdown-button" aria-haspopup="true" aria-expanded="false" aria-controls="')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Usermenu"))))
            __M_writer(u'">\n          <span class="icon-fallback icon-fallback-img">\n            <span class="icon icon-angle-down" aria-hidden="true"></span>\n            <span class="sr-only">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Usermenu dropdown"))))
            __M_writer(u'</span>\n          </span>\n      </button>\n      <ul class="dropdown-menu list-divided is-hidden" id="')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Usermenu"))))
            __M_writer(u'" tabindex="-1">\n          ')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'navigation_dropdown_menu_links'):
                context['self'].navigation_dropdown_menu_links(**pageargs)
            

            __M_writer(u'\n          <li class="dropdown-item item has-block-link">\n            <a class="action action-signout" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('logout'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Sign Out"))))
            __M_writer(u'</a>\n          </li>\n      </ul>\n  </div>\n\n')
        else:
            __M_writer(u'  <h3 class="title">\n    <span class="label">\n      <span class="label-prefix sr-only">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Currently signed in as:"))))
            __M_writer(u'</span>\n      <span class="account-username" title="')
            __M_writer(filters.html_escape(filters.decode.utf8( user.username )))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8( user.username )))
            __M_writer(u'</span>\n    </span>\n    <span class="icon fa fa-caret-down ui-toggle-dd" aria-hidden="true"></span>\n  </h3>\n\n  <div class="wrapper wrapper-nav-sub">\n    <div class="nav-sub">\n      <ul>\n        <li class="nav-item nav-account-dashboard">\n          <a href="/">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("{studio_name} Home").format(studio_name=settings.STUDIO_SHORT_NAME))))
            __M_writer(u'</a>\n        </li>\n')
            if GlobalStaff().has_user(user):
                __M_writer(u'        <li class="nav-item">\n          <a href="')
                __M_writer(filters.html_escape(filters.decode.utf8(reverse('maintenance:maintenance_index'))))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Maintenance"))))
                __M_writer(u'</a>\n        </li>\n')
            __M_writer(u'        <li class="nav-item nav-account-signout">\n          <a class="action action-signout" href="')
            __M_writer(filters.html_escape(filters.decode.utf8(reverse('logout'))))
            __M_writer(u'">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Sign Out"))))
            __M_writer(u'</a>\n        </li>\n      </ul>\n    </div>\n  </div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_navigation_dropdown_menu_links(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def navigation_dropdown_menu_links():
            return render_navigation_dropdown_menu_links(context)
        __M_writer = context.writer()
        __M_writer(u'\n              <li class="dropdown-item item has-block-link">\n                <a href="/">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("{studio_name} Home").format(studio_name=settings.STUDIO_SHORT_NAME))))
        __M_writer(u'</a>\n              </li>\n          ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 3, "30": 2, "33": 1, "42": 1, "43": 2, "44": 8, "45": 10, "46": 11, "47": 13, "48": 13, "49": 14, "50": 14, "51": 14, "52": 14, "53": 16, "54": 16, "55": 19, "56": 19, "57": 22, "58": 22, "63": 27, "64": 29, "65": 29, "66": 29, "67": 29, "68": 34, "69": 35, "70": 37, "71": 37, "72": 38, "73": 38, "74": 38, "75": 38, "76": 47, "77": 47, "78": 49, "79": 50, "80": 51, "81": 51, "82": 51, "83": 51, "84": 54, "85": 55, "86": 55, "87": 55, "88": 55, "94": 23, "100": 23, "101": 25, "102": 25, "108": 102}, "uri": "widgets/user_dropdown.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/widgets/user_dropdown.html"}
__M_END_METADATA
"""
