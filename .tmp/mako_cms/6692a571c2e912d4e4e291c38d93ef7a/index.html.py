# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1497849144.192962
_enable_loop = True
_template_filename = u'/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/index.html'
_template_uri = 'index.html'
_source_encoding = 'utf-8'
_exports = [u'content', u'requirejs', u'bodyclass', 'online_help_token', u'title']



from django.utils.translation import ugettext as _

from openedx.core.djangolib.markup import HTML, Text


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'base.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        rerun_creator_status = context.get('rerun_creator_status', UNDEFINED)
        def online_help_token():
            return render_online_help_token(context._locals(__M_locals))
        get_online_help_info = context.get('get_online_help_info', UNDEFINED)
        program_authoring_url = context.get('program_authoring_url', UNDEFINED)
        libraries = context.get('libraries', UNDEFINED)
        def bodyclass():
            return render_bodyclass(context._locals(__M_locals))
        allow_course_reruns = context.get('allow_course_reruns', UNDEFINED)
        def title():
            return render_title(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        course_creator_status = context.get('course_creator_status', UNDEFINED)
        libraries_enabled = context.get('libraries_enabled', UNDEFINED)
        is_programs_enabled = context.get('is_programs_enabled', UNDEFINED)
        len = context.get('len', UNDEFINED)
        in_process_course_actions = context.get('in_process_course_actions', UNDEFINED)
        user = context.get('user', UNDEFINED)
        sorted = context.get('sorted', UNDEFINED)
        allow_unicode_course_id = context.get('allow_unicode_course_id', UNDEFINED)
        programs = context.get('programs', UNDEFINED)
        show_new_library_button = context.get('show_new_library_button', UNDEFINED)
        request_course_creator_url = context.get('request_course_creator_url', UNDEFINED)
        courses = context.get('courses', UNDEFINED)
        str = context.get('str', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        def requirejs():
            return render_requirejs(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer(u'\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n\n')
        __M_writer(u'\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'title'):
            context['self'].title(**pageargs)
        

        __M_writer(u'\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'bodyclass'):
            context['self'].bodyclass(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'requirejs'):
            context['self'].requirejs(**pageargs)
        

        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        request_course_creator_url = context.get('request_course_creator_url', UNDEFINED)
        str = context.get('str', UNDEFINED)
        allow_course_reruns = context.get('allow_course_reruns', UNDEFINED)
        settings = context.get('settings', UNDEFINED)
        rerun_creator_status = context.get('rerun_creator_status', UNDEFINED)
        is_programs_enabled = context.get('is_programs_enabled', UNDEFINED)
        programs = context.get('programs', UNDEFINED)
        len = context.get('len', UNDEFINED)
        libraries = context.get('libraries', UNDEFINED)
        courses = context.get('courses', UNDEFINED)
        get_online_help_info = context.get('get_online_help_info', UNDEFINED)
        in_process_course_actions = context.get('in_process_course_actions', UNDEFINED)
        user = context.get('user', UNDEFINED)
        course_creator_status = context.get('course_creator_status', UNDEFINED)
        sorted = context.get('sorted', UNDEFINED)
        show_new_library_button = context.get('show_new_library_button', UNDEFINED)
        program_authoring_url = context.get('program_authoring_url', UNDEFINED)
        def content():
            return render_content(context)
        allow_unicode_course_id = context.get('allow_unicode_course_id', UNDEFINED)
        libraries_enabled = context.get('libraries_enabled', UNDEFINED)
        def online_help_token():
            return render_online_help_token(context)
        __M_writer = context.writer()
        __M_writer(u'\n<div class="wrapper-mast wrapper">\n  <header class="mast has-actions">\n    <h1 class="page-header">')
        __M_writer(filters.html_escape(filters.decode.utf8(_("{studio_name} Home").format(studio_name=settings.STUDIO_SHORT_NAME))))
        __M_writer(u'</h1>\n\n')
        if user.is_active:
            __M_writer(u'    <nav class="nav-actions" aria-label="')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Page Actions'))))
            __M_writer(u'">\n      <h3 class="sr">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Page Actions"))))
            __M_writer(u'</h3>\n      <ul>\n        <li class="nav-item">\n')
            if course_creator_status=='granted':
                __M_writer(u'          <a href="#" class="button new-button new-course-button"><span class="icon fa fa-plus icon-inline" aria-hidden="true"></span>\n              ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("New Course"))))
                __M_writer(u'</a>\n')
            elif course_creator_status=='disallowed_for_this_site' and settings.FEATURES.get('STUDIO_REQUEST_EMAIL',''):
                __M_writer(u'          <a href="mailto:')
                __M_writer(filters.html_escape(filters.decode.utf8(settings.FEATURES.get('STUDIO_REQUEST_EMAIL',''))))
                __M_writer(u'">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Email staff to create course"))))
                __M_writer(u'</a>\n')
            __M_writer(u'\n')
            if show_new_library_button:
                __M_writer(u'            <a href="#" class="button new-button new-library-button"><span class="icon fa fa-plus icon-inline" aria-hidden="true"></span>\n            ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("New Library"))))
                __M_writer(u'</a>\n')
            __M_writer(u'\n')
            if is_programs_enabled:
                __M_writer(u'            <a href=')
                __M_writer(filters.html_escape(filters.decode.utf8(program_authoring_url + 'new')))
                __M_writer(u' class="button new-button new-program-button"><span class="icon fa fa-plus icon-inline" aria-hidden="true"></span>\n            ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("New Program"))))
                __M_writer(u'</a>\n')
            __M_writer(u'        </li>\n      </ul>\n    </nav>\n')
        __M_writer(u'  </header>\n</div>\n\n<div class="wrapper-content wrapper">\n')
        if user.is_active:
            __M_writer(u'  <section class="content">\n    <article class="content-primary" role="main">\n\n')
            if course_creator_status=='granted':
                __M_writer(u'      <div class="wrapper-create-element wrapper-create-course">\n        <form class="form-create create-course course-info" id="create-course-form" name="create-course-form">\n          <div class="wrap-error">\n            <div id="course_creation_error" name="course_creation_error" class="message message-status message-status error" role="alert">\n            <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Please correct the highlighted fields below."))))
                __M_writer(u'</p>\n            </div>\n          </div>\n\n          <div class="wrapper-form">\n            <h3 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Create a New Course"))))
                __M_writer(u'</h3>\n\n            <fieldset>\n              <legend class="sr">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Required Information to Create a New Course"))))
                __M_writer(u'</legend>\n\n              <ol class="list-input">\n                <li class="field text required" id="field-course-name">\n                  <label for="new-course-name">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Course Name"))))
                __M_writer(u'</label>\n')
                __M_writer(u'                  <input class="new-course-name" id="new-course-name" type="text" name="new-course-name" required placeholder="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('e.g. Introduction to Computer Science'))))
                __M_writer(u'" aria-describedby="tip-new-course-name tip-error-new-course-name" />\n                  <span class="tip" id="tip-new-course-name">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("The public display name for your course. This cannot be changed, but you can set a different display name in Advanced Settings later."))))
                __M_writer(u'</span>\n                  <span class="tip tip-error is-hiding" id="tip-error-new-course-name"></span>\n                </li>\n                <li class="field text required" id="field-organization">\n                  <label for="new-course-org">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Organization"))))
                __M_writer(u'</label>\n')
                __M_writer(u'                  <input class="new-course-org" id="new-course-org" type="text" name="new-course-org" required placeholder="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('e.g. UniversityX or OrganizationX'))))
                __M_writer(u'" aria-describedby="tip-new-course-org tip-error-new-course-org" />\n                  <span class="tip" id="tip-new-course-org">')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_("The name of the organization sponsoring the course. {strong_start}Note: The organization name is part of the course URL.{strong_end} This cannot be changed, but you can set a different display name in Advanced Settings later.")).format(
                      strong_start=HTML('<strong>'),
                      strong_end=HTML('</strong>'),
                  ))))
                __M_writer(u'</span>\n                  <span class="tip tip-error is-hiding" id="tip-error-new-course-org"></span>\n                </li>\n\n                <li class="field text required" id="field-course-number">\n                  <label for="new-course-number">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Course Number"))))
                __M_writer(u'</label>\n')
                __M_writer(u'                  <input class="new-course-number" id="new-course-number" type="text" name="new-course-number" required placeholder="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('e.g. CS101'))))
                __M_writer(u'" aria-describedby="tip-new-course-number tip-error-new-course-number" />\n                  <span class="tip" id="tip-new-course-number">')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_("The unique number that identifies your course within your organization. {strong_start}Note: This is part of your course URL, so no spaces or special characters are allowed and it cannot be changed.{strong_end}")).format(
                      strong_start=HTML('<strong>'),
                      strong_end=HTML('</strong>'),
                  ))))
                __M_writer(u'</span>\n                  <span class="tip tip-error is-hiding" id="tip-error-new-course-number"></span>\n                </li>\n\n                <li class="field text required" id="field-course-run">\n                  <label for="new-course-run">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Course Run"))))
                __M_writer(u'</label>\n')
                __M_writer(u'                  <input class="new-course-run" id="new-course-run" type="text" name="new-course-run" required placeholder="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('e.g. 2014_T1'))))
                __M_writer(u'" aria-describedby="tip-new-course-run tip-error-new-course-run" />\n                  <span class="tip" id="tip-new-course-run">')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_("The term in which your course will run. {strong_start}Note: This is part of your course URL, so no spaces or special characters are allowed and it cannot be changed.{strong_end}")).format(
                      strong_start=HTML('<strong>'),
                      strong_end=HTML('</strong>'),
                  ))))
                __M_writer(u'</span>\n                  <span class="tip tip-error is-hiding" id="tip-error-new-course-run"></span>\n                </li>\n              </ol>\n\n            </fieldset>\n          </div>\n\n          <div class="actions">\n            <input type="hidden" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(allow_unicode_course_id)))
                __M_writer(u'" class="allow-unicode-course-id" />\n            <input type="submit" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Create'))))
                __M_writer(u'" class="action action-primary new-course-save" />\n            <input type="button" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Cancel'))))
                __M_writer(u'" class="action action-secondary action-cancel new-course-cancel" />\n          </div>\n        </form>\n      </div>\n\n')
            __M_writer(u'\n')
            if libraries_enabled and show_new_library_button:
                __M_writer(u'      <div class="wrapper-create-element wrapper-create-library">\n        <form class="form-create create-library library-info" id="create-library-form" name="create-library-form">\n          <div class="wrap-error">\n            <div id="library_creation_error" name="library_creation_error" class="message message-status message-status error" role="alert">\n            <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Please correct the highlighted fields below."))))
                __M_writer(u'</p>\n            </div>\n          </div>\n\n          <div class="wrapper-form">\n            <h3 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Create a New Library"))))
                __M_writer(u'</h3>\n\n            <fieldset>\n              <legend class="sr">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Required Information to Create a New Library"))))
                __M_writer(u'</legend>\n\n              <ol class="list-input">\n                <li class="field text required" id="field-library-name">\n                  <label for="new-library-name">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Library Name"))))
                __M_writer(u'</label>\n')
                __M_writer(u'                  <input class="new-library-name" id="new-library-name" type="text" name="new-library-name" required placeholder="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('e.g. Computer Science Problems'))))
                __M_writer(u'" aria-describedby="tip-new-library-name tip-error-new-library-name" />\n                  <span class="tip" id="tip-new-library-name">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("The public display name for your library."))))
                __M_writer(u'</span>\n                  <span class="tip tip-error is-hiding" id="tip-error-new-library-name"></span>\n                </li>\n                <li class="field text required" id="field-organization">\n                  <label for="new-library-org">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Organization"))))
                __M_writer(u'</label>\n                  <input class="new-library-org" id="new-library-org" type="text" name="new-library-org" required placeholder="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('e.g. UniversityX or OrganizationX'))))
                __M_writer(u'" aria-describedby="tip-new-library-org tip-error-new-library-org" />\n                  <span class="tip" id="tip-new-library-org">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("The public organization name for your library."))))
                __M_writer(u' ')
                __M_writer(filters.html_escape(filters.decode.utf8(_("This cannot be changed."))))
                __M_writer(u'</span>\n                  <span class="tip tip-error is-hiding" id="tip-error-new-library-org"></span>\n                </li>\n\n                <li class="field text required" id="field-library-number">\n                  <label for="new-library-number">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Library Code"))))
                __M_writer(u'</label>\n')
                __M_writer(u'                  <input class="new-library-number" id="new-library-number" type="text" name="new-library-number" required placeholder="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('e.g. CSPROB'))))
                __M_writer(u'" aria-describedby="tip-new-library-number tip-error-new-library-number" />\n                  <span class="tip" id="tip-new-library-number">')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_("The unique code that identifies this library. {strong_start}Note: This is part of your library URL, so no spaces or special characters are allowed.{strong_end} This cannot be changed.")).format(
                      strong_start=HTML('<strong>'),
                      strong_end=HTML('</strong>'),
                  ))))
                __M_writer(u'</span>\n                  <span class="tip tip-error is-hiding" id="tip-error-new-library-number"></span>\n                </li>\n              </ol>\n\n            </fieldset>\n          </div>\n\n          <div class="actions">\n            <input type="hidden" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(allow_unicode_course_id)))
                __M_writer(u'" class="allow-unicode-course-id" />\n            <input type="submit" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Create'))))
                __M_writer(u'" class="action action-primary new-library-save" />\n            <input type="button" value="')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Cancel'))))
                __M_writer(u'" class="action action-secondary action-cancel new-library-cancel" />\n          </div>\n        </form>\n      </div>\n')
            __M_writer(u'\n      <!-- STATE: processing courses -->\n')
            if allow_course_reruns and rerun_creator_status and len(in_process_course_actions) > 0:
                __M_writer(u'      <div class="courses courses-processing">\n          <h3 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Courses Being Processed"))))
                __M_writer(u'</h3>\n\n          <ul class="list-courses">\n')
                for course_info in sorted(in_process_course_actions, key=lambda s: s['display_name'].lower() if s['display_name'] is not None else ''):
                    __M_writer(u'            <!-- STATE: re-run is processing -->\n')
                    if course_info['is_in_progress']:
                        __M_writer(u'            <li class="wrapper-course has-status" data-course-key="')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['course_key'])))
                        __M_writer(u'">\n              <div class="course-item course-rerun is-processing">\n                <div class="course-details" href="#">\n                  <h3 class="course-title">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['display_name'])))
                        __M_writer(u'</h3>\n\n                  <div class="course-metadata">\n                    <span class="course-org metadata-item">\n                      <span class="label">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Organization:"))))
                        __M_writer(u'</span> <span class="value">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['org'])))
                        __M_writer(u'</span>\n                    </span>\n                    <span class="course-num metadata-item">\n                      <span class="label">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Course Number:"))))
                        __M_writer(u'</span>\n                      <span class="value">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['number'])))
                        __M_writer(u'</span>\n                    </span>\n                    <span class="course-run metadata-item">\n                      <span class="label">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Course Run:"))))
                        __M_writer(u'</span> <span class="value">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['run'])))
                        __M_writer(u'</span>\n                    </span>\n                  </div>\n                </div>\n\n                <dl class="course-status">\n                  <dt class="label sr">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("This course run is currently being created."))))
                        __M_writer(u'</dt>\n                  <dd class="value">\n                    <span class="icon fa fa-refresh fa-spin" aria-hidden="true"></span>\n')
                        __M_writer(u'                    <span class="copy">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Configuring as re-run"))))
                        __M_writer(u'</span>\n                  </dd>\n                </dl>\n              </div>\n\n              <div class="status-message">\n                <p class="copy">')
                        __M_writer(filters.html_escape(filters.decode.utf8(Text(_('The new course will be added to your course list in 5-10 minutes. Return to this page or {link_start}refresh it{link_end} to update the course list. The new course will need some manual configuration.')).format(
                    link_start=HTML('<a href="#" class="action-reload">'),
                    link_end=HTML('</a>'),
                  ))))
                        __M_writer(u'</p>\n              </div>\n            </li>\n')
                    __M_writer(u'\n            <!-- - - - -->\n\n            <!-- STATE: re-run has error -->\n')
                    if course_info['is_failed']:
                        __M_writer(u'            <li class="wrapper-course has-status" data-course-key="')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['course_key'])))
                        __M_writer(u'">\n              <div class="course-item course-rerun has-error">\n                <div class="course-details" href="#">\n                  <h3 class="course-title">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['display_name'])))
                        __M_writer(u'</h3>\n\n                  <div class="course-metadata">\n                    <span class="course-org metadata-item">\n                      <span class="label">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Organization:"))))
                        __M_writer(u'</span> <span class="value">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['org'])))
                        __M_writer(u'</span>\n                    </span>\n                    <span class="course-num metadata-item">\n                      <span class="label">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Course Number:"))))
                        __M_writer(u'</span>\n                      <span class="value">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['number'])))
                        __M_writer(u'</span>\n                    </span>\n                    <span class="course-run metadata-item">\n                      <span class="label">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Course Run:"))))
                        __M_writer(u'</span> <span class="value">')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['run'])))
                        __M_writer(u'</span>\n                    </span>\n                  </div>\n                </div>\n\n                <dl class="course-status">\n')
                        __M_writer(u'                  <dt class="label sr">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("This re-run processing status:"))))
                        __M_writer(u'</dt>\n                  <dd class="value">\n                    <span class="icon fa fa-warning" aria-hidden="true"></span>\n                    <span class="copy">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Configuration Error"))))
                        __M_writer(u'</span>\n                  </dd>\n                </dl>\n              </div>\n\n              <div class="status-message has-actions">\n                <p class="copy">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("A system error occurred while your course was being processed. Please go to the original course to try the re-run again, or contact your PM for assistance."))))
                        __M_writer(u'</p>\n\n                <ul  class="status-actions">\n                  <li class="action action-dismiss">\n                    <a href="#" class="button dismiss-button" data-dismiss-link="')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['dismiss_link'])))
                        __M_writer(u'">\n                      <span class="icon fa fa-times-circle" aria-hidden="true"></span>\n                      <span class="button-copy">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Dismiss"))))
                        __M_writer(u'</span>\n                    </a>\n                  </li>\n                </ul>\n              </div>\n            </li>\n')
                __M_writer(u'          </ul>\n      </div>\n')
            __M_writer(u'\n')
            if libraries_enabled or is_programs_enabled:
                __M_writer(u'      <ul id="course-index-tabs">\n        <li class="courses-tab active"><a>')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Courses"))))
                __M_writer(u'</a></li>\n\n')
                if libraries_enabled:
                    __M_writer(u'          <li class="libraries-tab"><a>')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Libraries"))))
                    __M_writer(u'</a></li>\n')
                __M_writer(u'\n')
                if is_programs_enabled:
                    __M_writer(u'          <li class="programs-tab"><a>')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Programs"))))
                    __M_writer(u'</a></li>\n')
                __M_writer(u'      </ul>\n')
            __M_writer(u'\n')
            if len(courses) > 0:
                __M_writer(u'      <div class="courses courses-tab active">\n        <ul class="list-courses">\n')
                for course_info in sorted(courses, key=lambda s: s['display_name'].lower() if s['display_name'] is not None else ''):
                    __M_writer(u'          <li class="course-item" data-course-key="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_info['course_key'])))
                    __M_writer(u'">\n            <a class="course-link" href="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_info['url'])))
                    __M_writer(u'">\n              <h3 class="course-title">')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_info['display_name'])))
                    __M_writer(u'</h3>\n\n              <div class="course-metadata">\n                <span class="course-org metadata-item">\n                  <span class="label">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Organization:"))))
                    __M_writer(u'</span> <span class="value">')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_info['org'])))
                    __M_writer(u'</span>\n                </span>\n                <span class="course-num metadata-item">\n                  <span class="label">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Course Number:"))))
                    __M_writer(u'</span>\n                  <span class="value">')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_info['number'])))
                    __M_writer(u'</span>\n                </span>\n                <span class="course-run metadata-item">\n                  <span class="label">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Course Run:"))))
                    __M_writer(u'</span> <span class="value">')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_info['run'])))
                    __M_writer(u'</span>\n                </span>\n              </div>\n            </a>\n\n            <ul  class="item-actions course-actions">\n')
                    if allow_course_reruns and rerun_creator_status and course_creator_status=='granted':
                        __M_writer(u'              <li class="action action-rerun">\n                <a href="')
                        __M_writer(filters.html_escape(filters.decode.utf8(course_info['rerun_link'])))
                        __M_writer(u'" class="button rerun-button">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Re-run Course"))))
                        __M_writer(u'</a>\n              </li>\n')
                    __M_writer(u'              <li class="action action-view">\n                <a href="')
                    __M_writer(filters.html_escape(filters.decode.utf8(course_info['lms_link'])))
                    __M_writer(u'" rel="external" class="button view-button">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("View Live"))))
                    __M_writer(u'</a>\n              </li>\n            </ul>\n          </li>\n')
                __M_writer(u'        </ul>\n      </div>\n\n')
            else:
                __M_writer(u'      <div class="notice notice-incontext notice-instruction notice-instruction-nocourses list-notices courses-tab active">\n        <div class="notice-item">\n          <div class="msg">\n            <h3 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Are you staff on an existing {studio_name} course?").format(studio_name=settings.STUDIO_SHORT_NAME))))
                __M_writer(u'</h3>\n            <div class="copy">\n              <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_('The course creator must give you access to the course. Contact the course creator or administrator for the course you are helping to author.'))))
                __M_writer(u'</p>\n            </div>\n          </div>\n        </div>\n\n')
                if course_creator_status == "granted":
                    __M_writer(u'        <div class="notice-item has-actions">\n          <div class="msg">\n            <h3 class="title">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Create Your First Course'))))
                    __M_writer(u'</h3>\n            <div class="copy">\n              <p>')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Your new course is just a click away!'))))
                    __M_writer(u'</p>\n            </div>\n          </div>\n\n          <ul class="list-actions">\n            <li class="action-item">\n              <a href="#" class="action-primary action-create action-create-course new-course-button"><span class="icon fa fa-plus icon-inline" aria-hidden="true"></span> ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Create Your First Course'))))
                    __M_writer(u'</a>\n            </li>\n          </ul>\n        </div>\n')
                __M_writer(u'\n      </div>\n')
            __M_writer(u'\n\n')
            if course_creator_status == "unrequested":
                __M_writer(u'      <div class="wrapper wrapper-creationrights">\n        <h3 class="title">\n          <a href="#instruction-creationrights" class="ui-toggle-control show-creationrights"><span class="label">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Becoming a Course Creator in {studio_name}').format(studio_name=settings.STUDIO_SHORT_NAME))))
                __M_writer(u'</span> <span class="icon fa fa-times-circle" aria-hidden="true"></span></a>\n        </h3>\n\n        <div class="notice notice-incontext notice-instruction notice-instruction-creationrights ui-toggle-target" id="instruction-creationrights">\n          <div class="copy">\n            <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_('{studio_name} is a hosted solution for our xConsortium partners and selected guests. Courses for which you are a team member appear above for you to edit, while course creator privileges are granted by {platform_name}. Our team will evaluate your request and provide you feedback within 24 hours during the work week.').format(
              studio_name=settings.STUDIO_NAME, platform_name=settings.PLATFORM_NAME))))
                __M_writer(u'</p>\n          </div>\n\n          <div class="status status-creationrights is-unrequested">\n            <h4 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your Course Creator Request Status:'))))
                __M_writer(u'</h4>\n\n            <form id="request-coursecreator" action="')
                __M_writer(filters.html_escape(filters.decode.utf8(request_course_creator_url)))
                __M_writer(u'" method="post" enctype="multipart/form-data">\n              <div class="form-actions">\n                <button type="submit" id="request-coursecreator-submit" name="request-coursecreator-submit" class="action-primary action-request"><span class="icon fa fa-cog icon-inline fa fa-spin" aria-hidden="true"></span> <span class="label">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Request the Ability to Create Courses'))))
                __M_writer(u'</span></button>\n              </div>\n            </form>\n          </div>\n        </div>\n      </div>\n\n')
            elif course_creator_status == "denied":
                __M_writer(u'      <div class="wrapper wrapper-creationrights is-shown">\n        <h3 class="title">\n          <a href="#instruction-creationrights" class="ui-toggle-control current show-creationrights"><span class="label">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your Course Creator Request Status'))))
                __M_writer(u'</span> <span class="icon fa fa-times-circle" aria-hidden="true"></span></a>\n        </h3>\n\n        <div class="notice notice-incontext notice-instruction notice-instruction-creationrights ui-toggle-target" id="instruction-creationrights">\n          <div class="copy">\n            <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_('{studio_name} is a hosted solution for our xConsortium partners and selected guests. Courses for which you are a team member appear above for you to edit, while course creator privileges are granted by {platform_name}. Our team is has completed evaluating your request.').format(
              studio_name=settings.STUDIO_NAME, platform_name=settings.PLATFORM_NAME,
            ))))
                __M_writer(u'</p>\n          </div>\n\n          <div class="status status-creationrights has-status is-denied">\n            <h4 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your Course Creator Request Status:'))))
                __M_writer(u'</h4>\n\n            <dl class="status-update">\n              <dt class="label">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your Course Creator request is:'))))
                __M_writer(u'</dt>\n              <dd class="value">\n                <span class="status-indicator"></span>\n                <span class="value-formal">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Denied'))))
                __M_writer(u'</span>\n                <span class="value-description">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your request did not meet the criteria/guidelines specified by {platform_name} Staff.').format(platform_name=settings.PLATFORM_NAME))))
                __M_writer(u'</span>\n              </dd>\n            </dl>\n          </div>\n        </div>\n      </div>\n\n')
            elif course_creator_status == "pending":
                __M_writer(u'      <div class="wrapper wrapper-creationrights is-shown">\n        <h3 class="title">\n          <a href="#instruction-creationrights" class="ui-toggle-control current show-creationrights"><span class="label">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your Course Creator Request Status'))))
                __M_writer(u'</span> <span class="icon fa fa-times-circle" aria-hidden="true"></span></a>\n        </h3>\n\n        <div class="notice notice-incontext notice-instruction notice-instruction-creationrights ui-toggle-target" id="instruction-creationrights">\n          <div class="copy">\n            <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_('{studio_name} is a hosted solution for our xConsortium partners and selected guests. Courses for which you are a team member appear above for you to edit, while course creator privileges are granted by {platform_name}. Our team is currently  evaluating your request.').format(
              studio_name=settings.STUDIO_NAME, platform_name=settings.PLATFORM_NAME,
            ))))
                __M_writer(u'</p>\n          </div>\n\n          <div class="status status-creationrights has-status is-pending">\n            <h4 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your Course Creator Request Status:'))))
                __M_writer(u'</h4>\n\n            <dl class="status-update">\n              <dt class="label">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your Course Creator request is:'))))
                __M_writer(u'</dt>\n              <dd class="value">\n                <span class="status-indicator"></span>\n                <span class="value-formal">')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Pending'))))
                __M_writer(u'</span>\n                <span class="value-description">\n                  ')
                __M_writer(filters.html_escape(filters.decode.utf8(_('Your request is currently being reviewed by {platform_name} staff and should be updated shortly.').format(platform_name=settings.PLATFORM_NAME))))
                __M_writer(u'\n                </span>\n              </dd>\n            </dl>\n          </div>\n        </div>\n      </div>\n')
            __M_writer(u'\n')
            if len(libraries) > 0:
                __M_writer(u'      <div class="libraries libraries-tab">\n        <ul class="list-courses">\n')
                for library_info in sorted(libraries, key=lambda s: s['display_name'].lower() if s['display_name'] is not None else ''):
                    __M_writer(u'          <li class="course-item">\n            <a class="library-link" href="')
                    __M_writer(filters.html_escape(filters.decode.utf8(library_info['url'])))
                    __M_writer(u'">\n              <h3 class="course-title">')
                    __M_writer(filters.html_escape(filters.decode.utf8(library_info['display_name'])))
                    __M_writer(u'</h3>\n\n              <div class="course-metadata">\n                <span class="course-org metadata-item">\n                  <span class="label">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Organization:"))))
                    __M_writer(u'</span> <span class="value">')
                    __M_writer(filters.html_escape(filters.decode.utf8(library_info['org'])))
                    __M_writer(u'</span>\n                </span>\n                <span class="course-num metadata-item">\n                  <span class="label">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Course Number:"))))
                    __M_writer(u'</span>\n                  <span class="value">')
                    __M_writer(filters.html_escape(filters.decode.utf8(library_info['number'])))
                    __M_writer(u'</span>\n                </span>\n')
                    if not library_info["can_edit"]:
                        __M_writer(u'                  <span class="extra-metadata">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("(Read-only)"))))
                        __M_writer(u'</span>\n')
                    __M_writer(u'              </div>\n            </a>\n          </li>\n')
                __M_writer(u'        </ul>\n      </div>\n\n')
            else:
                __M_writer(u'      <div class="notice notice-incontext notice-instruction notice-instruction-nocourses list-notices libraries-tab">\n        <div class="notice-item">\n          <div class="msg">\n              <h3 class="title">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Were you expecting to see a particular library here?"))))
                __M_writer(u'</h3>\n              <div class="copy">\n                  <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_('The library creator must give you access to the library. Contact the library creator or administrator for the library you are helping to author.'))))
                __M_writer(u'</p>\n              </div>\n          </div>\n        </div>\n')
                if show_new_library_button:
                    __M_writer(u'        <div class="notice-item has-actions">\n          <div class="msg">\n              <h3 class="title">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Create Your First Library'))))
                    __M_writer(u'</h3>\n              <div class="copy">\n                  <p>')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Libraries hold a pool of components that can be re-used across multiple courses. Create your first library with the click of a button!'))))
                    __M_writer(u'</p>\n              </div>\n          </div>\n\n        <ul class="list-actions">\n          <li class="action-item">\n              <a href="#" class="action-primary action-create new-button action-create-library new-library-button"><span class="icon fa fa-plus icon-inline" aria-hidden="true"></span> ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Create Your First Library'))))
                    __M_writer(u'</a>\n          </li>\n        </ul>\n        </div>\n')
                __M_writer(u'      </div>\n')
            __M_writer(u'\n')
            if is_programs_enabled:
                if len(programs) > 0:
                    __M_writer(u'          <div class="programs programs-tab">\n            <!-- Classes related to courses are intentionally reused here, to duplicate the styling used for course listing. -->\n            <ul class="list-courses">\n')
                    for program in programs:
                        __M_writer(u'              <li class="course-item">\n\n                <a class="program-link" href=')
                        __M_writer(filters.html_escape(filters.decode.utf8(program_authoring_url + str(program['id']))))
                        __M_writer(u'>\n                  <h3 class="course-title">')
                        __M_writer(filters.html_escape(filters.decode.utf8(program['name'])))
                        __M_writer(u'</h3>\n\n                  <div class="course-metadata">\n                    <span class="course-org metadata-item">\n                      <!-- As of this writing, programs can only be owned by one organization. If that constraint is relaxed, this will need to be revisited. -->\n                      <span class="label">')
                        __M_writer(filters.html_escape(filters.decode.utf8(_("Organization:"))))
                        __M_writer(u'</span> <span class="value">')
                        __M_writer(filters.html_escape(filters.decode.utf8(program['organizations'][0]['key'])))
                        __M_writer(u'</span>\n                    </span>\n                  </div>\n                </a>\n\n              </li>\n')
                    __M_writer(u'            </ul>\n          </div>\n\n')
                else:
                    __M_writer(u'          <div class="notice notice-incontext notice-instruction notice-instruction-nocourses list-notices programs-tab">\n            <div class="notice-item has-actions">\n\n              <div class="msg">\n                <h3 class="title">')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("You haven't created any programs yet."))))
                    __M_writer(u'</h3>\n                <div class="copy">\n                  <p>')
                    __M_writer(filters.html_escape(filters.decode.utf8(_("Programs are groups of courses related to a common subject."))))
                    __M_writer(u'</p>\n                </div>\n              </div>\n\n              <ul class="list-actions">\n                <li class="action-item">\n                  <a href=')
                    __M_writer(filters.html_escape(filters.decode.utf8(program_authoring_url + 'new')))
                    __M_writer(u' class="action-primary action-create new-button action-create-program new-program-button"><span class="icon fa fa-plus icon-inline" aria-hidden="true"></span> ')
                    __M_writer(filters.html_escape(filters.decode.utf8(_('Create Your First Program'))))
                    __M_writer(u'</a>\n                </li>\n              </ul>\n\n            </div>\n          </div>\n')
            __M_writer(u'\n    </article>\n    <aside class="content-supplementary" role="complementary">\n      <div class="bit">\n        <h3 class="title title-3">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('New to {studio_name}?').format(studio_name=settings.STUDIO_NAME))))
            __M_writer(u'</h3>\n        <p>')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Click Help in the upper-right corner to get more information about the {studio_name} page you are viewing. You can also use the links at the bottom of the page to access our continually updated documentation and other {studio_name} resources.').format(studio_name=settings.STUDIO_SHORT_NAME))))
            __M_writer(u'</p>\n\n        <ol class="list-actions">\n          <li class="action-item">\n\n            <a href="')
            __M_writer(filters.html_escape(filters.decode.utf8(get_online_help_info(online_help_token())['doc_url'])))
            __M_writer(u'" target="_blank">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Getting Started with {studio_name}").format(studio_name=settings.STUDIO_NAME))))
            __M_writer(u'</a>\n          </li>\n        </ol>\n      </div>\n\n')
            if course_creator_status=='disallowed_for_this_site' and settings.FEATURES.get('STUDIO_REQUEST_EMAIL',''):
                __M_writer(u'      <div class="bit">\n        <h3 class="title title-3">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Can I create courses in {studio_name}?").format(studio_name=settings.STUDIO_NAME))))
                __M_writer(u'</h3>\n        <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_("In order to create courses in {studio_name}, you must {link_start}contact {platform_name} staff to help you create a course{link_end}.")).format(
            studio_name=settings.STUDIO_NAME,
            platform_name=settings.PLATFORM_NAME,
            link_start=HTML('<a href="mailto:{email}">').format(email=settings.FEATURES.get('STUDIO_REQUEST_EMAIL','')),
            link_end=HTML("</a>"),
          ))))
                __M_writer(u'</p>\n      </div>\n')
            __M_writer(u'\n')
            if course_creator_status == "unrequested":
                __M_writer(u'      <div class="bit">\n        <h3 class="title title-3">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Can I create courses in {studio_name}?").format(studio_name=settings.STUDIO_NAME))))
                __M_writer(u'</h3>\n        <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(_('In order to create courses in {studio_name}, you must have course creator privileges to create your own course.').format(studio_name=settings.STUDIO_NAME))))
                __M_writer(u'</p>\n      </div>\n\n')
            elif course_creator_status == "denied":
                __M_writer(u'      <div class="bit">\n        <h3 class="title title-3">')
                __M_writer(filters.html_escape(filters.decode.utf8(_("Can I create courses in {studio_name}?").format(studio_name=settings.STUDIO_NAME))))
                __M_writer(u'</h3>\n        <p>')
                __M_writer(filters.html_escape(filters.decode.utf8(Text(_("Your request to author courses in {studio_name} has been denied. Please {link_start}contact {platform_name} Staff with further questions{link_end}.")).format(
            studio_name=settings.STUDIO_NAME,
            platform_name=settings.PLATFORM_NAME,
            link_start=HTML('<a href="mailto:{email}">').format(email=settings.TECH_SUPPORT_EMAIL),
            link_end=HTML('</a>'),
          ))))
                __M_writer(u'</p>\n      </div>\n\n')
            __M_writer(u'    </aside>\n  </section>\n\n\n')
        else:
            __M_writer(u'  <section class="content">\n    <article class="content-primary" role="main">\n      <div class="introduction">\n        <h2 class="title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("Thanks for signing up, {name}!").format(name=user.username))))
            __M_writer(u'</h2>\n      </div>\n\n      <div class="notice notice-incontext notice-instruction notice-instruction-verification">\n        <div class="msg">\n          <h3 class="title">')
            __M_writer(filters.html_escape(filters.decode.utf8(_("We need to verify your email address"))))
            __M_writer(u'</h3>\n          <div class="copy">\n            <p>')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Almost there! In order to complete your sign up we need you to verify your email address ({email}). An activation message and next steps should be waiting for you there.').format(email=user.email))))
            __M_writer(u'</p>\n          </div>\n        </div>\n      </div>\n    </article>\n\n    <aside class="content-supplementary" role="complementary">\n      <div class="bit">\n        <h3 class="title title-3">')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Need help?'))))
            __M_writer(u'</h3>\n        <p>')
            __M_writer(filters.html_escape(filters.decode.utf8(_('Please check your Junk or Spam folders in case our email isn\'t in your INBOX. Still can\'t find the verification email? Request help via the link below.'))))
            __M_writer(u'</p>\n      </div>\n    </aside>\n  </section>\n\n')
        __M_writer(u'</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_requirejs(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def requirejs():
            return render_requirejs(context)
        __M_writer = context.writer()
        __M_writer(u'\n  require(["js/factories/index"], function (IndexFactory) {\n      IndexFactory();\n  });\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_bodyclass(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def bodyclass():
            return render_bodyclass(context)
        __M_writer = context.writer()
        __M_writer(u'is-signedin index view-dashboard')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_online_help_token(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        return "home" 
        
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        settings = context.get('settings', UNDEFINED)
        def title():
            return render_title(context)
        __M_writer = context.writer()
        __M_writer(filters.html_escape(filters.decode.utf8(_("{studio_name} Home").format(studio_name=settings.STUDIO_SHORT_NAME))))
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "line_map": {"16": 2, "33": 1, "67": 1, "68": 6, "69": 8, "70": 10, "75": 11, "80": 12, "85": 18, "90": 640, "96": 20, "123": 20, "124": 23, "125": 23, "126": 25, "127": 26, "128": 26, "129": 26, "130": 27, "131": 27, "132": 30, "133": 31, "134": 32, "135": 32, "136": 33, "137": 34, "138": 34, "139": 34, "140": 34, "141": 34, "142": 36, "143": 37, "144": 38, "145": 39, "146": 39, "147": 41, "148": 42, "149": 43, "150": 43, "151": 43, "152": 44, "153": 44, "154": 46, "155": 50, "156": 54, "157": 55, "158": 58, "159": 59, "160": 63, "161": 63, "162": 68, "163": 68, "164": 71, "165": 71, "166": 75, "167": 75, "168": 78, "169": 78, "170": 78, "171": 79, "172": 79, "173": 83, "174": 83, "175": 86, "176": 86, "177": 86, "178": 87, "182": 90, "183": 95, "184": 95, "185": 99, "186": 99, "187": 99, "188": 100, "192": 103, "193": 108, "194": 108, "195": 111, "196": 111, "197": 111, "198": 112, "202": 115, "203": 124, "204": 124, "205": 125, "206": 125, "207": 126, "208": 126, "209": 132, "210": 133, "211": 134, "212": 138, "213": 138, "214": 143, "215": 143, "216": 146, "217": 146, "218": 150, "219": 150, "220": 154, "221": 154, "222": 154, "223": 155, "224": 155, "225": 159, "226": 159, "227": 160, "228": 160, "229": 161, "230": 161, "231": 161, "232": 161, "233": 166, "234": 166, "235": 171, "236": 171, "237": 171, "238": 172, "242": 175, "243": 184, "244": 184, "245": 185, "246": 185, "247": 186, "248": 186, "249": 191, "250": 193, "251": 194, "252": 195, "253": 195, "254": 198, "255": 199, "256": 200, "257": 201, "258": 201, "259": 201, "260": 204, "261": 204, "262": 208, "263": 208, "264": 208, "265": 208, "266": 211, "267": 211, "268": 212, "269": 212, "270": 215, "271": 215, "272": 215, "273": 215, "274": 221, "275": 221, "276": 229, "277": 229, "278": 229, "279": 235, "283": 238, "284": 242, "285": 246, "286": 247, "287": 247, "288": 247, "289": 250, "290": 250, "291": 254, "292": 254, "293": 254, "294": 254, "295": 257, "296": 257, "297": 258, "298": 258, "299": 261, "300": 261, "301": 261, "302": 261, "303": 271, "304": 271, "305": 271, "306": 274, "307": 274, "308": 280, "309": 280, "310": 284, "311": 284, "312": 286, "313": 286, "314": 294, "315": 297, "316": 298, "317": 299, "318": 300, "319": 300, "320": 302, "321": 303, "322": 303, "323": 303, "324": 305, "325": 306, "326": 307, "327": 307, "328": 307, "329": 309, "330": 311, "331": 312, "332": 313, "333": 315, "334": 316, "335": 316, "336": 316, "337": 317, "338": 317, "339": 318, "340": 318, "341": 322, "342": 322, "343": 322, "344": 322, "345": 325, "346": 325, "347": 326, "348": 326, "349": 329, "350": 329, "351": 329, "352": 329, "353": 335, "354": 336, "355": 337, "356": 337, "357": 337, "358": 337, "359": 340, "360": 341, "361": 341, "362": 341, "363": 341, "364": 346, "365": 349, "366": 350, "367": 353, "368": 353, "369": 355, "370": 355, "371": 360, "372": 361, "373": 363, "374": 363, "375": 365, "376": 365, "377": 371, "378": 371, "379": 376, "380": 379, "381": 381, "382": 382, "383": 384, "384": 384, "385": 389, "387": 390, "388": 394, "389": 394, "390": 396, "391": 396, "392": 398, "393": 398, "394": 405, "395": 406, "396": 408, "397": 408, "398": 413, "401": 415, "402": 419, "403": 419, "404": 422, "405": 422, "406": 425, "407": 425, "408": 426, "409": 426, "410": 433, "411": 434, "412": 436, "413": 436, "414": 441, "417": 443, "418": 447, "419": 447, "420": 450, "421": 450, "422": 453, "423": 453, "424": 455, "425": 455, "426": 463, "427": 464, "428": 465, "429": 467, "430": 468, "431": 469, "432": 469, "433": 470, "434": 470, "435": 474, "436": 474, "437": 474, "438": 474, "439": 477, "440": 477, "441": 478, "442": 478, "443": 480, "444": 481, "445": 481, "446": 481, "447": 483, "448": 487, "449": 490, "450": 491, "451": 494, "452": 494, "453": 496, "454": 496, "455": 500, "456": 501, "457": 503, "458": 503, "459": 505, "460": 505, "461": 511, "462": 511, "463": 516, "464": 518, "465": 519, "466": 520, "467": 521, "468": 524, "469": 525, "470": 527, "471": 527, "472": 528, "473": 528, "474": 533, "475": 533, "476": 533, "477": 533, "478": 540, "479": 543, "480": 544, "481": 548, "482": 548, "483": 550, "484": 550, "485": 556, "486": 556, "487": 556, "488": 556, "489": 564, "490": 568, "491": 568, "492": 569, "493": 569, "494": 574, "495": 574, "496": 574, "497": 574, "498": 579, "499": 580, "500": 581, "501": 581, "502": 582, "508": 587, "509": 590, "510": 591, "511": 592, "512": 593, "513": 593, "514": 594, "515": 594, "516": 597, "517": 598, "518": 599, "519": 599, "520": 600, "526": 605, "527": 609, "528": 613, "529": 614, "530": 617, "531": 617, "532": 622, "533": 622, "534": 624, "535": 624, "536": 632, "537": 632, "538": 633, "539": 633, "540": 639, "546": 14, "552": 14, "558": 12, "564": 12, "570": 10, "574": 10, "581": 11, "588": 11, "594": 588}, "uri": "index.html", "filename": "/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/cms/templates/index.html"}
__M_END_METADATA
"""
