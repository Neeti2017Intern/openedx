#!/bin/sh
echo $PATH | egrep "/home/sankalp/edx-ficus.3-1/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/home/sankalp/edx-ficus.3-1/apps/edx/edx-platform/node_modules/.bin:/home/sankalp/edx-ficus.3-1/apps/edx/bin:/home/sankalp/edx-ficus.3-1/postgresql/bin:/home/sankalp/edx-ficus.3-1/git/bin:/home/sankalp/edx-ficus.3-1/perl/bin:/home/sankalp/edx-ficus.3-1/nodejs/bin:/home/sankalp/edx-ficus.3-1/java/bin:/home/sankalp/edx-ficus.3-1/erlang/bin:/home/sankalp/edx-ficus.3-1/ruby/bin:/home/sankalp/edx-ficus.3-1/python/bin:/home/sankalp/edx-ficus.3-1/elasticsearch/bin:/home/sankalp/edx-ficus.3-1/memcached/bin:/home/sankalp/edx-ficus.3-1/mongodb/bin:/home/sankalp/edx-ficus.3-1/sqlite/bin:/home/sankalp/edx-ficus.3-1/mysql/bin:/home/sankalp/edx-ficus.3-1/apache2/bin:/home/sankalp/edx-ficus.3-1/common/bin:$PATH"
export PATH
fi
echo $LD_LIBRARY_PATH | egrep "/home/sankalp/edx-ficus.3-1/common" > /dev/null
if [ $? -ne 0 ] ; then
LD_LIBRARY_PATH="/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/gems/2.1.0/gems/passenger-5.1.2/lib:/home/sankalp/edx-ficus.3-1/postgresql/lib:/home/sankalp/edx-ficus.3-1/git/lib:/home/sankalp/edx-ficus.3-1/perl/lib:/home/sankalp/edx-ficus.3-1/perl/lib/5.16.3/x86_64-linux-thread-multi/CORE:/home/sankalp/edx-ficus.3-1/erlang/lib:/home/sankalp/edx-ficus.3-1/ruby/lib:/home/sankalp/edx-ficus.3-1/python/lib:/home/sankalp/edx-ficus.3-1/memcached/lib:/home/sankalp/edx-ficus.3-1/mongodb/lib:/home/sankalp/edx-ficus.3-1/sqlite/lib:/home/sankalp/edx-ficus.3-1/mysql/lib:/home/sankalp/edx-ficus.3-1/apache2/lib:/home/sankalp/edx-ficus.3-1/common/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH
fi

TERMINFO=/home/sankalp/edx-ficus.3-1/common/share/terminfo
export TERMINFO
##### GIT ENV #####
GIT_EXEC_PATH=/home/sankalp/edx-ficus.3-1/git/libexec/git-core/
export GIT_EXEC_PATH
GIT_TEMPLATE_DIR=/home/sankalp/edx-ficus.3-1/git/share/git-core/templates
export GIT_TEMPLATE_DIR
GIT_SSL_CAINFO=/home/sankalp/edx-ficus.3-1/common/openssl/certs/curl-ca-bundle.crt
export GIT_SSL_CAINFO


PERL5LIB="/home/sankalp/edx-ficus.3-1/git/lib/site_perl/5.16.3:$PERL5LIB"
export PERL5LIB
GITPERLLIB="/home/sankalp/edx-ficus.3-1/git/lib/site_perl/5.16.3"
export GITPERLLIB
                    ##### GHOSTSCRIPT ENV #####
GS_LIB="/home/sankalp/edx-ficus.3-1/common/share/ghostscript/fonts"
export GS_LIB
##### IMAGEMAGICK ENV #####
MAGICK_HOME="/home/sankalp/edx-ficus.3-1/common"
export MAGICK_HOME

MAGICK_CONFIGURE_PATH="/home/sankalp/edx-ficus.3-1/common/lib/ImageMagick-6.9.8/config-Q16:/home/sankalp/edx-ficus.3-1/common/"
export MAGICK_CONFIGURE_PATH

MAGICK_CODER_MODULE_PATH="/home/sankalp/edx-ficus.3-1/common/lib/ImageMagick-6.9.8/modules-Q16/coders"
export MAGICK_CODER_MODULE_PATH
SASL_CONF_PATH=/home/sankalp/edx-ficus.3-1/common/etc
export SASL_CONF_PATH
SASL_PATH=/home/sankalp/edx-ficus.3-1/common/lib/sasl2 
export SASL_PATH
LDAPCONF=/home/sankalp/edx-ficus.3-1/common/etc/openldap/ldap.conf
export LDAPCONF
##### PERL ENV #####
PERL5LIB="/home/sankalp/edx-ficus.3-1/perl/lib/5.16.3:/home/sankalp/edx-ficus.3-1/perl/lib/site_perl/5.16.3:/home/sankalp/edx-ficus.3-1/perl/lib/5.16.3/x86_64-linux-thread-multi:/home/sankalp/edx-ficus.3-1/perl/lib/site_perl/5.16.3/x86_64-linux-thread-multi"
export PERL5LIB
##### NODEJS ENV #####

export NODE_PATH=/home/sankalp/edx-ficus.3-1/nodejs/lib/node_modules

            ##### JAVA ENV #####
JAVA_HOME=/home/sankalp/edx-ficus.3-1/java
export JAVA_HOME

##### RUBY ENV #####
                GEM_HOME="/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/gems/2.1.0"
                GEM_PATH="/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/gems/2.1.0"
                RUBY_HOME="/home/sankalp/edx-ficus.3-1/ruby"
                RUBYLIB="/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/site_ruby/2.1.0:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/site_ruby/2.1.0/x86_64-linux:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/site_ruby/:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/vendor_ruby/2.1.0:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/vendor_ruby/2.1.0/x86_64-linux:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/vendor_ruby/:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/2.1.0:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/2.1.0/x86_64-linux:/home/sankalp/edx-ficus.3-1/ruby/lib/ruby/:/home/sankalp/edx-ficus.3-1/ruby/lib"
                BUNDLE_CONFIG="/home/sankalp/edx-ficus.3-1/ruby/.bundler/config"
                export GEM_HOME
                export GEM_PATH
                export RUBY_HOME
                export RUBYLIB
                export BUNDLE_CONFIG
            ##### MEMCACHED ENV #####
		
      ##### MONGODB ENV #####

      ##### SQLITE ENV #####
			
##### MYSQL ENV #####

##### APACHE ENV #####

##### CURL ENV #####
CURL_CA_BUNDLE=/home/sankalp/edx-ficus.3-1/common/openssl/certs/curl-ca-bundle.crt
export CURL_CA_BUNDLE
##### SSL ENV #####
SSL_CERT_FILE=/home/sankalp/edx-ficus.3-1/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE
OPENSSL_CONF=/home/sankalp/edx-ficus.3-1/common/openssl/openssl.cnf
export OPENSSL_CONF
OPENSSL_ENGINES=/home/sankalp/edx-ficus.3-1/common/lib/engines
export OPENSSL_ENGINES


. /home/sankalp/edx-ficus.3-1/scripts/build-setenv.sh
PYTHONHOME=/home/sankalp/edx-ficus.3-1/python
export PYTHONHOME

PYTHON_EGG_CACHE=/home/sankalp/edx-ficus.3-1/.tmp
export PYTHON_EGG_CACHE
